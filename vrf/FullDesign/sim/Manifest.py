action = "simulation"

include_dirs = [ "../externals/HDLVerificationLibrary/BasicBlocks/src",
                 "../externals/HDLVerificationLibrary/SerDes/src",
                 "../externals/HDLVerificationLibrary/SPI/src",
                 "../environment/CLICpix2_Matrix",
                 "../environment/",
                 "../tests/sequences/",
                 "../tests/",
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/include',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/questa_mvc_src/sv',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/questa_mvc_src/sv/mvc_base',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/include',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/examples/i2c/common',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/questa_mvc_src/sv/i2c/',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/examples/spi/common',
                 __import__('os').environ.get('QUESTA_MVC_HOME') + '/questa_mvc_src/sv/spi/ ' ]

if 'netlist' in locals():
    vlog_opt = '-sv +define+netlist+NTC+RECREM'

top_module = "CLICpix2_Simulation_top"
sim_top = "CLICpix2_Simulation_top"
sim_tool = "modelsim"

files = ["../environment/CLICpix2_Simulation_top.sv"]

modules = { "local" : [ "../../../rtl" ] }
