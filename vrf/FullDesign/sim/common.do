### HDLVerificationLibrary Package
lappend auto_path [file join [pwd] .. externals HDLVerificationLibrary QuestaScripts ]
package require vsimPackage 1.0

namespace import ::vsimPackage::start_vsim
namespace import ::vsimPackage::compile_design
namespace import ::vsimPackage::run_test
namespace import ::vsimPackage::generate_arguments_from_file

set design "rtl"
set vopt "novopt"
set wave "wave.do"
set vsim_param "-classdebug -uvmcontrol=all -msgmode both -quiet -solvefaildebug=2 -sv_seed 1"
#set vsim_param "-classdebug"
#set vsim_param "-learn spiTest_learn"

set sdf_assignments_file sdfAssignments
set sdf_kind "-sdfmax"
set sdftyp [generate_arguments_from_file $sdf_assignments_file $sdf_kind RTL_PATH ../../../rtl] 

set voptargs "-ocf visibilityAfterOpt.ocf"
#set voptargs "+acc"

for {set i ${argc}} {$i > 0} {incr i -1} {
    switch [set [set i]] {
	"vopt"     { set vopt "vopt" }
	default  {set design [set [set i]]}
    }
}

set script {}
if { $design == "netlist" } {
    lappend script {suppress 3916} {run 100 ns} {suppress -clear 3916}
}
