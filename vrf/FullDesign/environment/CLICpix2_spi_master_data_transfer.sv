//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_spi_master_data_transfer.sv
// Description     : The SPI sequence item used by CLICpix2.
// Author          : Adrian Fiergolski
// Created On      : Tue Jul 12 16:26:01 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_spi_master_data_transfer
//The sequence item use to communicate with the CLICpix2 over the SPI interface.
class CLICpix2_spi_master_data_transfer extends spi_master_data_transfer #(SPI_SS_WIDTH);

   rand bit[7:0] address;
   rand bit[7:0] data;
   bit[7:0] data_miso;  //received data
   
   `uvm_object_utils_begin(CLICpix2_spi_master_data_transfer)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_spi_master_data_transfer> with the given ~name~.
   function new(string name="CLICpix2_spi_master_data_transfer");
      super.new(name);
   endfunction // new

   //Constraint: CLICpix2_constraint 
   constraint CLICpix2_constraint {
      mosi_data.size == 16;  //size
      first_transfer == 1'b1;
      last_transfer == 1'b1;
      
      foreach( address[i] )
	 address[i] == mosi_data[8+i];
      foreach( data[i] ) 
	 data[i] == mosi_data[i];
   }

   //Constraint: CLICpix2_valid_address_constraint 
   constraint CLICpix2_valid_address_constraint{
      address inside {CLICpix2_config::registers_ValidAddresses};
   }

   //Constraint: CLICpix2_RCR_constraint
   constraint CLICpix2_RCR_constraint{
      if(address == `READOUT_CONTROL_REGISTER_ADDRESS){
	 ( data[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] == 2'b00 ) -> ( data[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b01 );
	 ( data[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] == 2'b01 ) -> ( data[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b10 );
	 ( data[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] == 2'b10 ) -> ( data[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b11 );
	 data[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] inside { 2'b00, 2'b01, 2'b10 };
      }
   }

   //Constraint: CLICpix2_noRCR_during_PRBS
   constraint CLICpix2_noRCR_during_PRBS{
      if( isPRBS_on() ) {
	 !( address inside {`READOUT_CONTROL_REGISTER_ADDRESS});
      }
   }

   //Constraint: CLICpix2_noReadout_during_PRBS
   constraint CLICpix2_noReadout_during_PRBS{
      if( isPRBS_on() ) {
	 !( address inside {`READOUT_ADDRESS});
      }
   }

   //Constraint: CLICpix2_noReadout_afterRecentRCRaccess
   constraint CLICpix2_noReadout_afterRecentRCRaccess{
      if( wasRCRrecentelyModified() ) {
	 !( address inside {`READOUT_ADDRESS});
      }
   }

   
   //Constraint: CLICpix2_noReadout_whenMatrix_notConfigured
   constraint CLICpix2_noReadout_whenMatrix_notConfigured{
      if( ! isMatrixConfigured() ) {
	 !( address inside {`READOUT_ADDRESS});
      }
   }

   //Constraint: CLICPix2_noPRBS_if_wrongConfiguration
   constraint CLICPix2_noPRBS_if_wrongConfiguration{
      if( ! canPRBSbeEnabled() ) {
	 !( address inside {`START_STOP_PRBS_ADDRESS});
      }
   }   
   
   //Function: isPRBS_on
   //Check if the DUT is sending PRBS sequence
   function bit isPRBS_on();
      SPI_CLICpix2_sqr sqr_;
      assert( $cast( sqr_,  get_sequencer() ) );
      return sqr_.prbs_on;
   endfunction // isPRBS_on
   

   //Function: isMatrixConfigured
   function bit isMatrixConfigured();
      SPI_CLICpix2_sqr sqr_;
      assert( $cast( sqr_,  get_sequencer() ) );
      return sqr_.matrixConfigured;
   endfunction // is_matrixConfigured

   //Function: canPRBSbeEnabled
   //It checks if 8 columns readout is enabled
   function bit canPRBSbeEnabled();
      SPI_CLICpix2_sqr sqr_;
      assert( $cast( sqr_,  get_sequencer() ) );
      return (sqr_.rcrRegister[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b11);
   endfunction // canPRBSbeEnabled

   //Function: wasRCRrecentelyModified
   function bit wasRCRrecentelyModified();
      SPI_CLICpix2_sqr sqr_;
      assert( $cast( sqr_,  get_sequencer() ) );
      return sqr_.rcr_access;
   endfunction // wasRCRreventelyModified
   
   //Function: pre_randomize
   //To not call super.pre_randomize
   function void pre_randomize();
      data_width_constraint.constraint_mode(0);
   endfunction // pre_randomize

   //Task :do_receive
   //Assigns address and data
   virtual task do_receive(mvc_pkg::mvc_config_base config_base);
      super.do_receive(config_base);

      foreach( address[i] )
	address[i] = mosi_data[8+i];
      foreach( data[i] ) 
	data[i] = mosi_data[i];
      foreach( data_miso[i] )
	data_miso[i] = miso_data[i];
   endtask // do_receive

   //Function: do_copy
   virtual function void do_copy (uvm_object rhs);
      CLICpix2_spi_master_data_transfer rhs_;
      assert( $cast( rhs_, rhs) );
      address = rhs_.address;
      data = rhs_.data;
      data_miso = rhs_.data_miso;
   endfunction // do_copy

   //Function: do_print
   virtual function void do_print(uvm_printer printer);
      printer.print_int("address", address, $bits(address), UVM_DEC );
      printer.print_int("write", data, $bits(data), UVM_HEX );
      printer.print_int("read", data_miso, $bits(data_miso), UVM_HEX );
   endfunction // do_print

endclass // CLICpix2_spi_master_data_transfer
