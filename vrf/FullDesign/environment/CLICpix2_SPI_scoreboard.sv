//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_SPI_scoreboard.sv
// Description     : The scoreboard of the SPI interface.
// Author          : Adrian Fiergolski
// Created On      : Wed Feb 24 09:56:47 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: CLICpix2_SPI_scoreboard

//Class: CLICpix2_SPI_scoreboard_config
//The configuration of the <CLICpix2_SPI_scoreboard> scoreboard.
class CLICpix2_SPI_scoreboard_config extends scoreboard_base_config;

   //Variable: resetInter
   vCLICpix2_ClockReset resetInter;

   //Variable: spi_sqr
   SPI_CLICpix2_sqr  spi_sqr;
   
   `uvm_object_utils_begin(CLICpix2_SPI_scoreboard_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_SPI_scoreboard_config> with the given ~name~.
   function new(string name="CLICpix2_SPI_scoreboard_config");
      super.new(name);
   endfunction // new
   
endclass // CLICpix2_SPI_scoreboard_config

//Class: CLICpix2_SPI_scoreboard
//The class implements the CLICpix2 SPI scoreboards.
//It uses the following ports:
//- "SPI.registers"
class CLICpix2_SPI_scoreboard extends scoreboard_fifos;

   //Variable: cfg_
   CLICpix2_SPI_scoreboard_config cfg_;

   //Variable: mem_arr
   protected byte mem_arr[int unsigned ] = '{default: 'h00};

   //Variable: resetInter
   vCLICpix2_ClockReset resetInter;

   //Variable : tr
   //Current transaction
   protected CLICpix2_spi_master_data_transfer tr;

   //Section: Statistics

   //Variable: total_cnt
   //Count number of received SPI transactions.
   int 		  unsigned total_cnt = 0;

   //Variable: mismatch_cnt
   //Number of detected mismatches.
   int 		  unsigned mismatch_cnt = 0;

   //Variable: register_cnt
   //Number of valid register accesses.
   int 		  unsigned register_cnt = 0;
   
   `uvm_component_utils_begin(CLICpix2_SPI_scoreboard)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_SPI_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //Retrieve the configuration and the interfaces.
   function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      assert( $cast( cfg_, cfg) );
      if(cfg_ == null)
	`uvm_error("NO_CONFIG", "Can't configure the scoreboard");
      
      resetInter = cfg_.resetInter;

      mem_arr_init();

   endfunction // build_phase

   //Task: run_phase
   //It spawns in background <resetTrack> tasks.
   virtual task run_phase(uvm_phase phase);
      fork
	 resetTrack();
      join_none;
      super.run_phase(phase);
   endtask // run_phase

   //Task: receive
   //The task selects the proper receive_* method.
   virtual task receive(string port, uvm_sequence_item transaction_);
      CLICpix2_spi_master_data_transfer spi;
      case(port)
	"SPI.registers": begin
	assert($cast(spi, transaction_) );
	   receive_spi(spi);	
	end
	default: super.receive(port, transaction_);
      endcase // case (port)
      
   endtask // receive

   
   //Function: receive_spi
   //The function proceses SPI transactions.
   virtual function void receive_spi(CLICpix2_spi_master_data_transfer spi);
      tr = spi;
      if(spi.slave_id == 0) begin
	 total_cnt++;
	 spi_read(spi.address, spi.data_miso);   //first analyse read data
	 spi_write(spi.address, spi.data);  //afterwards update the <mem_arr> with the written values
      end
   endfunction // receive_spi
   
   //Function: read_mem_arr
   //Wraps read access to the <mem_arr>
   virtual function logic[7:0] read_mem_arr(input byte pointer);
      if( pointer inside {CLICpix2_config::registers_ValidAddresses} )
	case (pointer)
	  `MATRIX_PROGRAMMING_ADDRESS: return 8'b?;
	  `START_STOP_PRBS_ADDRESS: return 8'b?;
	  default: return mem_arr[pointer];
	endcase // case (pointer)
      else //dummy address
	return 8'b?;
   endfunction // write_mem_arr

   virtual function byte peek_mem_arr(input byte pointer);
      return mem_arr[pointer];
   endfunction // peek_mem_arr
   
   
   //Function: write_mem_arr
   //Wraps write access to the <mem_arr>
   virtual function void write_mem_arr(input byte pointer, input byte data);
      if( pointer inside {CLICpix2_config::registers_ValidAddresses} )
	case (pointer)
	  `READOUT_ADDRESS :  ; //do nothing
	  `MATRIX_PROGRAMMING_ADDRESS :  ; //do_nothing
	  `START_STOP_PRBS_ADDRESS:
	    cfg_.spi_sqr.prbs_on = 1'b1;
	  `READOUT_CONTROL_REGISTER_ADDRESS: begin
	     mem_arr[pointer] = data;
	     cfg_.spi_sqr.rcrRegister = data;
	  end
	  default: mem_arr[pointer] = data;
	endcase // case (pointer)
      
   endfunction // write_mem_arr

   
   //Function: spi_read
   //Analyse data being read.
   virtual function void spi_read(input byte address, input byte data);

      logic[7:0] expected; //can contain don't care values

      expected = read_mem_arr(address);
      SPI_read_corresponds_to_register_model: assert ( data ==? expected ) begin //ok
	 register_cnt++;
	 `uvm_info("spi scoreboard",
		   $sformatf({"\n Data matched --- Address: %h : data: %h " },
			     address, expected ), UVM_LOW );
      end
      else begin
	 SPI_read_corresponds_to_register_model: assert (0);
	 mismatch_cnt ++;
	 `uvm_error("spi scoreboard",
		    $sformatf({"\n Data mismatch --- Address: %h : read: %h expected: %h" },
			      address, data,  expected ) );
      end
   endfunction // spi_read

   //Function: spi_write
   //Function updated mem_arr
   virtual function void spi_write(input byte address, input byte data);
      
      if( ! mem_arr.exists(address) ) begin  //address out of range
	 `uvm_warning("spi scoreboard",
		      $sformatf({"\n Write address out of CLICpix2 space : \n %h : %h"}, address, data  ) );
      end
      else begin
	 write_mem_arr(address, data);
	 `uvm_info("spi scoreboard",
		   $sformatf("\n Write access --- address: %h, data: %h\n",
			     address, data), UVM_LOW );
      end
      
   endfunction // spi_write

   //Task: resetTrack
   task resetTrack();
      forever begin
	 @(negedge resetInter.reset_n);
	 cfg_.spi_sqr.rcrRegister = CLICpix2_config::registers_DefaultValues[`READOUT_CONTROL_REGISTER_ADDRESS];
	 mem_arr_init();
	 cfg_.spi_sqr.prbs_on = 1'b0;
      end
   endtask // resetTrack
   
   //Function: convert2string
   virtual function string convert2string();
      convert2string =$sformatf( {"\n SPI packets: %0d",
				  "\n Data mismatch: %0d",
				  "\n Valid register acceses: %0d"},
				 total_cnt, mismatch_cnt, register_cnt );
   endfunction // convert2string

   //Function: report_phase
   //It reports statistics.
   //This function prints only type of the scoreboard and should be defined in all inheriting classes. 
   virtual function void report_phase(uvm_phase phase);
      super.report_phase(phase);
      //Print statistics
      `uvm_info( {get_type_name(), " statistics"} , convert2string(), UVM_MEDIUM );
   endfunction // report_phase

   //Function: mem_arr_init
   //It initialises the <mem_arra> with values from <default_arr>.
   virtual function void mem_arr_init();
      mem_arr.delete();
      foreach( CLICpix2_config::registers_ValidAddresses[i] )
	mem_arr[ CLICpix2_config::registers_ValidAddresses[i] ] = CLICpix2_config::registers_DefaultValues[i] ;
   endfunction // mem_arr_init

endclass // CLICpix2_SPI_scoreboard
