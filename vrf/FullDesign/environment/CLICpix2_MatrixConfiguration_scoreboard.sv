//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_MatrixConfiguration_scoreboard.sv
// Description     : The scoreboard of the Matrix Configuration.
// Author          : Adrian Fiergolski
// Created On      : Wed Jul 13 15:27:45 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: CLICpix2_MatrixConfiguration_scoreboard

//Class: CLICpix2_MatrixConfiguration_scoreboard_config
//The configuration of the <CLICpix2_MatrixConfiguration_scoreboard> scoreboard.
class CLICpix2_MatrixConfiguration_scoreboard_config extends scoreboard_base_config;

   //Variable: matrixInter 
   vCLICpix2_Matrix_inter matrixInter;

   `uvm_object_utils_begin(CLICpix2_MatrixConfiguration_scoreboard_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_MatrixConfiguration_scoreboard_config> with the given ~name~.
   function new(string name="CLICpix2_MatrixConfiguration_scoreboard_config");
      super.new(name);
   endfunction // new
   
endclass // CLICpix2_MatrixConfiguration_scoreboard_config


//Class: CLICpix2_MatrixConfiguration_scoreboard
//The class implements the CLICpix2 MatrixConfiguration scoreboards.
//It uses the following ports:
//- "SPI.config"
class CLICpix2_MatrixConfiguration_scoreboard extends scoreboard_fifos;

   //Variable : tr
   //Current transaction
   protected CLICpix2_Matrix_sequence_item tr;


   //Variable: matrixInter
   vCLICpix2_Matrix_inter matrixInter;
   
   //Section: Statistics

   //Variable: total_cnt
   //Count number of received MatrixConfiguration transactions.
   int 		  unsigned total_cnt = 0;

   
   `uvm_component_utils_begin(CLICpix2_MatrixConfiguration_scoreboard)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_MatrixConfiguration_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //Retrieve the configuration and the interfaces.
   function void build_phase(uvm_phase phase);
      CLICpix2_MatrixConfiguration_scoreboard_config cfg_;

      super.build_phase(phase);
      assert( $cast( cfg_, cfg) );
      
      matrixInter = cfg_.matrixInter;
   endfunction // build_phase
   
   //Task: receive
   //The task selects the proper receive_* method.
   virtual task receive(string port, uvm_sequence_item transaction_);
      CLICpix2_Matrix_sequence_item tr;

      case(port)
	"SPI.config": begin
	assert($cast(tr, transaction_) );
	   receive_matrixConfig(tr);	
	end
	default: super.receive(port, transaction_);
      endcase // case (port)
      
   endtask // receive

   
   //Task: receive_matrixConfig
   //The task proceses MatrixConfiguration transactions.
   virtual task receive_matrixConfig(CLICpix2_Matrix_sequence_item tr);
      this.tr = tr;
      total_cnt++;
      check_matrix_assertions(tr);
   endtask // receive_matrixConfig
   
   //Function: convert2string
   virtual function string convert2string();
      convert2string =$sformatf( {"\n Matrix Configuration packets: %0d" },
				 total_cnt);
   endfunction // convert2string

   //Function: report_phase
   //It reports statistics.
   //This function prints only type of the scoreboard and should be defined in all inheriting classes. 
   virtual function void report_phase(uvm_phase phase);
      super.report_phase(phase);
      //Print statistics
      `uvm_info( {get_type_name(), " statistics"} , convert2string(), UVM_MEDIUM );
   endfunction // report_phase

   //Task: check_matrix_assertions
   task check_matrix_assertions(CLICpix2_Matrix_sequence_item tr);
      
      repeat(17) //wait 17 clock cycles when configuration propagates in the matrix
	@(posedge matrixInter.clock);

      foreach( tr.pixels[r,c] ) begin
	 SPI_matrix_configuration_model_vs_registers_mask: assert( matrixInter.matrix[r][c].mask == tr.pixels[r][c].c.mask ) else
	   $error( "WRONG_MATRIX_CONFIG", $sformatf("Pixel[%0d][%0d] - Send mask: %h, configured mask: %h",r,c, tr.pixels[r][c].c.mask, matrixInter.matrix[r][c].mask) );
	 SPI_matrix_configuration_model_vs_registers_th_adj: assert( matrixInter.matrix[r][c].th_adj == tr.pixels[r][c].c.th_adj ) else
	   $error( "WRONG_MATRIX_CONFIG", $sformatf("Pixel[%0d][%0d] - Send th_adj: %h, configured th_adj: %h",r,c, tr.pixels[r][c].c.th_adj, matrixInter.matrix[r][c].th_adj) );
	 SPI_matrix_configuration_model_vs_registers_count_mode: assert( matrixInter.matrix[r][c].count_mode == tr.pixels[r][c].c.count_mode ) else
	   $error( "WRONG_MATRIX_CONFIG", $sformatf("Pixel[%0d][%0d] - Send count_mode: %h, configured count_mode: %h",r,c, tr.pixels[r][c].c.count_mode, matrixInter.matrix[r][c].count_mode) );
	 SPI_matrix_configuration_model_vs_registers_TP_en: assert( matrixInter.matrix[r][c].TP_en == tr.pixels[r][c].c.TP_en ) else
	   $error( "WRONG_MATRIX_CONFIG", $sformatf("Pixel[%0d][%0d] - Send TP_en: %h, configured TP_en: %h",r,c, tr.pixels[r][c].c.TP_en, matrixInter.matrix[r][c].TP_en) );
	 SPI_matrix_configuration_model_vs_registers_long_counter: assert( matrixInter.matrix[r][c].long_counter == tr.pixels[r][c].c.long_counter ) else
	   $error( "WRONG_MATRIX_CONFIG", $sformatf("Pixel[%0d][%0d] - Send long_counter: %h, configured long_counter: %h",r,c, tr.pixels[r][c].c.long_counter, matrixInter.matrix[r][c].long_counter) );
      end
      
   endtask // check_matrix_assertion

endclass // CLICpix2_MatrixConfiguration_scoreboard
