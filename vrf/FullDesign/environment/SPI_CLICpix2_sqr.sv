//                              -*- Mode: Verilog -*-
// Filename        : SPI_CLICpix2_sqr.sv
// Description     : Sequencer for CLICpix2 SPI access.
// Author          : Adrian Fiergolski
// Created On      : Fri Sep  2 19:23:30 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: SPI_CLICpix2_sqr
//It adds fields accessible by sequences.
class SPI_CLICpix2_sqr extends mvc_sequencer;

   //Variable: matrixConfigured
   //CLICpix2 matrix is configured
   bit matrixConfigured = 1'b0;

   //Variable: prbs_on
   //Indicates if PRBS is on.
   bit prbs_on = 1'b0;

   //Variable: rcr_access
   //Indicates that there was RCR register access
   //So incoming data may be invalid
   bit rcr_access = 1'b0;
   
   //Variable: rcrRegister
   //It's maintained by a <CLICpix2_SPI_scoreboard>.
   //Normally there should be register model.
   byte rcrRegister = CLICpix2_config::registers_DefaultValues[`READOUT_CONTROL_REGISTER_ADDRESS];

   `uvm_component_utils_begin(SPI_CLICpix2_sqr)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <SPI_CLICpix2_sqr> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Task: run_phase
   //It spawns in background <reset_rcr_access> task.
   virtual task run_phase(uvm_phase phase);
      fork
	 reset_rcr_access();
      join_none
      super.run_phase(phase);
   endtask // run_phase

   //Task: reset_rcr_access
   virtual task reset_rcr_access();
      forever begin
	 @(rcrRegister);
	 rcr_access = 1'b1;
	 
	 while(rcr_access) begin
	    fork 
	       begin
		  fork
		     @(rcrRegister);
		     #300ns rcr_access = 1'b0;
		  join_any
		  disable fork;
	       end
	    join;
	 end
      
      end // forever begin
   endtask // reset_rcr_access
   
endclass // SPI_CLICpix2_sqr
