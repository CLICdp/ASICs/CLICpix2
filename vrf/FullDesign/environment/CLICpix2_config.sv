//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_config.sv
// Description     : Configuration of the CLICpix2_env
// Author          : Adrian Fiergolski
// Created On      : Thu Jan 28 18:41:26 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: CLICpix2 environment configuration.

//Class: CLICpix2_seq_config
//It holds sequence specific configuration.
class CLICpix2_seq_config extends uvm_object;
   
   `uvm_object_utils_begin(CLICpix2_seq_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_seq_config> with the given ~name~.
   function new(string name="CLICpix2_seq_config");
      super.new(name);
   endfunction // new
   
endclass // CLICpix2_seq_config

//Class: CLICpix2_config
//It holds configuration of the <CLICpix2_env>.
class CLICpix2_config extends uvm_object;

   //Variable: clockReset_inter
   vCLICpix2_ClockReset clockReset_inter;

   //Variable: outputs_inter
   vCLICpix2_outputs outputs_inter;

   //Variable: serial_inter
   vSerDes_inter serial_inter;

   //Variable: spi_inter;
   vSPI_inter spi_inter;
   
   //Variable: matrix_inter
   vCLICpix2_Matrix_inter matrix_inter;

   //Variable: seqCfg
   //Configuration of the sequence
   CLICpix2_seq_config seqCfg;

   //Variable: spiCfg
   //Configuration of the spi environment
   SPI_config spiCfg;

   //Variable: serialCfg
   //Configuration of the SerDes environment
   SerDes_config serialCfg;

   //Variable: chargeCfg
   //Configuration of the Charge injection agent
   CLICpix2_ChargeInjection_config chargeCfg;

   //Variable: scoreCfg
   //Configuration of the scoreboard
   CLICpix2_scoreboard_config scoreCfg;

   //Variable: registers_ValidAddresses
   static const byte registers_ValidAddresses[] = '{ `CLICPIX2_REGISTERS_VALID_ADDRESSES };
   
   //Variable: registers_DefaultValues
   static const byte registers_DefaultValues[] =  '{ `CLICPIX2_REGISTERS_DEFAULT_VALUES };
   
   `uvm_object_utils_begin(CLICpix2_config)
      `uvm_field_object(seqCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(spiCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(serialCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(chargeCfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(scoreCfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_config> with the given ~name~.
   function new(string name="CLICpix2_config");
      super.new(name);
   endfunction // new
   
   //Function: createDefault
   //
   //It creates the default <seqCfg>, <scoeCfg> and spiCfg.
   //~parent~ gives the possibility to profit factory.
   virtual function void createDefault(uvm_component parent = null);
      seqCfg = CLICpix2_seq_config::type_id::create("CLICpix2 default sequence configuration", parent);
      scoreCfg = CLICpix2_scoreboard_config::type_id::create("CLICpix2 scoreboard configuration", parent);
      spiCfg = SPI_config::type_id::create("SPI configuration", parent);
      spiCfg.createDefault(parent);
      serialCfg = SerDes_config::type_id::create("SerDes configuration", parent);
      chargeCfg = CLICpix2_ChargeInjection_config::type_id::create("Charge injection configuration", parent);
   endfunction // createDefault

endclass // CLICpix2_config
