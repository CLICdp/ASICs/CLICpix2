//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_virtual_sqr.sv
// Description     : The virtual sequencer of the CLICpix2_env.
// Author          : Adrian Fiergolski
// Created On      : Thu Jan 28 18:39:06 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_virtual_sqr
//The virtual sequencer to run sequences in <CLICpix2_env>
class CLICpix2_virtual_sqr extends uvm_virtual_sequencer;

   //Variable: port
   //TLM port to peek packets from mac_agent's monitor
   uvm_blocking_peek_port #(uvm_sequence_item) port[string];

   //Variable: 
   CLICpix2_config cfg;
   
   //Variable: spi_sqr
   SPI_virtual_sqr spi_sqr;

   //Variable: charge_sqr
   CLICpix2_ChargeInjection_sqr charge_sqr;

   
   `uvm_component_utils_begin(CLICpix2_virtual_sqr)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(spi_sqr, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_virtual_sqr> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   function void build_phase(uvm_phase phase);
      uvm_object tmp;
      
      super.build_phase(phase);

      //Set configuration
      if( get_config_object("cfg", tmp, 0) )
	assert( $cast(cfg, tmp) );
      
      if(cfg == null) begin
	 `uvm_fatal("NOCONFIG", "config not set for this component.")
      end

      foreach( cfg.spiCfg.monCfg.submonitors[name] ) 
	port[ name ] = new( name, this);

      port[ "Serial" ] = new( "Serial", this);
      port[ "Charge" ] = new( "Charge", this);
      
   endfunction // build_phase
   
endclass // CLICpix2_virtual_sqr
