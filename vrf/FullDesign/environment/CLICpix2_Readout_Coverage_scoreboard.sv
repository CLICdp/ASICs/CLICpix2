//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_Readout_Coverage_scoreboard.sv
// Description     : The class expands the Readout scoreboard with coverage metrics.
// Author          : Adrian Fiergolski
// Created On      : Mon Jul 25 15:26:06 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_Readout_Coverage_scoreboard
//The class expands the Readout scoreboard with coverage metrics.
class CLICpix2_Readout_Coverage_scoreboard extends CLICpix2_Readout_scoreboard;

   enum { CHARGE_INJECTION, MATRIX_CONFIGURATION} readoutKind = MATRIX_CONFIGURATION;
         
   `uvm_component_utils_begin(CLICpix2_Readout_Coverage_scoreboard)
   `uvm_component_utils_end

   //Variable: pixelCompressionOccurred
   //Asserted when pixel compression occurred
   bit 	pixelCompressionOccurred = 0;

   //Variable: superPixelCompressionOccurred
   bit 	superPixelCompressionOccurred = 0;

   //Variable: columnCompressionOccurred
   bit 	columnCompressionOccurred = 0;

   //Variable: collumnOrsuperPixelCompression
   //Needed for coverage
   bit 	collumnOrsuperPixelCompression = 0;

   //Variable: emptyPacket
   bit 	emptyPacket = 1;
   
   //Variable: clkDiv
   //Readout clock divider
   //For coverage only.
   protected bit [1:0] clkDiv;

   //Variable: padding
   //Count number of padded bits
   int unsigned padding[8] = '{ 8{0} };
   
   //Covergroup: Readout_cg
   covergroup Readout_cg;
      readout_kind: coverpoint readoutKind;
      
      parallel_collumns : coverpoint rcr {
	 bins coll_2 = {2'b01};
	 bins coll_4 = {2'b10};
	 bins coll_8 = {2'b11};
      }

      clk_divider : coverpoint clkDiv {
         bins col2 = {2'b00};
         bins col4 = {2'b01};
         bins col8 = {2'b10};
      }

      tot_clk_divider : coverpoint totClkDiv;

      pixel_compression_occurred: coverpoint pixelCompressionOccurred;
      super_pixel_compression_occurred: coverpoint superPixelCompressionOccurred;
      column_compression_occurred: coverpoint columnCompressionOccurred;
      empty: coverpoint emptyPacket;
      readout_configuration: cross parallel_collumns, pixel_compression_occurred, collumnOrsuperPixelCompression, empty {
	 illegal_bins can_t_be_empty_and_have_pixel_compression = binsof(pixel_compression_occurred) intersect {1'b1} && 
								  binsof(collumnOrsuperPixelCompression) intersect{1'b1} &&
								  binsof(empty) intersect{1'b1};
      }
      
      
   endgroup // Readout_cg
   
   //Covergroup: ReadoutPadding_cg
   covergroup ReadoutPadding_cg with function sample(int unsigned padd);
      padding : coverpoint padd
	{bins padding_0 = {0};
	 bins padding[10] = { [1:3599] };
	 bins padding_3600 = {3600};
      }
   endgroup // ReadoutPadding_cg

   //Covergroup: ReadoutPixels_cg
   covergroup ReadoutPixels_cg with function sample( CONFIGURATION_PIXEL_LOGIC_T pixel);
      pixCfg : cross pixel.mask, pixel.long_counter, pixel.count_mode;
   endgroup // ReadoutPixels_cg

   //Covergroup: HighTOA_cg
   //To be sure, that high values have been checked
   covergroup HighTOA_cg with function sample(READOUT_PIXEL_T pixel, CONFIGURATION_PIXEL_LOGIC_T cfg );
      toa: coverpoint { pixel.tot, pixel.toa} iff cfg.long_counter { 
         bins low = { [0: CLICpix2_Matrix_TOA_LIMIT_sequence_item::TOA_LIMIT] };
         bins high = { [CLICpix2_Matrix_TOA_LIMIT_sequence_item::TOA_LIMIT+1 : $] };
      }
   endgroup // HighTOA_cg
   
   //Function: new
   //Creates a new <CLICpix2_Readout_Coverage_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
      Readout_cg = new();
      Readout_cg.set_inst_name( "Readout_cg" );
      ReadoutPadding_cg = new();
      ReadoutPadding_cg.set_inst_name( "ReadoutPadding_cg" );
      ReadoutPixels_cg = new();
      ReadoutPixels_cg.set_inst_name( "ReadoutPixels_cg" );
      HighTOA_cg = new ();
      HighTOA_cg.set_inst_name( "HighTOA_cg" );
   endfunction // new

   //Function: receive_charge_pkg
   //The function sets <readoutKind> and clkDiv.
   virtual function void receive_charge_pkg( CLICpix2_Matrix_sequence_item pkg);
      super.receive_charge_pkg(pkg);
      readoutKind = CHARGE_INJECTION;
      clkDiv = cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT];
   endfunction // receive_charge_pkg
   
   //Function: receive_MatrixConfig_pkg
   //The function sets <readoutKind>.
   virtual function void receive_MatrixConfig_pkg(CLICpix2_Matrix_sequence_item pkg);
      super.receive_MatrixConfig_pkg(pkg);
      readoutKind = MATRIX_CONFIGURATION;
   endfunction // receive_MatrixConfig_pkg

   //Function: print_pixels
   //Padding debug
   virtual function void print_pixels();
      string pixels;
      for(int r=CLICPIX2_ROW-1; r >= 0; r--) begin
	 for(int c=0; c< CLICPIX2_COL; c++)
	   if(received_pixels[r][c] == '0)
	     pixels ={pixels, " "};
	   else
	     pixels ={pixels,"X"};
	 pixels ={pixels, "\n"};
      end
      `uvm_info("Readout:", $sformatf("\n%s", pixels), UVM_MEDIUM );
   endfunction // print_pixels
   
   
   //Function: receive_serdes_pkg
   //It samples the <Readout_cg>
   virtual function void receive_serdes_pkg(SerDes_sequence_item  pkg);
      super.receive_serdes_pkg(pkg);
      collumnOrsuperPixelCompression = superPixelCompressionOccurred || columnCompressionOccurred;
      foreach(received_pixels[r,c])
	emptyPacket &= (received_pixels[r][c] == '0);

      //print_pixels();
      Readout_cg.sample();

      collumnOrsuperPixelCompression = 1'b0;
      emptyPacket = 1'b1;
   endfunction // receive_pkg

   //Function: process_dc_bit
   //Tries to examine <pixelCompressionOccurred>, <superPixelCompressionOccurred> and <columnCompressionOccurred>.
   virtual function void process_dc_bit(
					ref PIXEL_T pixels_dc [CLICPIX2_ROW *2][8], //double-column array
					ref int dc_counter, //number of processed bits for the given double column
					ref int sp_counter, //number of processed bits for the given super-pixel
					ref int row_index, 
					input int col_index,
					ref int row_slice, //bit of the given word
					input bit data);

      if(data == 0) begin  //sth must by skipped
	 //middle of the double-column
	 if( dc_counter < 2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL )  ) begin
	    if( sp_counter == 0 && 
		cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_SP_COMP_BIT] ) //no super-pixel
	     superPixelCompressionOccurred = 1'b1;
	    else if( row_slice == $bits(PIXEL_T)-1 && 
		     cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_COMP_BIT]) //no pixel
	      pixelCompressionOccurred = 1'b1;
	 end
	 else if( dc_counter == 2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL ) + 1 && 
		  cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_SP_COMP_BIT]) //no column
	   columnCompressionOccurred = 1'b1;
      end // if (data == 0)

      if(dc_counter == 2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL ) )  //3600
	padding[col_index]++;
      
      super.process_dc_bit(pixels_dc, dc_counter, sp_counter, row_index, col_index, row_slice, data);

   endfunction // process_dc_bit

   //Function: extract_rcr_columns
   virtual function bit extract_rcr_columns( ref WORD_TYPE data[$]);
      padding = '{ 8 {0} }; //reset padding counter
      extract_rcr_columns  = super.extract_rcr_columns(data);

      padding.rsort();
      for(int i= 0; i < 2**rcr-1; i++)  //ignore the longest double_column
	ReadoutPadding_cg.sample(padding[i]);
      
   endfunction // extract_rcr_columns
   
   //Function: clean
   //It cleans <pixelCompressionOccurred>, <superPixelCompressionOccurred> and <columnCompressionOccurred>.
   virtual function void clean();
      pixelCompressionOccurred = 1'b0;
      superPixelCompressionOccurred = 1'b0;
      columnCompressionOccurred = 1'b0;
   endfunction // clean

   //Function: compare_pixels
   //Samples pixel configuration
   virtual function void compare_pixels();
      super.compare_pixels;
      foreach( matrixInter.matrix[r,c] )
	ReadoutPixels_cg.sample( matrixInter.matrix[r][c] );
      foreach(received_pixels[r,c] )
	HighTOA_cg.sample( received_pixels[r][c], matrixInter.matrix[r][c]);
   endfunction // compare_pixels
   
endclass // CLICpix2_Readout_Coverage_scoreboard
