//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_env.sv
// Description     : The environment simulating the DUT.
// Author          : Adrian Fiergolski
// Created On      : Wed Jan 27 13:52:32 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_env
//The environment to verify CLICpix2 design.
class CLICpix2_env extends uvm_env;

   //Variable: spi_env
   //SPI environment
   SPI_CLICpix2_env spi_env;

   //Variable: serial_agent
   SerDes_agent serial_agent;

   //Variable charge_agent
   CLICpix2_ChargeInjection_agent charge_agent;
         
   //Variable: sqr
   //Virtual sequencer
   CLICpix2_virtual_sqr sqr;

   //Variable: scoreboard
   CLICpix2_scoreboard scoreboard;
      
   `uvm_component_utils_begin(CLICpix2_env)
      `uvm_field_object(spi_env, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(serial_agent, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(charge_agent, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(sqr, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(scoreboard, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_env> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //
   //It creates the internal agents.
   virtual function void build_phase(uvm_phase phase);
      uvm_object tmp;
      
      CLICpix2_config clicpix2Conf;
            
      //Set configuration
      if( get_config_object("cfg", tmp, 0) )
	assert( $cast(clicpix2Conf, tmp) );
      
      if(clicpix2Conf == null) begin
	 `uvm_fatal("NOCONFIG", "CLICpix2_config not set for this component.")
      end
      
      ////////////////////
      //Virtual sequencer
      ////////////////////
      sqr = CLICpix2_virtual_sqr::type_id::create("sqr", this);
      set_config_object( "sqr", "cfg", clicpix2Conf, .clone(0) );
      
      /////////////
      //Scoreboard
      /////////////
      scoreboard = CLICpix2_scoreboard::type_id::create("scoreboard", this);
      set_config_object( "scoreboard", "cfg", clicpix2Conf.scoreCfg, .clone(0) );

      ////////////////////
      //SPI_CLICpix2_environment
      ///////////////////
      
      spi_env = SPI_CLICpix2_env::type_id::create("spi_env", this);
      set_config_object( "spi_env", "cfg", clicpix2Conf.spiCfg, .clone(0) );

      ////////////////////////
      //SerDes environment
      ////////////////////////
      serial_agent = SerDes_agent::type_id::create("serial_agent", this);
      set_config_object( "serial_agent", "cfg", clicpix2Conf.serialCfg, .clone(0) );

      ////////////////
      //Charge agent
      ////////////////
      charge_agent = CLICpix2_ChargeInjection_agent::type_id::create("charge_agent", this);
      set_config_object( "charge_agent*", "cfg", clicpix2Conf.chargeCfg, .clone(0) );
      
      
      super.build_phase(phase);
      
   endfunction // build_phase

   //Function: connect_phase
   //
   //It connects:
   //- SPI submonitors' ports with the <scoreboard>
   //- virtual sequencer <sqr> to specific sequencers
   virtual function void connect_phase(uvm_phase phase);

      super.connect_phase(phase);

      foreach( scoreboard.ports[name] ) begin
	 string kind = name.substr(0, 3);
	 string name_ = name.substr(4, name.len()-1 );

	 case(kind)
	   "SPI." : begin
	      `uvm_info("connecting analysis port", 
			$psprintf("%s -> %s",
				  spi_env.monitor.ap[ name_ ].get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      if( spi_env.monitor.ap.exists( name_ ) )
		spi_env.monitor.ap[ name_ ].connect( scoreboard.ports[name] );
	      else
		`uvm_fatal( "NOPORT", $psprintf( "Monitor %s is missing port %s", spi_env.monitor.get_full_name(), name_ ) );
	   end // case: "SPI."
	   
	   "SER." : begin
	      `uvm_info("connecting analysis port", 
			$psprintf("%s -> %s",
				  serial_agent.monitor.ap.get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      serial_agent.monitor.ap.connect( scoreboard.ports[name] );
	   end // case: "SerDes."

	   "CHA." : begin
	      `uvm_info("connecting analysis port", 
			$psprintf("%s -> %s",
				  charge_agent.monitor.ap.get_full_name(),
				  scoreboard.ports[name].get_full_name() ),
			UVM_MEDIUM);
	      charge_agent.monitor.ap.connect( scoreboard.ports[name] );

	   end
	   
	   default: begin
	      `uvm_fatal("Connection error", {"Subcoreboard ", name, " tries to uses port of unrecognised kind."} );
	   end
	 endcase // case (kind)
	   
      end // foreach ( scoreboard.ports[name] )
      
      //Virtual sequencer
      sqr.spi_sqr = spi_env.sqr;
      sqr.charge_sqr = charge_agent.sqr;

      /////////////////
      //Peek ports
      /////////////////
      
      //spi port connection to virtual sequencer
      foreach( spi_env.monitor.peekPorts[name] ) begin
	`uvm_info("connecting peek port", 
		  $psprintf("%s -> %s",
			    spi_env.monitor.peekPorts[ name ].get_full_name(),
			    sqr.port[name].get_full_name() ),
		  UVM_MEDIUM);
	 if( sqr.port.exists(name) )
	   sqr.port[ name ].connect( spi_env.monitor.peekPorts[ name ] );
	 else
	   `uvm_fatal( "NOPORT", $psprintf( "Sequencer %s is missing port %s", sqr.get_full_name(), name ) );
      end // foreach ( spi_env.monitor.peekPorts[name] )

      //serial port connection to virtual sequencer
      `uvm_info("connecting peek port", 
		$psprintf("%s -> %s",
			  serial_agent.monitor.peekPort.get_full_name(),
			  sqr.port["Serial"].get_full_name() ),
		UVM_MEDIUM);
      if( sqr.port.exists("Serial") )
	sqr.port[ "Serial" ].connect( serial_agent.monitor.peekPort );
      else
	`uvm_fatal( "NOPORT", $psprintf( "Sequencer %s is missing port %s", sqr.get_full_name(), "Serial" ) );

      //charge injection port connection to virtual sequencer
      `uvm_info("connecting peek port", 
		$psprintf("%s -> %s",
			  charge_agent.monitor.peekPort.get_full_name(),
			  sqr.port["Charge"].get_full_name() ),
		UVM_MEDIUM);
      if( sqr.port.exists("Charge") )
	sqr.port[ "Charge" ].connect( charge_agent.monitor.peekPort );
      else
	`uvm_fatal( "NOPORT", $psprintf( "Sequencer %s is missing port %s", sqr.get_full_name(), "Charge" ) );
      
      
   endfunction // connect_phase

endclass // CLICpix2_env
