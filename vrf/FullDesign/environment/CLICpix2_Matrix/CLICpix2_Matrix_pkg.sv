//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_Matrix_pkg.sv
// Description     : It contains types used by matrix configuration.
// Author          : Adrian Fiergolski
// Created On      : Fri Apr 22 11:30:18 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


`ifndef CLICPIX2_MATRIX_PKG_SV
 `define CLICPIX2_MATRIX_PKG_SV


//Package: CLICpix2_Matrix_pkg
//The package contains types shared between <CLICpix2_Matrix> class and the <CLICpix2_matrix> interface.
package CLICpix2_Matrix_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>

   import BasicBlocks_pkg::*;
   
   //Typedef: CONFIGURATION_PIXEL_T
   typedef struct packed {
      bit 	  mask;
      bit 	  reserved_0;
      bit [3:0]   th_adj;
      bit [3:0]   reserved_1;
      bit 	  count_mode;
      bit 	  TP_en;
      bit 	  long_counter;
      bit 	  reserved_2;
   } CONFIGURATION_PIXEL_T;

   //Typedef: CONFIGURATION_PIXEL_LOGIC_T
   typedef struct packed {
      logic 	  mask;
      logic 	  reserved_0;
      logic [3:0]   th_adj;
      logic [3:0]   reserved_1;
      logic 	  count_mode;
      logic 	  TP_en;
      logic 	  long_counter;
      logic 	  reserved_2;
   } CONFIGURATION_PIXEL_LOGIC_T;

   parameter TOT_SIZE = 5;
   parameter TOA_SIZE = 8;
   
   //Typedef: READOUT_PIXEL_T
   typedef struct packed {
      bit 	  flag;
      bit [TOT_SIZE-1 :0]   tot; 
      bit [TOA_SIZE-1 :0]   toa; //with respect to the rising edge of the shutter 
   } READOUT_PIXEL_T;

   //Typedef: PIXEL_T
   typedef union packed {
      READOUT_PIXEL_T r;
      CONFIGURATION_PIXEL_T c;
   } PIXEL_T;

   //Parameter: CLICPIX2_ROW
   parameter CLICPIX2_ROW = 128;

   //Parameter: CLICPIX2_COL
   parameter CLICPIX2_COL = 128;

   //Parameter: CLICPIX2_SUPERPIXEL
   //Number of pixels in the super-pixel
   parameter CLICPIX2_SUPERPIXEL = 16;

   //Parameter: CLICPIX2_ACQUISITION_CLOCK_PERIOD
   parameter CLICPIX2_ACQUISITION_CLOCK_PERIOD = 10;   
   
   //Typedef: CLICPIX2_CONIGURATION_SPI_STREAM
   typedef byte  CLICPIX2_CONIGURATION_SPI_STREAM [28816];

   //Typedef: vCLICpix2_Matrix_inter
   typedef virtual interface CLICpix2_Matrix_inter vCLICpix2_Matrix_inter;

 `include "CLICpix2_ChargeInjection_config.sv"
 `include "CLICpix2_Matrix_sequence_item.sv"
 `include "CLICpix2_ChargeInjection_sqr.sv"
 `include "CLICpix2_ChargeInjection_driver.sv"
 `include "CLICpix2_ChargeInjection_monitor.sv"
 `include "CLICpix2_ChargeInjection_agent.sv"

   
endpackage // CLICpix2_Matrix_pkg

 `include "CLICpix2_Matrix_inter.sv"   
   
`endif
