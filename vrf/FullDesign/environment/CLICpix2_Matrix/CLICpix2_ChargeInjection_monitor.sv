//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_ChargeInjection_monitor.sv
// Description     : The charge injection monitor.
// Author          : Adrian Fiergolski
// Created On      : Wed Jul  6 12:04:40 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: CLICpix2_ChargeInjection monitor

// Class: CLICpix2_ChargeInjection_monitor
// It is the monitor of charge injection interface.
class CLICpix2_ChargeInjection_monitor extends monitor_base;

   
   // Variable: inter
   // A virtual interface of the <CLICpix2_ChargeInjection_inter>.
   vCLICpix2_Matrix_inter inter;

   //Typedef: COUNTERS_OVERFLOW_T
   typedef enum { TOT, TOA, TOA_LONG, COUNT, COUNT_LONG } COUNTERS_OVERFLOW_T;
      
   covergroup Counters_overflow_cg with function sample (COUNTERS_OVERFLOW_T overflow);
      overflow: coverpoint overflow;
   endgroup // Counters_overflow_cg
   
   `uvm_component_utils( CLICpix2_ChargeInjection_monitor )

   // Function: new
   // Create a new <CLICpix2_ChargeInjection_monitor> with the given instance ~name~ and parent. 
   // If ~name~ is not supplied, the driver is unnamed.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
      Counters_overflow_cg = new();
      Counters_overflow_cg.set_inst_name( "Counters_overflow_cg" );
   endfunction // new

   // Function: build_phase
   // Implements UVM build_phase.
   // It assigns interface.
   virtual function void  build_phase(uvm_phase phase);
      CLICpix2_ChargeInjection_monitor_config cfg_;
      super.build_phase(phase);
      assert( $cast( cfg_, monCfg) );
      inter = cfg_.inter;
   endfunction // build_phase

   //Task: receive
   //The task collects charge injection transactions.
   task receive();
      CLICpix2_Matrix_sequence_item tr_ = CLICpix2_Matrix_sequence_item::type_id::create("Transaction_monitor");
      tr_.knob  = CLICpix2_Matrix_sequence_item::READOUT;
      @(posedge inter.cbs.shutter); //wait for shutter
      fork
	 begin : PIXEL_THREADS
	    foreach ( tr_.pixels[row, col] ) begin
	       automatic int unsigned row_ = row;
	       automatic int unsigned col_ = col;
	       fork
		  begin
		     automatic int unsigned tot = 0;
		     automatic int unsigned toa = 0;
		     automatic int unsigned counts = 0;
		     automatic logic tot_shutter = 1; //count TOT only of the first hit

		     fork
			begin
			   fork
			      begin
				 @( negedge inter.discriminator_input[row_][col_]  //wait for active input (active low)
				    iff inter.matrix[row_][col_].mask == 0);       //and the given pixel can't be masked
				 tr_.pixels[row_][col_].r.flag = 1'b1;             //the is a hit
				 counts++;
			      end
			      @(negedge inter.cbs.shutter);                        //wait for shutter close
			   join_any
			   disable fork;
			end
		     join;

		     while(inter.cbs.shutter) begin
			fork
			   begin
			      fork
				 begin
				    @( negedge inter.discriminator_input[row_][col_] );
				    counts ++;
				 end
			      join_none
			      @(inter.cbs);
			      disable fork;
			   end
			join;
			
			toa++;  //start counting toa


			if( inter.discriminator_input[row_][col_] == 0 && tot_shutter)
			  tot++;

			if( inter.discriminator_input[row_][col_] == 1 )  //close TOT shutter
			  tot_shutter = 0;

		     end
		     
		     if( tr_.pixels[row_][col_].r.flag ) //if pixel was hit store results
		       if( inter.matrix[row_][col_].long_counter ) //long counter enabled
			 if( inter.matrix[row_][col_].count_mode ) begin //count mode for the given pixel is enabled
			    if( counts > 2 ** ( TOT_SIZE + TOA_SIZE ) -2 ) begin //overflow protection
			       Counters_overflow_cg.sample( COUNT_LONG );
			       { tr_.pixels[row_][col_].r.tot, tr_.pixels[row_][col_].r.toa}  =  2 ** ( TOT_SIZE + TOA_SIZE ) -2;
			    end
			    else
			      { tr_.pixels[row_][col_].r.tot, tr_.pixels[row_][col_].r.toa } = counts;
			 end
	       		 else begin
			    if( toa > 2 ** ( TOT_SIZE + TOA_SIZE ) -2 ) begin //overflow protection
			       Counters_overflow_cg.sample( TOA_LONG );
			       { tr_.pixels[row_][col_].r.tot, tr_.pixels[row_][col_].r.toa}  =  2 ** ( TOT_SIZE + TOA_SIZE ) -2;
			    end
			    else
			      { tr_.pixels[row_][col_].r.tot, tr_.pixels[row_][col_].r.toa } = toa;
			 end // else: !if( inter.matrix[row_][col_].count_mode )
		       else begin                                //long counter disabled
			  if( tot > 2 ** ( TOT_SIZE ) -2 ) begin //overflow protection
			     Counters_overflow_cg.sample( TOT );
			     tr_.pixels[row_][col_].r.tot  =  2 ** ( TOT_SIZE ) -2;
			  end
			  else 
			    tr_.pixels[row_][col_].r.tot = tot;      //save tot
			  
			  if( inter.matrix[row_][col_].count_mode )//count mode for the given pixel is enabled
			    if( counts > 2 ** ( TOA_SIZE ) -2 ) begin //overflow protection
			       Counters_overflow_cg.sample( COUNT );
			       tr_.pixels[row_][col_].r.toa  =  2 ** ( TOA_SIZE ) -2;
			    end
			    else
			      tr_.pixels[row_][col_].r.toa = counts;
			  else begin
			     if( toa > 2 ** ( TOA_SIZE ) -2 ) begin //overflow protection
				Counters_overflow_cg.sample( TOA );
				tr_.pixels[row_][col_].r.toa  =  2 ** ( TOA_SIZE ) -2;
			     end
			     else
			       tr_.pixels[row_][col_].r.toa = toa;
			  end // else: !if( counts > 2 ** ( TOA_SIZE ) -2 )
		       end // else: !if( inter.matrix[row][col].count_mode )

		  end // fork begin
	       join_none;
	    end // foreach ( tr_.pixels[row,col] )
	    wait fork;
	 end; // block: PIXEL_THREADS
      join;
      assert( $cast( tr, tr_) );
   endtask // receive

   
endclass // CLICpix2_ChargeInjection_monitor
