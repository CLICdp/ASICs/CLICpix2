//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_Matrix_sequence_item.sv
// Description     : Basic matrix sequence item.
// Author          : Adrian Fiergolski
// Created On      : Wed Jul 13 12:11:06 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_Matrix_sequence_item
//Basic matrix sequence item.
class CLICpix2_Matrix_sequence_item extends uvm_sequence_item;

   //Variable: knob
   rand enum {CONFIGURATION, READOUT} knob;
   
   //Variable: pixels
   rand PIXEL_T pixels[CLICPIX2_ROW][CLICPIX2_COL] = '{ CLICPIX2_ROW{ '{ CLICPIX2_COL{ 'b0} }} };

   //Variable: pattern
   rand enum {HALF_DIAGONAL_0_0, HALF_DIAGONAL_64_0, ALL_ZEROS, RANDOM} pattern;
   
   //Variable: inter
   protected vCLICpix2_Matrix_inter inter;
     
   `uvm_object_utils_begin(CLICpix2_Matrix_sequence_item)
   `uvm_object_utils_end

   //Variable: cfg
   protected CLICpix2_ChargeInjection_config cfg;
   
   //Function: new
   //Creates a new <CLICpix2_Matrix_sequence_item> with the given ~name~.
   function new(string name="CLICpix2_Matrix_sequence_item");
      super.new(name);
   endfunction // new

   constraint readout_constaints{
      if (knob == READOUT) {
         foreach( pixels[row,col] ) {
	    if( ! inter.matrix[row][col].long_counter ) { //only if long counter mode is disabled constrain relations between TOT and TOA
	       if( inter.matrix[row][col].count_mode ) { 
		  pixels[row][col].r.tot > 0;  //bigger than 0
		  pixels[row][col].r.tot < pixels[row][col].r.toa * (cfg.MaxDelayBetwwenEvents - 2); //but still need to give chance to occurrence of other hist in order to satisfy counter (which require min 2 clock cycles)
	       }
	       else {	  
		  pixels[row][col].r.tot <= pixels[row][col].r.toa; 
	       }
	    }
	 }
	 pattern dist {
	    HALF_DIAGONAL_0_0 := 10,
	    HALF_DIAGONAL_64_0 := 10,
            RANDOM :=10,
	    ALL_ZEROS := 5
	 };

      }      
      solve knob before pixels;
   }

   constraint configuration_constaints{
      if (knob == CONFIGURATION) {
	  foreach( pixels[r,c] ) {
	     pixels[r][c].c.reserved_0 == 0;
             pixels[r][c].c.reserved_1 == 0;
	     pixels[r][c].c.reserved_2 == 0;
	  }
      }
      solve knob before pixels;
   }

   //Function: post_randomize
   function void post_randomize();
      super.post_randomize();

      if( knob == READOUT) begin
	 case( pattern )
	   HALF_DIAGONAL_0_0:
	     foreach( pixels[r,c] )
	       if(r >= c  && c < CLICPIX2_COL/2 )
		 pixels[r][c].r = '0;
	   HALF_DIAGONAL_64_0:
	     foreach( pixels [r,c] )
	       if( r >= c + CLICPIX2_ROW /2 && c < CLICPIX2_COL/2 )
		 pixels[r][c].r = '0;
	   ALL_ZEROS:
	     foreach( pixels [r,c] )
	       	 pixels[r][c].r = '0;
	   RANDOM: ;
	 endcase // case ( pattern )
      end // if ( knob == READOUT)
   endfunction // post_randomize
   
   
   //Function: m_set_p_sequencer
   //Get a pointer to ~inter~.
   virtual function void m_set_p_sequencer();
      bit  no_config;
      super.m_set_p_sequencer();
      assert( $cast( cfg , uvm_utils#(CLICpix2_ChargeInjection_config, "cfg")::get_config( m_sequencer, no_config ) ) );
      if( no_config )
	`uvm_fatal("NO_CONFIG", "CLICpix2_Matrix_sequence_item didn't find configuration (CLICpix2_ChargeInjection_config) in its sequencer" );
      inter = cfg.inter;
   endfunction // m_set_p_sequencer
   
   //Function: do_copy
   //Assures that pixels array is copied.
   virtual function void do_copy (uvm_object rhs);
      CLICpix2_Matrix_sequence_item rhs_;
      assert( $cast(rhs_, rhs) );
      foreach( rhs_.pixels[col,row] )
	pixels[col][row] = rhs_.pixels[col][row];

      knob = rhs_.knob;
   endfunction // do_copy

endclass // CLICpix2_Matrix_sequence_item

//Class: CLICpix2_Matrix_TOA_LIMIT_sequence_item
//The class adds limit on TOA.
class CLICpix2_Matrix_TOA_LIMIT_sequence_item extends CLICpix2_Matrix_sequence_item;
   
   //Variable: TOA_LIMIT
   //Limit the maximum TOA
   parameter TOA_LIMIT = 2**8-1;
   
   `uvm_object_utils_begin(CLICpix2_Matrix_TOA_LIMIT_sequence_item)
   `uvm_object_utils_end

      constraint readout_constraint_TOA_limit {
      if (knob == READOUT) {
	 foreach( pixels[row,col] ) {
	    if( inter.matrix[row][col].long_counter ) {
	       if(inter.matrix[row][col].count_mode){
		  {pixels[row][col].r.tot, pixels[row][col].r.toa} *  cfg.MaxDelayBetwwenEvents < TOA_LIMIT;
	       }
	       else {
		  {pixels[row][col].r.tot, pixels[row][col].r.toa} < TOA_LIMIT;
	       }
	    }
	    else {
	       if(inter.matrix[row][col].count_mode){
		  pixels[row][col].r.toa *  cfg.MaxDelayBetwwenEvents < TOA_LIMIT;
	       }
	       else {
		  pixels[row][col].r.toa < TOA_LIMIT;
	       }
	    }
	 }
      }     
      solve knob before pixels;
   }
      
   //Function: new
   //Creates a new <CLICpix2_Matrix_TOA_LIMIT_sequence_item> with the given ~name~.
   function new(string name="CLICpix2_Matrix_TOA_LIMIT_sequence_item");
      super.new(name);
   endfunction // new

   //Function: post_randomize
   function void post_randomize();
      super.post_randomize();
   endfunction // post_randomize
      
endclass // CLICpix2_Matrix_TOA_LIMIT_sequence_item

//Class: CLICpix2_Matrix_LongCounterOverflow_sequence_item
class CLICpix2_Matrix_LongCounterOverflow_sequence_item extends CLICpix2_Matrix_sequence_item;
   
   `uvm_object_utils_begin(CLICpix2_Matrix_LongCounterOverflow_sequence_item)
   `uvm_object_utils_end

   constraint LongCounterOverflow_constraint {
      if (knob == READOUT) {
	 foreach( pixels[row,col] ) {
	    if( inter.matrix[row][col].long_counter ) { //overflow all long counters
	       if(inter.matrix[row][col].count_mode){ 
		  {pixels[row][col].r.tot, pixels[row][col].r.toa} == '1;
	       }
	    }
	 }
      }     
      solve knob before pixels;
   }

   //Function: new
   //Creates a new <CLICpix2_Matrix_LongCounterOverflow_sequence_item> with the given ~name~.
   function new(string name="CLICpix2_Matrix_LongCounterOverflow_sequence_item");
      super.new(name);
   endfunction // new

   //Function: post_randomize
   function void post_randomize();
      super.post_randomize();
   endfunction // post_randomize

endclass // CLICpix2_Matrix_LongCounterOverflow_sequence_item

