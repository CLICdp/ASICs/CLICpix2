//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_ChargeInjection_config.sv
// Description     : Config of the ChargeInjection agent.
// Author          : Adrian Fiergolski
// Created On      : Thu Jun 30 18:00:16 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


//Title: Charge injection configuration

//Class: CLICpix2_ChargeInjection_seq_config
//
//Basic class holding SerDes configuration of sequences.
class CLICpix2_ChargeInjection_seq_config extends uvm_object;
   
   `uvm_object_utils(CLICpix2_ChargeInjection_seq_config)

   //Function: new
   //
   //Creates a new <CLICpix2_ChargeInjection_seq_config> with the given ~name~
   function new(string name = "CLICpix2_ChargeInjection_seq_config");
      super.new(name);
   endfunction // new

endclass // CLICpix2_ChargeInjection_seq_config

//Class: CLICpix2_ChargeInjection_monitor_config
//The configuration class of the <CLICpix2_ChargeInjection_monnitor>
class CLICpix2_ChargeInjection_monitor_config extends monitor_base_config;
   
   `uvm_object_utils_begin(CLICpix2_ChargeInjection_monitor_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_ChargeInjection_monitor_config> with the given ~name~.
   function new(string name="CLICpix2_ChargeInjection_monitor_config");
      super.new(name);
   endfunction // new

   //Variable: inter
   vCLICpix2_Matrix_inter inter;
   
endclass // CLICpix2_ChargeInjection_monitor_config

//Class: CLICpix2_ChargeInjection_config
//
//Class holding SerDes configuration.
class CLICpix2_ChargeInjection_config extends uvm_object;

   //Variable: seqCfg
   //The configuration of the sequence
   CLICpix2_ChargeInjection_seq_config seqCfg;

   //Variable: monCfg
   //The configuration of the sequence
   CLICpix2_ChargeInjection_monitor_config monCfg;
   
   //Variable: inter
   vCLICpix2_Matrix_inter inter;

   //parameter: MAXDELAYBETWEENEVENTS_SIZE
   parameter MAXDELAYBETWEENEVENTS_SIZE = 4;
   
   //Variable: MaxDelayBetwwenEvents
   //Applies for pixels in Event counting mode.
   const bit[MAXDELAYBETWEENEVENTS_SIZE:0] MaxDelayBetwwenEvents = 10;
   
   //Variable: is_active 
   //Specifies whether agent is active.
   rand uvm_active_passive_enum is_active = UVM_ACTIVE;  
   
   `uvm_object_utils_begin(CLICpix2_ChargeInjection_config)
      `uvm_field_enum(uvm_active_passive_enum, is_active, UVM_DEFAULT)
      `uvm_field_object(seqCfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_object_utils_end

   //Function: new
   //
   //Creates a new <CLICpix2_ChargeInjection_config> with the given ~name~ and instantiates <seqCfg>.
   function new(string name = "CLICpix2_ChargeInjection_config");
      super.new(name);
      seqCfg = CLICpix2_ChargeInjection_seq_config::type_id::create("SerDes sequence default config" );
      monCfg = CLICpix2_ChargeInjection_monitor_config::type_id::create("SerDes monitor default config");
   endfunction // new
   
endclass // CLICpix2_ChargeInjection_config
