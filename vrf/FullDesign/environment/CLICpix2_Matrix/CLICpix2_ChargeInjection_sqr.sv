//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_ChargeInjection_sqr.sv
// Description     : Charge injection sequencer.
// Author          : Adrian Fiergolski
// Created On      : Wed Jul  6 12:03:06 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

// Class: CLICpix2_ChargeInjection_sqr
// Implement generic UVM sequencer of the CLICpix2_ChargeInjection.

class CLICpix2_ChargeInjection_sqr extends uvm_push_sequencer #( CLICpix2_Matrix_sequence_item );

   //Variable: cfg
   //CLICpix2_ChargeInjection configuration
   CLICpix2_ChargeInjection_config cfg;

   //Variable: 
   `uvm_component_utils_begin( CLICpix2_ChargeInjection_sqr )
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   // Function: new
   //
   // Creates a new <CLICpix2_ChargeInjection_sqr> with the given instance ~name~ and parent. 
   // If ~name~ is not supplied, the driver is unnamed.
   
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new
   
   //Function: build_phase
   //
   //It looks for a configuration.
   function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      if(cfg == null)
	`uvm_warning("NOCONFIG", "CLICpix2_ChargeInjection_config not set for this component")
   endfunction // build_phase

endclass // CLICpix2_ChargeInjection_sqr
