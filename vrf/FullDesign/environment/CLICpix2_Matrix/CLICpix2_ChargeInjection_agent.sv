//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_ChargeInjection_agent.sv
// Description     : ChargeInjection agent.
// Author          : Adrian Fiergolski
// Created On      : Wed Jul  6 11:59:55 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_ChargeInjection_agent
//CLICpix2_ChargeInjection Agent.
class CLICpix2_ChargeInjection_agent extends uvm_agent;

   //Variable: driver
   //The driver: <CLICpix2_ChargeInjection_driver>
   CLICpix2_ChargeInjection_driver driver;

   //Variable: sqr
   //The sequencer: <CLICpix2_ChargeInjection_sqr>.
   CLICpix2_ChargeInjection_sqr sqr;
   
   //Variable: monitor
   //The monitor: <CLICpix2_ChargeInjection_monitor>
   CLICpix2_ChargeInjection_monitor monitor;

   //Variable: cfg
   //CLICpix2_ChargeInjection configuration
   CLICpix2_ChargeInjection_config cfg;
   
   `uvm_component_utils_begin(CLICpix2_ChargeInjection_agent)
      `uvm_field_object(sqr, UVM_DEFAULT| UVM_REFERENCE)
      `uvm_field_object(driver, UVM_DEFAULT| UVM_REFERENCE)
      `uvm_field_object(monitor, UVM_DEFAULT| UVM_REFERENCE)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_ChargeInjection_agent> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   // Function: build_phase
   // Implement UVM build_phase.
   // The function creates a driver, a monitor and a sequencer.
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);

      monitor = CLICpix2_ChargeInjection_monitor::type_id::create("monitor",this);

      if(cfg == null) begin
	 `uvm_warning("NOCONFIG", "CLICpix2_ChargeInjection_config not set for this component.")
      end
      else is_active = cfg.is_active;
      
      if(get_is_active() == UVM_ACTIVE) begin
	 driver = CLICpix2_ChargeInjection_driver::type_id::create("driver",this);
	 sqr = CLICpix2_ChargeInjection_sqr::type_id::create("sqr", this);
      end

      set_config_object("monitor", "monCfg", cfg.monCfg, .clone(0) );
   endfunction // build_phase

   // Function: connect_phase
   //
   // Implement UVM connect phase.
   // It connects driver with sequencer.
   
   virtual function void connect_phase(uvm_phase phase);
      if(get_is_active() == UVM_ACTIVE) begin
         sqr.req_port.connect(driver.req_export);
      end
   endfunction // connect_phase

   
endclass // CLICpix2_ChargeInjection_agent
