//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_ChargeInjection_driver.sv
// Description     : The driver of the charge injection.
// Author          : Adrian Fiergolski
// Created On      : Thu Jun 30 17:19:37 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_ChargeInjection_driver
//ChargeInjection driver.
class CLICpix2_ChargeInjection_driver extends uvm_push_driver #(CLICpix2_Matrix_sequence_item);
   
   //Variable: inter
   vCLICpix2_Matrix_inter inter;

   //Variable: cfg
   CLICpix2_ChargeInjection_config cfg;

   //Variable: item_done
   // It is triggered when received word has been send.
   protected event item_done;

   //Typedef: TOA_T
   //Total TOA
   typedef bit [63:0] TOA_T;

   `uvm_component_utils_begin(CLICpix2_ChargeInjection_driver)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_ChargeInjection_driver> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   // Function: build_phase
   // Implements UVM build_phase.
   // It assigns interface.
   virtual function void  build_phase(uvm_phase phase);

      uvm_object temp;
      
      super.build_phase(phase);

      //Get configuration
      if( get_config_object("cfg", temp, 0) )
	assert( $cast(cfg, temp) );
      
      if( cfg == null )
	`uvm_fatal("NOCONFIG", $sformatf("No configuration set for the %s", get_name() ) );

      inter = cfg.inter;
   endfunction // build_phase

   // Task: put
   //
   // Implementation of the ~put~ task of the ~req_export~.
   // It assign the ~item~ do the req, sends the new packet into <SERDES_driver::debug_port> and waits for the event <item_done>.
   virtual task put(CLICpix2_Matrix_sequence_item item);
      this.req = item;
      @item_done;
   endtask // put

   // Task: run_phase
   //
   // Implements run phase of the uvm_driver.
   // It checks for a new transaction. When it happens, it calls <injectCharge>,
   virtual task run_phase(uvm_phase phase);
      forever begin
	 wait(req != null);
	 injectCharge();
	 req = null;
	 ->item_done;
      end
   endtask // run_phase

   //Task : injectCharge
   //The task inject charge to the CLICpix2.
   virtual task injectCharge();

      TOA_T max_toa; //maximum TOA in the given req.
      
      //Open shutter
      inter.cb.shutter <= 1'b1;
      @(inter.cb);
      
      #( $urandom_range(0, 500) * 1ns ); //wait some random time before injecting charge
      @(inter.cb);

      //find max_toa
      max_toa = find_max_toa();
      
      //spawns threads
      foreach ( req.pixels[row, col] ) begin
	 automatic int unsigned row_ = row;
	 automatic int unsigned col_ = col;
	 fork
	    begin
	       TOA_T toa_; //toa which takes into account long counter configuration

	       //find toa of the given pixel
	       toa_ = get_final_toa(row_, col_);
	       
	       inter.cb.discriminator_input[row_][col_] <= 1'b1; //default value
	       
	       if( req.pixels[row_][col_].r.flag) begin //only if the pixels should have a hit
		  repeat( max_toa-toa_ ) //wait the required time
		    @(inter.cb);
		  
		  if( inter.matrix[row_][col_].count_mode ) //if count mode
		    inject_events(row_, col_, toa_);
		  else
		    inject_single_event(row_, col_, toa_);
		  
	       end
	    end
	 join_none
      end
      wait fork;
      inter.cb.shutter <= 1'b0;
      @(inter.cb);
   endtask // injectCharge

   //Task: inject_single_event
   //The task injects a single event
   virtual task inject_single_event( int unsigned row, int unsigned col, TOA_T toa);
      TOA_T charge_tot;

      #( $urandom_range(1, CLICPIX2_ACQUISITION_CLOCK_PERIOD-1) *1ns); 
      inter.discriminator_input[row][col] = 1'b0; //inject charge

      if( inter.matrix[row][col].long_counter )  // long counter mode
	charge_tot = $urandom_range(1, toa);     //randomise TOT
      else                                       //otherwise
	charge_tot = req.pixels[row][col].r.tot; //it's given
      
      repeat (charge_tot)
	@inter.cb;
      #( $urandom_range(1, CLICPIX2_ACQUISITION_CLOCK_PERIOD-1) *1ns); 
      inter.discriminator_input[row][col] = 1'b1; //done
      
   endtask // inject_single_event

   //Task: inject_events
   //The task inject many events if pixel is in count mode
   virtual task inject_events(int unsigned row, int unsigned col, TOA_T toa);
      int unsigned counts; //number of events to be injected
      TOA_T ttot;    //total TOT
      bit [ CLICpix2_ChargeInjection_config::MAXDELAYBETWEENEVENTS_SIZE + TOT_SIZE + TOA_SIZE -1 : 0] tot[];
      
      if( inter.matrix[row][col].long_counter ) begin //long counter enabled
	 counts = { req.pixels[row][col].r.tot, req.pixels[row][col].r.toa };
	 ttot = $urandom_range(counts, toa);
      end
      else begin
	 counts = req.pixels[row][col].r.toa;
	 ttot = $urandom_range(counts + req.pixels[row][col].r.tot-1, toa);
      end
      
      tot = new[counts];
      foreach(tot[i])
	 tot[i] = 1;

      if( ! inter.matrix[row][col].long_counter ) begin //if long counter disabled
	 tot[0] = req.pixels[row][col].r.tot;
      end
      
      ttot-= tot.sum();
      while(ttot) begin
	 int unsigned idx = $urandom_range(1, counts-1);
	 tot[idx]++;
	 ttot--;
      end
      
      for(int i=0; i <counts; i++) begin
	 #( $urandom_range(1, CLICPIX2_ACQUISITION_CLOCK_PERIOD-1) *1ns); 
	 inter.discriminator_input[row][col] = 1'b0; //inject charge
	 repeat(tot[i])  //wait
	   @inter.cb;
	 #( $urandom_range(1, CLICPIX2_ACQUISITION_CLOCK_PERIOD-1) *1ns); 
	 inter.discriminator_input[row][col] = 1'b1; //inject charge
	 repeat( $urandom_range(1, cfg.MaxDelayBetwwenEvents) )
	   @inter.cb;
      end
      
   endtask

   //Function: find_max_toa
   //It find the first event which appears before deny of shutter
   virtual function TOA_T find_max_toa();
      find_max_toa = 0;
      foreach( req.pixels[row, col] ) begin
	 //toa which takes into account long counter configuration
	 TOA_T toa_ = get_final_toa(row, col); 

	 //if  bigger toa
	 if( find_max_toa < toa_ )
	   find_max_toa = toa_;

      end // foreach ( req.pixels[col, row] )

   endfunction // find_max_toa

   //Function: get_final_toa
   //The function returns the final toa for the given pixel taking into account its configuration
   function TOA_T get_final_toa( int unsigned row, int unsigned col );
      if( inter.matrix[row][col].long_counter ) //long counter enabled
	if( inter.matrix[row][col].count_mode ) //count mode for the given pixel is enabled
	  get_final_toa = { req.pixels[row][col].r.tot, req.pixels[row][col].r.toa } *  cfg.MaxDelayBetwwenEvents;
	else
	  get_final_toa = { req.pixels[row][col].r.tot, req.pixels[row][col].r.toa };
      
      else
	if( inter.matrix[row][col].count_mode ) //count mode for the given pixel is enabled
	  get_final_toa = req.pixels[row][col].r.toa *  cfg.MaxDelayBetwwenEvents;
	else
	  get_final_toa = req.pixels[row][col].r.toa;
   endfunction // get_final_toa
   
   
endclass // CLICpix2_ChargeInjection_driver
