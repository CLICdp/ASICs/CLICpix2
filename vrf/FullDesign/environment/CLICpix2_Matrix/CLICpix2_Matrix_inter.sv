//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_Matrix_inter.sv
// Description     : Interface controlling CLICpix2 matrix.
// Author          : Adrian Fiergolski
// Created On      : Thu Jun 30 16:07:50 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Interface: CLICpix2_Matrix_inter
//Interface controlling CLICpix2 matrix.

interface CLICpix2_Matrix_inter;

   import CLICpix2_Matrix_pkg::*;

   logic clock;
   logic reset_n;
   logic clkOutput;
   
   logic [127:0] discriminator_input[127:0] = '{ 128{ '1}};
   logic [127:0] testpulse_output[127:0];
   
   logic 	 shutter = 'b0;
   logic 	 testPulse = 'b0;
   logic 	 powerPulse = 'b0;
   
   clocking cb @(posedge clock);
      output 	 discriminator_input;
      output 	 shutter;
      output 	 testPulse;
   endclocking // cb


   clocking cbs @(posedge clock); //monitor (slave)
      input 	 discriminator_input;
      input 	 shutter;
      input 	 testPulse;
   endclocking // cbs

   //////////////////////////////////////////
   //Configuration of the Matrix
   //////////////////////////////////////////

   //values assigned directly from the DUT registers
   CONFIGURATION_PIXEL_LOGIC_T matrix[CLICpix2_Matrix_pkg::CLICPIX2_ROW][CLICpix2_Matrix_pkg::CLICPIX2_COL];
   bit 		 matrixIsBeingConfigured = 1'b0;

   //////////////////////////////////////////
   //Backdoor configuration of the Matrix
   //////////////////////////////////////////
   
   //Variable: b_matrix
   //Matrix to assign backdoor configuration
   CONFIGURATION_PIXEL_LOGIC_T b_matrix[CLICpix2_Matrix_pkg::CLICPIX2_ROW][CLICpix2_Matrix_pkg::CLICPIX2_COL];

   //Event: b_matrix_load
   //Enforces backdoor configuration
   event 	 b_matrix_load;
   task matrix_backdoorConfig(CLICpix2_Matrix_sequence_item matrix);

      //assign the <b_matrix> configuration
      foreach(b_matrix[r,c])
	b_matrix[r][c] = matrix.pixels[r][c].c;
      
      ->b_matrix_load;
   endtask // matrix_backdoorConfig

`ifndef netlist
   always @(b_matrix_load) begin
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.column_dataout <= 64'b0;
   end
`endif //  `ifndef netlist
   
`ifdef netlist

   parameter MATRIX_ASSIGN_DELAY = 1ns;
   
   //Interface: CLICpix2_Backdoor_inter
interface CLICpix2_Backdoor_inter;
   logic clk;
   logic eoc_u_out;

interface CLICpix2_Backdoor_pixel_inter (input clk);
   logic pr_cm_dataout;
   logic pr_cm_mask;
   logic [3:0]  pr_pr_config_reg;
   logic pr_pr_event_count;
   logic pr_pr_TP;
   logic pr_pr_long_counter;
   logic [7:0] pr_pr_TOA;
   logic [4:0] pr_pr_TOT;
   logic cd_clkout2;
   logic cd_clkout4;
   logic cd_clkout8;
endinterface // CLICpix2_Backdoor_pixel_inter

interface CLICpix2_Backdoor_superpixel_inter(input clk);
   logic     cmc_dataout;
   CLICpix2_Backdoor_pixel_inter pixel_l[1:8](clk);
   CLICpix2_Backdoor_pixel_inter pixel_r[1:8](clk);

endinterface

   CLICpix2_Backdoor_superpixel_inter superpixel[16](clk);
   
endinterface // CLICpix2_Backdoor_inter

   CLICpix2_Backdoor_inter backdoor_inter[64]();

   assign backdoor_inter[0].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_0_eoc.clkin_divided;
   assign backdoor_inter[1].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_1_eoc.clkin_divided;
   assign backdoor_inter[2].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_2_eoc.clkin_divided;
   assign backdoor_inter[3].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_3_eoc.clkin_divided;
   assign backdoor_inter[4].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_4_eoc.clkin_divided;
   assign backdoor_inter[5].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_5_eoc.clkin_divided;
   assign backdoor_inter[6].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_6_eoc.clkin_divided;
   assign backdoor_inter[7].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_7_eoc.clkin_divided;
   assign backdoor_inter[8].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_8_eoc.clkin_divided;
   assign backdoor_inter[9].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_9_eoc.clkin_divided;
   assign backdoor_inter[10].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_10_eoc.clkin_divided;
   assign backdoor_inter[11].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_11_eoc.clkin_divided;
   assign backdoor_inter[12].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_12_eoc.clkin_divided;
   assign backdoor_inter[13].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_13_eoc.clkin_divided;
   assign backdoor_inter[14].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_14_eoc.clkin_divided;
   assign backdoor_inter[15].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_15_eoc.clkin_divided;
   assign backdoor_inter[16].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_16_eoc.clkin_divided;
   assign backdoor_inter[17].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_17_eoc.clkin_divided;
   assign backdoor_inter[18].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_18_eoc.clkin_divided;
   assign backdoor_inter[19].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_19_eoc.clkin_divided;
   assign backdoor_inter[20].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_20_eoc.clkin_divided;
   assign backdoor_inter[21].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_21_eoc.clkin_divided;
   assign backdoor_inter[22].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_22_eoc.clkin_divided;
   assign backdoor_inter[23].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_23_eoc.clkin_divided;
   assign backdoor_inter[24].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_24_eoc.clkin_divided;
   assign backdoor_inter[25].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_25_eoc.clkin_divided;
   assign backdoor_inter[26].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_26_eoc.clkin_divided;
   assign backdoor_inter[27].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_27_eoc.clkin_divided;
   assign backdoor_inter[28].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_28_eoc.clkin_divided;
   assign backdoor_inter[29].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_29_eoc.clkin_divided;
   assign backdoor_inter[30].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_30_eoc.clkin_divided;
   assign backdoor_inter[31].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_31_eoc.clkin_divided;
   assign backdoor_inter[32].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_32_eoc.clkin_divided;
   assign backdoor_inter[33].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_33_eoc.clkin_divided;
   assign backdoor_inter[34].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_34_eoc.clkin_divided;
   assign backdoor_inter[35].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_35_eoc.clkin_divided;
   assign backdoor_inter[36].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_36_eoc.clkin_divided;
   assign backdoor_inter[37].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_37_eoc.clkin_divided;
   assign backdoor_inter[38].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_38_eoc.clkin_divided;
   assign backdoor_inter[39].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_39_eoc.clkin_divided;
   assign backdoor_inter[40].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_40_eoc.clkin_divided;
   assign backdoor_inter[41].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_41_eoc.clkin_divided;
   assign backdoor_inter[42].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_42_eoc.clkin_divided;
   assign backdoor_inter[43].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_43_eoc.clkin_divided;
   assign backdoor_inter[44].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_44_eoc.clkin_divided;
   assign backdoor_inter[45].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_45_eoc.clkin_divided;
   assign backdoor_inter[46].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_46_eoc.clkin_divided;
   assign backdoor_inter[47].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_47_eoc.clkin_divided;
   assign backdoor_inter[48].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_48_eoc.clkin_divided;
   assign backdoor_inter[49].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_49_eoc.clkin_divided;
   assign backdoor_inter[50].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_50_eoc.clkin_divided;
   assign backdoor_inter[51].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_51_eoc.clkin_divided;
   assign backdoor_inter[52].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_52_eoc.clkin_divided;
   assign backdoor_inter[53].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_53_eoc.clkin_divided;
   assign backdoor_inter[54].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_54_eoc.clkin_divided;
   assign backdoor_inter[55].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_55_eoc.clkin_divided;
   assign backdoor_inter[56].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_56_eoc.clkin_divided;
   assign backdoor_inter[57].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_57_eoc.clkin_divided;
   assign backdoor_inter[58].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_58_eoc.clkin_divided;
   assign backdoor_inter[59].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_59_eoc.clkin_divided;
   assign backdoor_inter[60].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_60_eoc.clkin_divided;
   assign backdoor_inter[61].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_61_eoc.clkin_divided;
   assign backdoor_inter[62].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_62_eoc.clkin_divided;
   assign backdoor_inter[63].clk = $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_63_eoc.clkin_divided;

//   always @(backdoor_inter[0].eoc_u_out)
//      $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_0_eoc.u.out_reg.mod.qint = backdoor_inter[0].eoc_u_out;


   always @(b_matrix_load) begin
      @(backdoor_inter[0].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_0_eoc.u.out_reg.mod.qint = backdoor_inter[0].eoc_u_out;
   end

   always @(b_matrix_load) begin
      @(backdoor_inter[0].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_0_eoc.u.out_reg.mod.qint = backdoor_inter[0].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[1].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_1_eoc.u.out_reg.mod.qint = backdoor_inter[1].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[2].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_2_eoc.u.out_reg.mod.qint = backdoor_inter[2].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[3].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_3_eoc.u.out_reg.mod.qint = backdoor_inter[3].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[4].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_4_eoc.u.out_reg.mod.qint = backdoor_inter[4].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[5].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_5_eoc.u.out_reg.mod.qint = backdoor_inter[5].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[6].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_6_eoc.u.out_reg.mod.qint = backdoor_inter[6].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[7].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_7_eoc.u.out_reg.mod.qint = backdoor_inter[7].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[8].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_8_eoc.u.out_reg.mod.qint = backdoor_inter[8].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[9].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_9_eoc.u.out_reg.mod.qint = backdoor_inter[9].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[10].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_10_eoc.u.out_reg.mod.qint = backdoor_inter[10].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[11].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_11_eoc.u.out_reg.mod.qint = backdoor_inter[11].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[12].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_12_eoc.u.out_reg.mod.qint = backdoor_inter[12].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[13].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_13_eoc.u.out_reg.mod.qint = backdoor_inter[13].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[14].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_14_eoc.u.out_reg.mod.qint = backdoor_inter[14].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[15].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_15_eoc.u.out_reg.mod.qint = backdoor_inter[15].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[16].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_16_eoc.u.out_reg.mod.qint = backdoor_inter[16].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[17].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_17_eoc.u.out_reg.mod.qint = backdoor_inter[17].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[18].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_18_eoc.u.out_reg.mod.qint = backdoor_inter[18].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[19].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_19_eoc.u.out_reg.mod.qint = backdoor_inter[19].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[20].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_20_eoc.u.out_reg.mod.qint = backdoor_inter[20].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[21].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_21_eoc.u.out_reg.mod.qint = backdoor_inter[21].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[22].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_22_eoc.u.out_reg.mod.qint = backdoor_inter[22].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[23].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_23_eoc.u.out_reg.mod.qint = backdoor_inter[23].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[24].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_24_eoc.u.out_reg.mod.qint = backdoor_inter[24].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[25].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_25_eoc.u.out_reg.mod.qint = backdoor_inter[25].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[26].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_26_eoc.u.out_reg.mod.qint = backdoor_inter[26].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[27].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_27_eoc.u.out_reg.mod.qint = backdoor_inter[27].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[28].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_28_eoc.u.out_reg.mod.qint = backdoor_inter[28].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[29].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_29_eoc.u.out_reg.mod.qint = backdoor_inter[29].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[30].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_30_eoc.u.out_reg.mod.qint = backdoor_inter[30].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[31].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_31_eoc.u.out_reg.mod.qint = backdoor_inter[31].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[32].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_32_eoc.u.out_reg.mod.qint = backdoor_inter[32].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[33].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_33_eoc.u.out_reg.mod.qint = backdoor_inter[33].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[34].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_34_eoc.u.out_reg.mod.qint = backdoor_inter[34].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[35].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_35_eoc.u.out_reg.mod.qint = backdoor_inter[35].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[36].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_36_eoc.u.out_reg.mod.qint = backdoor_inter[36].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[37].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_37_eoc.u.out_reg.mod.qint = backdoor_inter[37].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[38].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_38_eoc.u.out_reg.mod.qint = backdoor_inter[38].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[39].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_39_eoc.u.out_reg.mod.qint = backdoor_inter[39].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[40].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_40_eoc.u.out_reg.mod.qint = backdoor_inter[40].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[41].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_41_eoc.u.out_reg.mod.qint = backdoor_inter[41].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[42].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_42_eoc.u.out_reg.mod.qint = backdoor_inter[42].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[43].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_43_eoc.u.out_reg.mod.qint = backdoor_inter[43].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[44].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_44_eoc.u.out_reg.mod.qint = backdoor_inter[44].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[45].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_45_eoc.u.out_reg.mod.qint = backdoor_inter[45].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[46].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_46_eoc.u.out_reg.mod.qint = backdoor_inter[46].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[47].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_47_eoc.u.out_reg.mod.qint = backdoor_inter[47].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[48].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_48_eoc.u.out_reg.mod.qint = backdoor_inter[48].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[49].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_49_eoc.u.out_reg.mod.qint = backdoor_inter[49].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[50].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_50_eoc.u.out_reg.mod.qint = backdoor_inter[50].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[51].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_51_eoc.u.out_reg.mod.qint = backdoor_inter[51].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[52].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_52_eoc.u.out_reg.mod.qint = backdoor_inter[52].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[53].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_53_eoc.u.out_reg.mod.qint = backdoor_inter[53].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[54].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_54_eoc.u.out_reg.mod.qint = backdoor_inter[54].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[55].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_55_eoc.u.out_reg.mod.qint = backdoor_inter[55].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[56].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_56_eoc.u.out_reg.mod.qint = backdoor_inter[56].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[57].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_57_eoc.u.out_reg.mod.qint = backdoor_inter[57].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[58].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_58_eoc.u.out_reg.mod.qint = backdoor_inter[58].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[59].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_59_eoc.u.out_reg.mod.qint = backdoor_inter[59].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[60].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_60_eoc.u.out_reg.mod.qint = backdoor_inter[60].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[61].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_61_eoc.u.out_reg.mod.qint = backdoor_inter[61].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[62].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_62_eoc.u.out_reg.mod.qint = backdoor_inter[62].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      @(backdoor_inter[63].clk);
      #(MATRIX_ASSIGN_DELAY) $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1_63_eoc.u.out_reg.mod.qint = backdoor_inter[63].eoc_u_out;
   end
   
   always @(b_matrix_load) begin
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[0] .mod.qint <= 1'b0; 
      backdoor_inter[0].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[1] .mod.qint <= 1'b0; 
      backdoor_inter[1].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[2] .mod.qint <= 1'b0; 
      backdoor_inter[2].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[3] .mod.qint <= 1'b0; 
      backdoor_inter[3].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[4] .mod.qint <= 1'b0; 
      backdoor_inter[4].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[5] .mod.qint <= 1'b0; 
      backdoor_inter[5].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[6] .mod.qint <= 1'b0; 
      backdoor_inter[6].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[7] .mod.qint <= 1'b0; 
      backdoor_inter[7].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[8] .mod.qint <= 1'b0; 
      backdoor_inter[8].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[9] .mod.qint <= 1'b0; 
      backdoor_inter[9].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[10] .mod.qint <= 1'b0; 
      backdoor_inter[10].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[11] .mod.qint <= 1'b0; 
      backdoor_inter[11].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[12] .mod.qint <= 1'b0; 
      backdoor_inter[12].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[13] .mod.qint <= 1'b0; 
      backdoor_inter[13].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[14] .mod.qint <= 1'b0; 
      backdoor_inter[14].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[15] .mod.qint <= 1'b0; 
      backdoor_inter[15].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[16] .mod.qint <= 1'b0; 
      backdoor_inter[16].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[17] .mod.qint <= 1'b0; 
      backdoor_inter[17].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[18] .mod.qint <= 1'b0; 
      backdoor_inter[18].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[19] .mod.qint <= 1'b0; 
      backdoor_inter[19].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[20] .mod.qint <= 1'b0; 
      backdoor_inter[20].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[21] .mod.qint <= 1'b0; 
      backdoor_inter[21].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[22] .mod.qint <= 1'b0; 
      backdoor_inter[22].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[23] .mod.qint <= 1'b0; 
      backdoor_inter[23].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[24] .mod.qint <= 1'b0; 
      backdoor_inter[24].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[25] .mod.qint <= 1'b0; 
      backdoor_inter[25].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[26] .mod.qint <= 1'b0; 
      backdoor_inter[26].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[27] .mod.qint <= 1'b0; 
      backdoor_inter[27].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[28] .mod.qint <= 1'b0; 
      backdoor_inter[28].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[29] .mod.qint <= 1'b0; 
      backdoor_inter[29].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[30] .mod.qint <= 1'b0; 
      backdoor_inter[30].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[31] .mod.qint <= 1'b0; 
      backdoor_inter[31].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[32] .mod.qint <= 1'b0; 
      backdoor_inter[32].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[33] .mod.qint <= 1'b0; 
      backdoor_inter[33].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[34] .mod.qint <= 1'b0; 
      backdoor_inter[34].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[35] .mod.qint <= 1'b0; 
      backdoor_inter[35].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[36] .mod.qint <= 1'b0; 
      backdoor_inter[36].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[37] .mod.qint <= 1'b0; 
      backdoor_inter[37].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[38] .mod.qint <= 1'b0; 
      backdoor_inter[38].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[39] .mod.qint <= 1'b0; 
      backdoor_inter[39].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[40] .mod.qint <= 1'b0; 
      backdoor_inter[40].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[41] .mod.qint <= 1'b0; 
      backdoor_inter[41].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[42] .mod.qint <= 1'b0; 
      backdoor_inter[42].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[43] .mod.qint <= 1'b0; 
      backdoor_inter[43].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[44] .mod.qint <= 1'b0; 
      backdoor_inter[44].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[45] .mod.qint <= 1'b0; 
      backdoor_inter[45].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[46] .mod.qint <= 1'b0; 
      backdoor_inter[46].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[47] .mod.qint <= 1'b0; 
      backdoor_inter[47].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[48] .mod.qint <= 1'b0; 
      backdoor_inter[48].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[49] .mod.qint <= 1'b0; 
      backdoor_inter[49].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[50] .mod.qint <= 1'b0; 
      backdoor_inter[50].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[51] .mod.qint <= 1'b0; 
      backdoor_inter[51].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[52] .mod.qint <= 1'b0; 
      backdoor_inter[52].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[53] .mod.qint <= 1'b0; 
      backdoor_inter[53].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[54] .mod.qint <= 1'b0; 
      backdoor_inter[54].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[55] .mod.qint <= 1'b0; 
      backdoor_inter[55].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[56] .mod.qint <= 1'b0; 
      backdoor_inter[56].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[57] .mod.qint <= 1'b0; 
      backdoor_inter[57].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[58] .mod.qint <= 1'b0; 
      backdoor_inter[58].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[59] .mod.qint <= 1'b0; 
      backdoor_inter[59].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[60] .mod.qint <= 1'b0; 
      backdoor_inter[60].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[61] .mod.qint <= 1'b0; 
      backdoor_inter[61].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[62] .mod.qint <= 1'b0; 
      backdoor_inter[62].eoc_u_out <= 1'b0;
      $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_control.readout.\column_dataout_reg[63] .mod.qint <= 1'b0; 
      backdoor_inter[63].eoc_u_out <= 1'b0;
   end // always @ (b_matrix_load)
`endif //  `ifdef netlist
   
   for(genvar doubleColumn = 0; doubleColumn < 64; doubleColumn++) begin
`ifndef netlist
      always @(b_matrix_load) begin
         $root.CLICpix2_Simulation_top.CLICpix2.periphery.genblk1[doubleColumn].eoc.u.out <= 1'b0;
      end
`endif //  `ifndef netlist    
   for (genvar superPixel = 0; superPixel < 16; superPixel++) begin
`ifndef netlist
      always @(b_matrix_load) begin
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.cmc.dataout <= 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.cm.mask <= b_matrix[8*superPixel+7][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.pr.config_reg <= b_matrix[8*superPixel+7][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.pr.event_count <= b_matrix[8*superPixel+7][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.pr.TP <= b_matrix[8*superPixel+7][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.pr.long_counter <= b_matrix[8*superPixel+7][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr.pr.TOT <= 5'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.cm.mask <= b_matrix[8*superPixel+7][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.pr.config_reg <= b_matrix[8*superPixel+7][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.pr.event_count <= b_matrix[8*superPixel+7][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.pr.TP <= b_matrix[8*superPixel+7][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.pr.long_counter <= b_matrix[8*superPixel+7][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.cd.clkout8 <= #2 1'b0;
	 
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.cm.mask <= b_matrix[8*superPixel+6][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.pr.config_reg <= b_matrix[8*superPixel+6][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.pr.event_count <= b_matrix[8*superPixel+6][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.pr.TP <= b_matrix[8*superPixel+6][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.pr.long_counter <= b_matrix[8*superPixel+6][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.cd.clkout8 <= #2 1'b0;
	 
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.cm.mask <= b_matrix[8*superPixel+6][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.pr.config_reg <= b_matrix[8*superPixel+6][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.pr.event_count <= b_matrix[8*superPixel+6][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.pr.TP <= b_matrix[8*superPixel+6][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.pr.long_counter <= b_matrix[8*superPixel+6][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.cm.mask <= b_matrix[8*superPixel+5][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.pr.config_reg <= b_matrix[8*superPixel+5][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.pr.event_count <= b_matrix[8*superPixel+5][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.pr.TP <= b_matrix[8*superPixel+5][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.pr.long_counter <= b_matrix[8*superPixel+5][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.cd.clkout8 <= #2 1'b0;
	 
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.cm.mask <= b_matrix[8*superPixel+5][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.pr.config_reg <= b_matrix[8*superPixel+5][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.pr.event_count <= b_matrix[8*superPixel+5][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.pr.TP <= b_matrix[8*superPixel+5][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.pr.long_counter <= b_matrix[8*superPixel+5][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.cd.clkout8 <= #2 1'b0;
	 
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.cm.mask <= b_matrix[8*superPixel+4][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.pr.config_reg <= b_matrix[8*superPixel+4][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.pr.event_count <= b_matrix[8*superPixel+4][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.pr.TP <= b_matrix[8*superPixel+4][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.pr.long_counter <= b_matrix[8*superPixel+4][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.cm.mask <= b_matrix[8*superPixel+4][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.pr.config_reg <= b_matrix[8*superPixel+4][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.pr.event_count <= b_matrix[8*superPixel+4][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.pr.TP <= b_matrix[8*superPixel+4][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.pr.long_counter <= b_matrix[8*superPixel+4][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.cm.mask <= b_matrix[8*superPixel+3][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.pr.config_reg <= b_matrix[8*superPixel+3][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.pr.event_count <= b_matrix[8*superPixel+3][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.pr.TP <= b_matrix[8*superPixel+3][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.pr.long_counter <= b_matrix[8*superPixel+3][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.cm.mask <= b_matrix[8*superPixel+3][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.pr.config_reg <= b_matrix[8*superPixel+3][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.pr.event_count <= b_matrix[8*superPixel+3][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.pr.TP <= b_matrix[8*superPixel+3][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.pr.long_counter <= b_matrix[8*superPixel+3][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.cm.mask <= b_matrix[8*superPixel+2][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.pr.config_reg <= b_matrix[8*superPixel+2][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.pr.event_count <= b_matrix[8*superPixel+2][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.pr.TP <= b_matrix[8*superPixel+2][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.pr.long_counter <= b_matrix[8*superPixel+2][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.cm.mask <= b_matrix[8*superPixel+2][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.pr.config_reg <= b_matrix[8*superPixel+2][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.pr.event_count <= b_matrix[8*superPixel+2][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.pr.TP <= b_matrix[8*superPixel+2][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.pr.long_counter <= b_matrix[8*superPixel+2][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.cd.clkout8 <= #2 1'b0;
	 
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.cm.mask <= b_matrix[8*superPixel+1][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.pr.config_reg <= b_matrix[8*superPixel+1][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.pr.event_count <= b_matrix[8*superPixel+1][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.pr.TP <= b_matrix[8*superPixel+1][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.pr.long_counter <= b_matrix[8*superPixel+1][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.cm.mask <= b_matrix[8*superPixel+1][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.pr.config_reg <= b_matrix[8*superPixel+1][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.pr.event_count <= b_matrix[8*superPixel+1][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.pr.TP <= b_matrix[8*superPixel+1][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.pr.long_counter <= b_matrix[8*superPixel+1][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.cd.clkout8 <= #2 1'b0;
	 
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.cm.mask <= b_matrix[8*superPixel+0][2*doubleColumn].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.pr.config_reg <= b_matrix[8*superPixel+0][2*doubleColumn].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.pr.event_count <= b_matrix[8*superPixel+0][2*doubleColumn].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.pr.TP <= b_matrix[8*superPixel+0][2*doubleColumn].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.pr.long_counter <= b_matrix[8*superPixel+0][2*doubleColumn].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr.pr.TOT <= 5'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.cd.clkout8 <= #2 1'b0;

	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.cm.dataout <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.cm.mask <= b_matrix[8*superPixel+0][2*doubleColumn + 1].mask;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.pr.config_reg <= b_matrix[8*superPixel+0][2*doubleColumn + 1].th_adj;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.pr.event_count <= b_matrix[8*superPixel+0][2*doubleColumn + 1].count_mode;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.pr.TP <= b_matrix[8*superPixel+0][2*doubleColumn + 1].TP_en;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.pr.long_counter <= b_matrix[8*superPixel+0][2*doubleColumn +1].long_counter;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.pr.TOA <= 8'b0;   
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr.pr.TOT <= 5'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.cd.clkout2 <= 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.cd.clkout4 <= #1 1'b0;
	 $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.cd.clkout8 <= #2 1'b0;
      end // always @ (b_matrix_load)

`endif //  `ifndef netlist

`ifdef netlist

   always @(b_matrix_load) begin
      @(backdoor_inter[doubleColumn].clk);
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.cmc_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].cmc_dataout;
      
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.cmc_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].cmc_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl1.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[1].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr1.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[1].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl2.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[2].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr2.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[2].cd_clkout8;
   
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl3.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[3].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr3.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[3].cd_clkout8;
 
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl4.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[4].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr4.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[4].cd_clkout8;
  
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl5.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[5].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr5.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[5].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl6.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[6].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr6.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[6].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl7.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[7].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr7.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[7].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pl8.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[8].cd_clkout8;

      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_cm_dataout_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_cm_dataout;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_cm_mask_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_cm_mask;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\config_reg_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_config_reg[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\config_reg_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_config_reg[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\config_reg_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_config_reg[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\config_reg_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_config_reg[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.event_count_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_event_count;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.TP_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TP;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.long_counter_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_long_counter;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[5] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[5];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[6] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[6];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOA_reg[7] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOA[7];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOT_reg[0] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOT[0];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOT_reg[1] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOT[1];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOT_reg[2] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOT[2];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOT_reg[3] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOT[3];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.pr_pr.\TOT_reg[4] .mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].pr_pr_TOT[4];
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.cd_clkout2_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].cd_clkout2;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.cd_clkout4_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 1) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].cd_clkout4;
      $root.CLICpix2_Simulation_top.CLICpix2.genblk1[doubleColumn].double_column.genblk1[superPixel].superpixel.pr8.cd_clkout8_reg.mod.qint <= #(MATRIX_ASSIGN_DELAY + 2) backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[8].cd_clkout8;

   end // always @ (b_matrix_load)

   always @(b_matrix_load) 
     backdoor_inter[doubleColumn].superpixel[superPixel].cmc_dataout <= 1'b0;

   for(genvar i = 1; i<= 8 ; i++) begin
      always @(b_matrix_load) begin
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_cm_dataout <= 1'b0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_cm_mask <= b_matrix[8*superPixel+ 8 -i  ][2*doubleColumn].mask;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_pr_config_reg <= b_matrix[8*superPixel+8 -i  ][2*doubleColumn].th_adj;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_pr_event_count <=  b_matrix[8*superPixel+8 -i ][2*doubleColumn].count_mode;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_pr_TP <= b_matrix[8*superPixel+8 -i ][2*doubleColumn].TP_en;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_pr_long_counter <= b_matrix[8*superPixel+8 -i ][2*doubleColumn].long_counter;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_pr_TOA <= '0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].pr_pr_TOT <= '0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].cd_clkout2 <= 1'b0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].cd_clkout4 <= 1'b0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_l[i].cd_clkout8 <= 1'b0;

	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_cm_dataout <= 1'b0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_cm_mask <= b_matrix[8*superPixel+ 8 -i  ][2*doubleColumn + 1].mask;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_pr_config_reg <= b_matrix[8*superPixel+8 -i  ][2*doubleColumn + 1].th_adj;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_pr_event_count <=  b_matrix[8*superPixel+8 -i ][2*doubleColumn + 1].count_mode;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_pr_TP <= b_matrix[8*superPixel+8 -i ][2*doubleColumn + 1].TP_en;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_pr_long_counter <= b_matrix[8*superPixel+8 -i ][2*doubleColumn + 1].long_counter;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_pr_TOA <= '0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].pr_pr_TOT <= '0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].cd_clkout2 <= 1'b0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].cd_clkout4 <= 1'b0;
	 backdoor_inter[doubleColumn].superpixel[superPixel].pixel_r[i].cd_clkout8 <= 1'b0;
      end // always @ (b_matrix_load)
    end // for (genvar i = 1; i<= 8 ; i++)

`endif //  `ifdef netlist
	 
end // for (genvar superPixel = 0; superPixel < 16; superPixel++)
end // for (genvar doubleColumn = 0; doubleColumn < 64; doubleColumn++)

   
endinterface // CLICpix2_matrix_inter

