
//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_pkg.sv
// Description     : Package containing CLICpix2 environment.
// Author          : Adrian Fiergolski
// Created On      : Wed Jan 27 18:11:51 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef CLICPIX2_PKG_SV
 `define CLICPIX2_PKG_SV

//Title: CLICpix2_pkg

  `define CLICPIX2_REGISTERS_VALID_ADDRESSES \
         2,   4,   8,  10,  12,  14,  16,  18,  20,  22,  24,  26,  28,  30,  32,\
	34,  36,  38,  40,  42,  44,  46,  48,  50,  52,  54,  56,  58,  60,  62  
   
  `define CLICPIX2_REGISTERS_DEFAULT_VALUES \
          0,   0,   0,  30,  50,  80,  90,  64, 136, 133, 133, 133, 133,  30,  50, \
	 90,   0, 138,   0, 138, 133,   0,   0,   0,   0,   0,   0,   0,  88,  11 

  `define READOUT_ADDRESS 2
  `define MATRIX_PROGRAMMING_ADDRESS 4
  `define START_STOP_PRBS_ADDRESS 8
  `define GLOBAL_CONTROL_REGISTER 60
  `define GLOBAL_CONTROL_REGISTER_TP_GEN_EN 7	   
  `define GLOBAL_CONTROL_REGISTER_TOT_CLK_DIV 1:0	   
  `define READOUT_CONTROL_REGISTER_ADDRESS 62
  `define READOUT_CONTROL_REGISTER_SP_COMP_BIT 5
  `define READOUT_CONTROL_REGISTER_COMP_BIT 4
  `define READOUT_CONTROL_REGISTER_CLK_DIV_BIT 3:2
  `define READOUT_CONTROL_REGISTER_PARAL_COLS_BIT 1:0
	   
	   
 `define BGAP_ENABLE_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.config_global[6]
 `define BGAP_CONTROL_R3_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.config_global[5:3]
 `define BGAP_CONTROL_R1_REG  $root.CLICpix2_Simulation_top.CLICpix2.periphery.bgap_control_R1
 `define BIAS_DISC_N_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_disc_N
 `define BIAS_DISC_P_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_disc_P
 `define BIAS_IKRUM_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_ikrum
 `define BIAS_PREAMP_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_preamp
 `define BIAS_DAC_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_DAC
 `define BIAS_BUFFER_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_buffer
 `define CASC_PREAMP_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.casc_preamp
 `define CASC_DAC_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.casc_DAC
 `define CASC_NMIRROR_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.casc_Nmirror
 `define VFBK_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.vfbk
 `define VTHMSB_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.vthMSB
 `define VTHLSB_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.vthLSB
 `define TEST_CAP1_LSB_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.test_cap1_LSB
 `define TEST_CAP1_MSB_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.test_cap1_MSB
 `define TEST_CAP2_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.test_cap2
 `define BIAS_DISC_N_OFF_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_disc_N_OFF
 `define BIAS_DISC_P_OFF_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_disc_P_OFF
 `define BIAS_PREAMP_OFF_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.bias_preamp_OFF
 `define OUTPUT_MUX_DAC_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.dac_control.output_mux_DAC
 `define CLK_DIV_REG $root.CLICpix2_Simulation_top.CLICpix2.periphery.readout_config[3:2]
	   

//Package: CLICpix2_pkg
//This package contains <CLICpix2_env>
package CLICpix2_pkg;

   import uvm_pkg::*;
 `include <uvm_macros.svh>
   
   import BasicBlocks_pkg::*;
   import SPI_pkg::*;
   import SerDes_pkg::*;
   import CLICpix2_Matrix_pkg::*;

   parameter CLOCK_PERIOD = 3.125ns;
   
   parameter SPI_SS_WIDTH = 1;
   parameter SPI_PERIOD = 10ns;
   typedef spi_vip_config#(SPI_SS_WIDTH) spi_CLICpix2_vip_config;
   typedef SPI_env#(SPI_SS_WIDTH) SPI_CLICpix2_env;
   typedef virtual mgc_spi#(SPI_SS_WIDTH) vSPI_inter;

   //Typedef: vCLICpix2_ClockReset
   typedef virtual interface CLICpix2_ClockReset vCLICpix2_ClockReset;

   //Typedef: vCLICpix2_outputs
   typedef virtual interface CLICpix2_outputs vCLICpix2_outputs;

   //Typedef: vCLICpix2_matrix
   typedef virtual interface CLICpix2_matrix vCLICpix2_matrix;
   
 `include "CLICpix2_scoreboard.sv"
 `include "CLICpix2_config.sv"
 `include "SPI_CLICpix2_sqr.sv"
 `include "CLICpix2_spi_master_data_transfer.sv"
 `include "CLICpix2_SPI_scoreboard.sv"
 `include "CLICpix2_SPI_Coverage_scoreboard.sv"
 `include "CLICpix2_MatrixConfiguration_monitor.sv"
 `include "CLICpix2_MatrixConfiguration_scoreboard.sv"
 `include "CLICpix2_MatrixConfiguration_Coverage_scoreboard.sv"
 `include "CLICpix2_Readout_scoreboard.sv"
 `include "CLICpix2_Readout_Coverage_scoreboard.sv"
 `include "CLICpix2_virtual_sqr.sv"
 `include "CLICpix2_env.sv"   

endpackage // CLICpix2_pkg

//Interface: CLICpix2_outputs
interface CLICpix2_outputs;
   logic 	   reset;
   logic bgap_enable, bgap_enable_ass, bgap_enable_n;
   logic [2:0] bgap_control_R3, bgap_control_R3_ass;
   logic [2:0] bgap_control_R1, bgap_control_R1_ass;
   
   logic [7:0] bias_disc_N,bias_disc_N_ass, bias_disc_N_n;
   logic [7:0] bias_disc_P, bias_disc_P_ass, bias_disc_P_n;
   logic [7:0] bias_ikrum, bias_ikrum_ass, bias_ikrum_n;
   logic [7:0] bias_preamp, bias_preamp_ass, bias_preamp_n;
   logic [7:0] bias_DAC, bias_DAC_ass, bias_DAC_n;
   logic [7:0] bias_buffer, bias_buffer_ass, bias_buffer_n;
   logic [7:0] casc_preamp, casc_preamp_ass, casc_preamp_n;
   logic [7:0] casc_DAC, casc_DAC_ass, casc_DAC_n;
   logic [7:0] casc_Nmirror, casc_Nmirror_ass, casc_Nmirror_n;
   logic [7:0] vfbk, vfbk_ass, vfbk_n;
   logic [7:0] vthMSB, vthMSB_ass, vthMSB_n;
   logic [7:0] vthLSB, vthLSB_ass, vthLSB_n;
   logic [7:0] test_cap1_LSB, test_cap1_LSB_ass, test_cap1_LSB_n;
   logic [7:0] test_cap1_MSB, test_cap1_MSB_ass, test_cap1_MSB_n;
   logic [7:0] test_cap2, test_cap2_ass, test_cap2_n; 
   logic [7:0] bias_disc_N_OFF, bias_disc_N_OFF_ass, bias_disc_N_OFF_n;
   logic [7:0] bias_disc_P_OFF, bias_disc_P_OFF_ass, bias_disc_P_OFF_n;
   logic [7:0] bias_preamp_OFF, bias_preamp_OFF_ass, bias_preamp_OFF_n;
   logic [21:0] output_mux_DAC, output_mux_DAC_ass, output_mux_DAC_n;

 `ifndef netlist
   parameter ASSERTION_ASSIGN = 1;
 `else
   parameter ASSERTION_ASSIGN = 3ns;
 `endif

   assign #(ASSERTION_ASSIGN) bgap_enable_ass = bgap_enable;
   assign #(ASSERTION_ASSIGN) bgap_control_R3_ass = bgap_control_R3;
   assign #(ASSERTION_ASSIGN) bgap_control_R1_ass = bgap_control_R1;
   assign #(ASSERTION_ASSIGN) bias_disc_N_ass = bias_disc_N;
   assign #(ASSERTION_ASSIGN) bias_disc_P_ass = bias_disc_P;
   assign #(ASSERTION_ASSIGN) bias_ikrum_ass = bias_ikrum;
   assign #(ASSERTION_ASSIGN) bias_preamp_ass = bias_preamp;
   assign #(ASSERTION_ASSIGN) bias_DAC_ass = bias_DAC;
   assign #(ASSERTION_ASSIGN) bias_buffer_ass = bias_buffer;
   assign #(ASSERTION_ASSIGN) casc_preamp_ass = casc_preamp;
   assign #(ASSERTION_ASSIGN) casc_DAC_ass = casc_DAC;
   assign #(ASSERTION_ASSIGN) casc_Nmirror_ass = casc_Nmirror;
   assign #(ASSERTION_ASSIGN) vfbk_ass = vfbk;
   assign #(ASSERTION_ASSIGN) vthMSB_ass = vthMSB;
   assign #(ASSERTION_ASSIGN) vthLSB_ass = vthLSB;
   assign #(ASSERTION_ASSIGN) test_cap1_LSB_ass = test_cap1_LSB;
   assign #(ASSERTION_ASSIGN) test_cap1_MSB_ass = test_cap1_MSB;
   assign #(ASSERTION_ASSIGN) test_cap2_ass = test_cap2;; 
   assign #(ASSERTION_ASSIGN) bias_disc_N_OFF_ass = bias_disc_N_OFF;
   assign #(ASSERTION_ASSIGN) bias_disc_P_OFF_ass = bias_disc_P_OFF;
   assign #(ASSERTION_ASSIGN) bias_preamp_OFF_ass = bias_preamp_OFF;
   assign #(ASSERTION_ASSIGN) output_mux_DAC_ass = output_mux_DAC;

   ///////////////
   //Assertions
   ///////////////
   negated_output_bgap_enable: assert property( @(bgap_enable_ass or reset ==0) disable iff(reset) bgap_enable == ~ bgap_enable_n );
   negated_output_bias_disc_N: assert property( @(bias_disc_N_ass or reset ==0) disable iff(reset) bias_disc_N == ~ bias_disc_N_n );
   negated_output_bias_disc_P: assert property( @(bias_disc_P_ass or reset ==0) disable iff(reset) bias_disc_P == ~ bias_disc_P_n );
   negated_output_bias_ikrum: assert property( @(bias_ikrum_ass or reset ==0) disable iff(reset) bias_ikrum == ~ bias_ikrum_n );
   negated_output_bias_preamp: assert property( @(bias_preamp_ass or reset ==0) disable iff(reset) bias_preamp == ~ bias_preamp_n );
   negated_output_bias_DAC: assert property( @(bias_DAC_ass or reset ==0) disable iff(reset) bias_DAC == ~ bias_DAC_n );
   negated_output_bias_buffer: assert property( @(bias_buffer_ass or reset ==0) disable iff(reset) bias_buffer == ~ bias_buffer_n );
   negated_output_casc_preamp: assert property( @(casc_preamp_ass or reset ==0) disable iff(reset) casc_preamp == ~ casc_preamp_n );
   negated_output_casc_DAC: assert property( @(casc_DAC_ass or reset ==0) disable iff(reset) casc_DAC == ~ casc_DAC_n );
   negated_output_casc_Nmirror: assert property( @(casc_Nmirror_ass or reset ==0) disable iff(reset) casc_Nmirror == ~ casc_Nmirror_n );
   negated_output_vfbk: assert property( @(vfbk_ass or reset ==0) disable iff(reset) vfbk == ~ vfbk_n );
   negated_output_vthMSB: assert property( @(vthMSB_ass or reset ==0) disable iff(reset) vthMSB == ~ vthMSB_n );
   negated_output_vthLSB: assert property( @(vthLSB_ass or reset ==0) disable iff(reset) vthLSB == ~ vthLSB_n );
   negated_output_test_cap1_LSB: assert property( @(test_cap1_LSB_ass or reset ==0) disable iff(reset) test_cap1_LSB == ~ test_cap1_LSB_n );
   negated_output_test_cap1_MSB: assert property( @(test_cap1_MSB_ass or reset ==0) disable iff(reset) test_cap1_MSB == ~ test_cap1_MSB_n );
   negated_output_test_cap2: assert property( @(test_cap2_ass or reset ==0) disable iff(reset) test_cap2 == ~ test_cap2_n );
   negated_output_bias_disc_N_OFF: assert property( @(bias_disc_N_OFF_ass or reset ==0) disable iff(reset) bias_disc_N_OFF == ~ bias_disc_N_OFF_n );
   negated_output_bias_disc_P_OFF: assert property( @(bias_disc_P_OFF_ass or reset ==0) disable iff(reset) bias_disc_P_OFF == ~ bias_disc_P_OFF_n );
   negated_output_bias_preamp_OFF: assert property( @(bias_preamp_OFF_ass or reset ==0) disable iff(reset) bias_preamp_OFF == ~ bias_preamp_OFF_n );
   negated_output_output_mux_DAC: assert property( @(output_mux_DAC_ass or reset == 0) disable iff(reset)  output_mux_DAC == ~ output_mux_DAC_n );

   bit	bgap_enable_reg;
   assign #(ASSERTION_ASSIGN) bgap_enable_reg = `BGAP_ENABLE_REG;
   register_to_output_bgap_enable: assert property( @(bgap_enable_reg or reset == 0) disable iff(reset)  1 |-> bgap_enable == `BGAP_ENABLE_REG );
   output_to_register_bgap_enable: assert property( @(bgap_enable_ass or reset == 0) disable iff(reset)  1 |-> bgap_enable == `BGAP_ENABLE_REG );

   bit[2:0]	bgap_control_R3_reg;
   assign #(ASSERTION_ASSIGN) bgap_control_R3_reg = `BGAP_CONTROL_R3_REG;
   register_to_output_bgap_control_R3: assert property( @(bgap_control_R3_reg or reset == 0) disable iff(reset)  1 |-> bgap_control_R3 == `BGAP_CONTROL_R3_REG );
   output_to_register_bgap_control_R3: assert property( @(bgap_control_R3_ass or reset == 0) disable iff(reset)  1 |-> bgap_control_R3 == `BGAP_CONTROL_R3_REG );

   bit [2:0] 	bgap_control_R1_reg;
   assign #(ASSERTION_ASSIGN) bgap_control_R1_reg = `BGAP_CONTROL_R1_REG;
   register_to_output_bgap_control_R1: assert property( @(bgap_control_R1_reg or reset == 0) disable iff(reset)  1 |-> bgap_control_R1 == `BGAP_CONTROL_R1_REG );
   output_to_register_bgap_control_R1: assert property( @(bgap_control_R1_ass or reset == 0) disable iff(reset)  1 |-> bgap_control_R1 == `BGAP_CONTROL_R1_REG );
   
   byte 	bias_disc_N_reg;
   assign #(ASSERTION_ASSIGN) bias_disc_N_reg = `BIAS_DISC_N_REG;
   register_to_output_bias_disc_N: assert property( @(bias_disc_N_reg or reset == 0) disable iff(reset)  1 |-> bias_disc_N == `BIAS_DISC_N_REG );
   output_to_register_bias_disc_N: assert property( @(bias_disc_N_ass or reset == 0) disable iff(reset)  1 |-> bias_disc_N == `BIAS_DISC_N_REG );
   
   byte 	bias_disc_P_reg;
   assign #(ASSERTION_ASSIGN) bias_disc_P_reg = `BIAS_DISC_P_REG;
   register_to_output_bias_disc_P: assert property( @(bias_disc_P_reg or reset == 0) disable iff(reset)  1 |-> bias_disc_P == `BIAS_DISC_P_REG );
   output_to_register_bias_disc_P: assert property( @(bias_disc_P_ass or reset == 0) disable iff(reset)  1 |-> bias_disc_P == `BIAS_DISC_P_REG );

   byte 	bias_ikrum_reg;
   assign #(ASSERTION_ASSIGN) bias_ikrum_reg = `BIAS_IKRUM_REG;
   register_to_output_bias_ikrum: assert property( @(bias_ikrum_reg or reset == 0) disable iff(reset)  1 |-> bias_ikrum == `BIAS_IKRUM_REG );
   output_to_register_bias_ikrum: assert property( @(bias_ikrum_ass or reset == 0) disable iff(reset)  1 |-> bias_ikrum == `BIAS_IKRUM_REG );

   byte 	bias_preamp_reg;
   assign #(ASSERTION_ASSIGN) bias_preamp_reg = `BIAS_PREAMP_REG;
   register_to_output_bias_preamp: assert property( @(bias_preamp_reg or reset == 0) disable iff(reset)  1 |-> bias_preamp == `BIAS_PREAMP_REG );
   output_to_register_bias_preamp: assert property( @(bias_preamp_ass or reset == 0) disable iff(reset)  1 |-> bias_preamp == `BIAS_PREAMP_REG );

   byte 	bias_DAC_reg;
   assign #(ASSERTION_ASSIGN) bias_DAC_reg = `BIAS_DAC_REG;
   register_to_output_bias_DAC: assert property( @(bias_DAC_reg or reset == 0) disable iff(reset)  1 |-> bias_DAC == `BIAS_DAC_REG );
   output_to_register_bias_DAC: assert property( @(bias_DAC_ass or reset == 0) disable iff(reset)  1 |-> bias_DAC == `BIAS_DAC_REG );

   byte 	bias_buffer_reg;
   assign #(ASSERTION_ASSIGN) bias_buffer_reg = `BIAS_BUFFER_REG;
   register_to_output_bias_buffer: assert property( @(bias_buffer_reg or reset == 0) disable iff(reset)  1 |-> bias_buffer == `BIAS_BUFFER_REG );
   output_to_register_bias_buffer: assert property( @(bias_buffer_ass or reset == 0) disable iff(reset)  1 |-> bias_buffer == `BIAS_BUFFER_REG );

   byte 	casc_preamp_reg;
   assign #(ASSERTION_ASSIGN) casc_preamp_reg = `CASC_PREAMP_REG;
   register_to_output_casc_preamp: assert property( @(casc_preamp_reg or reset == 0) disable iff(reset)  1 |-> casc_preamp == `CASC_PREAMP_REG );
   output_to_register_casc_preamp: assert property( @(casc_preamp_ass or reset == 0) disable iff(reset)  1 |-> casc_preamp == `CASC_PREAMP_REG );

   byte 	casc_DAC_reg;
   assign #(ASSERTION_ASSIGN) casc_DAC_reg = `CASC_DAC_REG;
   register_to_output_casc_DAC: assert property( @(casc_DAC_reg or reset == 0) disable iff(reset)  1 |-> casc_DAC == `CASC_DAC_REG );
   output_to_register_casc_DAC: assert property( @(casc_DAC_ass or reset == 0) disable iff(reset)  1 |-> casc_DAC == `CASC_DAC_REG );

   byte 	casc_Nmirror_reg;
   assign #(ASSERTION_ASSIGN) casc_Nmirror_reg = `CASC_NMIRROR_REG;
   register_to_output_casc_Nmirror: assert property( @(casc_Nmirror_reg or reset == 0) disable iff(reset)  1 |-> casc_Nmirror == `CASC_NMIRROR_REG );
   output_to_register_casc_Nmirror: assert property( @(casc_Nmirror_ass or reset == 0) disable iff(reset)  1 |-> casc_Nmirror == `CASC_NMIRROR_REG );

   byte 	vfbk_reg;
   assign #(ASSERTION_ASSIGN) vfbk_reg = `VFBK_REG;
   register_to_output_vfbk: assert property( @(vfbk_reg or reset == 0) disable iff(reset)  1 |-> vfbk == `VFBK_REG );
   output_to_register_vfbk: assert property( @(vfbk_ass or reset == 0) disable iff(reset)  1 |-> vfbk == `VFBK_REG );

   byte 	vthMSB_reg;
   assign #(ASSERTION_ASSIGN) vthMSB_reg = `VTHMSB_REG;
   register_to_output_vthMSB: assert property( @(vthMSB_reg or reset == 0) disable iff(reset)  1 |-> vthMSB == `VTHMSB_REG );
   output_to_register_vthMSB: assert property( @(vthMSB_ass or reset == 0) disable iff(reset)  1 |-> vthMSB == `VTHMSB_REG );

   byte 	vthLSB_reg;
   assign #(ASSERTION_ASSIGN) vthLSB_reg = `VTHLSB_REG;
   register_to_output_vthLSB: assert property( @(vthLSB_reg or reset == 0) disable iff(reset)  1 |-> vthLSB == `VTHLSB_REG );
   output_to_register_vthLSB: assert property( @(vthLSB_ass or reset == 0) disable iff(reset)  1 |-> vthLSB == `VTHLSB_REG );

   byte 	test_cap1_LSB_reg;
   assign #(ASSERTION_ASSIGN) test_cap1_LSB_reg = `TEST_CAP1_LSB_REG;
   register_to_output_test_cap1_LSB: assert property( @(test_cap1_LSB_reg or reset == 0) disable iff(reset)  1 |-> test_cap1_LSB == `TEST_CAP1_LSB_REG );
   output_to_register_test_cap1_LSB: assert property( @(test_cap1_LSB_ass or reset == 0) disable iff(reset)  1 |-> test_cap1_LSB == `TEST_CAP1_LSB_REG );

   byte 	test_cap1_MSB_reg;
   assign #(ASSERTION_ASSIGN) test_cap1_MSB_reg = `TEST_CAP1_MSB_REG;
   register_to_output_test_cap1_MSB: assert property( @(test_cap1_MSB_reg or reset == 0) disable iff(reset)  1 |-> test_cap1_MSB == `TEST_CAP1_MSB_REG );
   output_to_register_test_cap1_MSB: assert property( @(test_cap1_MSB_ass or reset == 0) disable iff(reset)  1 |-> test_cap1_MSB == `TEST_CAP1_MSB_REG );

   byte 	test_cap2_reg;
   assign #(ASSERTION_ASSIGN) test_cap2_reg = `TEST_CAP2_REG;
   register_to_output_test_cap2: assert property( @(test_cap2_reg or reset == 0) disable iff(reset)  1 |-> test_cap2 == `TEST_CAP2_REG );
   output_to_register_test_cap2: assert property( @(test_cap2_ass or reset == 0) disable iff(reset)  1 |-> test_cap2 == `TEST_CAP2_REG );

   byte 	bias_disc_N_OFF_reg;
   assign #(ASSERTION_ASSIGN) bias_disc_N_OFF_reg = `BIAS_DISC_N_OFF_REG;
   register_to_output_bias_disc_N_OFF: assert property( @(bias_disc_N_OFF_reg or reset == 0) disable iff(reset)  1 |-> bias_disc_N_OFF == `BIAS_DISC_N_OFF_REG );
   output_to_register_bias_disc_N_OFF: assert property( @(bias_disc_N_OFF_ass or reset == 0) disable iff(reset)  1 |-> bias_disc_N_OFF == `BIAS_DISC_N_OFF_REG );

   byte 	bias_disc_P_OFF_reg;
   assign #(ASSERTION_ASSIGN) bias_disc_P_OFF_reg = `BIAS_DISC_P_OFF_REG;
   register_to_output_bias_disc_P_OFF: assert property( @(bias_disc_P_OFF_reg or reset == 0) disable iff(reset)  1 |-> bias_disc_P_OFF == `BIAS_DISC_P_OFF_REG );
   output_to_register_bias_disc_P_OFF: assert property( @(bias_disc_P_OFF_ass or reset == 0) disable iff(reset)  1 |-> bias_disc_P_OFF == `BIAS_DISC_P_OFF_REG );

   byte 	bias_preamp_OFF_reg;
   assign #(ASSERTION_ASSIGN) bias_preamp_OFF_reg = `BIAS_PREAMP_OFF_REG;
   register_to_output_bias_preamp_OFF: assert property( @(bias_preamp_OFF_reg or reset == 0) disable iff(reset)  1 |-> bias_preamp_OFF == `BIAS_PREAMP_OFF_REG );
   output_to_register_bias_preamp_OFF: assert property( @(bias_preamp_OFF_ass or reset == 0) disable iff(reset)  1 |-> bias_preamp_OFF == `BIAS_PREAMP_OFF_REG );

   bit [21:0] 	output_mux_DAC_reg;
   assign #(ASSERTION_ASSIGN) output_mux_DAC_reg = `OUTPUT_MUX_DAC_REG;
   register_to_output_output_mux_DAC: assert property( @(output_mux_DAC_reg or reset == 0) disable iff(reset)  1 |-> output_mux_DAC == `OUTPUT_MUX_DAC_REG );
   output_to_register_output_mux_DAC: assert property( @(output_mux_DAC_ass or reset == 0) disable iff(reset)  1 |-> output_mux_DAC == `OUTPUT_MUX_DAC_REG );

endinterface // CLICpix2_outputs


   
//Interface: CLICpix2_ClockReset
//Interface used to access reset
interface CLICpix2_ClockReset;

   logic 	   reset_n;
   logic 	   clk = 0;

   initial 
     forever
       #(CLICpix2_pkg::CLOCK_PERIOD/2.0) clk = ~clk;

   clocking cb @(posedge clk);
      default output #2ns;
      output 	   reset_n;
   endclocking // cb

endinterface // CLICpix2_ClockReset
   
`endif
