//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_SPI_Coverage_scoreboard.sv
// Description     : Extension of the CLICpix2_SPI_scoreboard.
// Author          : Adrian Fiergolski
// Created On      : Fri Feb 26 16:38:06 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_SPI_Coverage_scoreboard
//Extension of the <C3PD_I2C_scoreboard> adding coverage elements.
class CLICpix2_SPI_Coverage_scoreboard extends CLICpix2_SPI_scoreboard;

   //Variable: hit_arr
   //The array is cleared with zeros after any reset.
   //When a register is read, the bit with the corresponding index in <hit_arr> changes from 0 to 1.
   protected bit hit_arr[int unsigned] = '{default: 1'b0};

   //Covergroup: SPI_access_cg
   //The coverage group collects statistics related to the SPI access
   covergroup SPI_access_cg;
      first_readout: coverpoint tr.address iff (hit_arr[tr.address] == 0 ) {
	 bins addresses[] = { `CLICPIX2_REGISTERS_VALID_ADDRESSES };
      }
      second_readout: coverpoint tr.address iff (hit_arr[tr.address] == 1) {
	 bins addresses[] =  { `CLICPIX2_REGISTERS_VALID_ADDRESSES };
      }

      dummy_address: coverpoint tr.address {
	 ignore_bins ig[] = { `CLICPIX2_REGISTERS_VALID_ADDRESSES };
      }
      
   endgroup // SPI_access_cg

   `uvm_component_utils_begin(CLICpix2_SPI_Coverage_scoreboard)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_SPI_Coverage_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
      SPI_access_cg = new();
      SPI_access_cg.set_inst_name( "SPI_access_cg" );
   endfunction // new

   //Function: mem_arr_init
   //On top of super class, it resets <hit_arr>.
   virtual function void mem_arr_init();
      hit_arr.delete();
      super.mem_arr_init();
   endfunction // mem_arr_init

   //Function: read_mem_arr
   //It updates coverage.
   virtual function logic[7:0] read_mem_arr(input byte pointer);
      read_mem_arr =  super.read_mem_arr(pointer);
      SPI_access_cg.sample;
      if( pointer inside {CLICpix2_config::registers_ValidAddresses} )
	hit_arr[tr.address] = 1'b1;
   endfunction // read_mem_arr
   
endclass // CLICpix2_SPI_Coverage_scoreboard
