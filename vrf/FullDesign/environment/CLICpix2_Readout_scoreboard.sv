//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_Readout_scoreboard.sv
// Description     : The scoreboard of the SerDes interface.
// Author          : Adrian Fiergolski
// Created On      : Wed Jun 29 18:25:08 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: CLICpix2_Readout_scoreboard

//Class: CLICpix2_Readout_scoreboard_config
//The configuration of the <CLICpix2_Readout_scoreboard> scoreboard.
class CLICpix2_Readout_scoreboard_config extends scoreboard_base_config;

   //Variable: resetInter
   vCLICpix2_ClockReset resetInter;
   //Variable: matrixInter
   vCLICpix2_Matrix_inter matrixInter;

   //Variable: spi_sqr
   SPI_CLICpix2_sqr  spi_sqr;
   
   `uvm_object_utils_begin(CLICpix2_Readout_scoreboard_config)
   `uvm_object_utils_end

   //Variable: spi_scoreboard
   //Address of the SPI scoreboard.
   //Required by the virtual sequencer in order to check Register Model.
   CLICpix2_SPI_scoreboard spi_scoreboard;
   
   //Function: new
   //Creates a new <CLICpix2_Readout_scoreboard_config> with the given ~name~.
   function new(string name="CLICpix2_Readout_scoreboard_config");
      super.new(name);
   endfunction // new
   
endclass // CLICpix2_Readout_scoreboard_config

//Class: CLICpix2_Readout_scoreboard
//The class implements the CLICpix2 SerDes scoreboards.
//It uses the following ports:
//- "SerDes"
class CLICpix2_Readout_scoreboard extends scoreboard_fifos;

   //Variable: cfg_
   CLICpix2_Readout_scoreboard_config cfg_;

   //Variable: resetInter
   vCLICpix2_ClockReset resetInter;

   //Variable: matrixInter
   vCLICpix2_Matrix_inter matrixInter;

   //Variable: expected_pixels
   READOUT_PIXEL_T expected_pixels[CLICPIX2_ROW][CLICPIX2_COL];

   //Variable: received_pixels
   READOUT_PIXEL_T received_pixels[CLICPIX2_ROW][CLICPIX2_COL];

   //Section: Validation

   //Variable: rcr
   //RCR register extracted from the current SerDes stream
   protected bit [1:0] rcr;

   //Variable: firstColumn
   //First column read for the given    
   protected byte firstColumn;

   //Variable: totClkDiv
   //It contains the value of the TOT clock divider.
   protected bit [1:0] totClkDiv;
   
   //Constant: DELIMETER
   const WORD_TYPE DELIMETER = '{ 1'b1, 8'hF7 };  //K23.7

   //Constant: lfsr5
`include "CLICpix2_Readout_scoreboard_lfsr5.svh"
   
   //Constant: lfsr8
`include "CLICpix2_Readout_scoreboard_lfsr8.svh" 

   //Constant: lfsr13
`include "CLICpix2_Readout_scoreboard_lfsr13.svh"   
   
   //Section: Statistics

   //Variable: total_cnt
   //Count number of received SerDes readout transactions.
   int 		  unsigned total_cnt = 0;

   `uvm_component_utils_begin(CLICpix2_Readout_scoreboard)
   `uvm_component_utils_end


   //Function: new
   //Creates a new <CLICpix2_Readout_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Function: build_phase
   //Retrieve the configuration and the interfaces.
   function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      assert( $cast( cfg_, cfg) );
      if(cfg_ == null)
	`uvm_error("NO_CONFIG", "Can't configure the scoreboard");
      
      resetInter = cfg_.resetInter;
      matrixInter = cfg_.matrixInter;
   endfunction // build_phase

   //Task: receive
   //The task selects the proper receive_* method.
   virtual task receive(string port, uvm_sequence_item transaction_);
      SerDes_sequence_item serdes_pkg;
      CLICpix2_Matrix_sequence_item matrix_pkg;

      case(port)
	"SER.master": begin
	   assert($cast(serdes_pkg, transaction_) );
	   if(cfg_.spi_sqr.prbs_on ) begin
	      receive_prbs_pkg(serdes_pkg);
	      cfg_.spi_sqr.prbs_on = 1'b0;
	   end
	   else if ( cfg_.spi_sqr.rcr_access == 1'b0 )  begin //not a fake packet due do RCR change
	      receive_serdes_pkg(serdes_pkg);
	      clean();
	   end
	end
	"CHA.master" : begin
	   assert($cast(matrix_pkg, transaction_) );
	   receive_charge_pkg(matrix_pkg);
	end
	"SPI.config" : begin
	   assert($cast(matrix_pkg, transaction_) );
	   receive_MatrixConfig_pkg(matrix_pkg);
	end
	default: super.receive(port, transaction_);
      endcase // case (port)

   endtask // receive

   //Function: receive_serdes_pkg
   //The function proceses Serial readout transactions.
   //It calls <decode_serdes_pkg> and compare pixels.
   virtual function void receive_serdes_pkg(SerDes_sequence_item  pkg);
      decode_serdes_pkg(pkg);
      compare_pixels();
   endfunction // receive_pkg

   //Function: clean
   //Prepare instance for reception of another Serdes readout package
   virtual function void clean();
      //after readout pixels are overwritten with 1'b0
      foreach( expected_pixels[r,c] )
	expected_pixels[r][c] = '0;
   endfunction // clean
   
   //Function: receive_prbs_pkg
   //The function proceses prbs sequence.
   virtual function void receive_prbs_pkg(SerDes_sequence_item  pkg);
      WORD_TYPE word;
      bit [14:0] prbs;

      do begin
	 word = pkg.data.pop_front();
	 
	 if(word == DELIMETER)  //end of RCR column
	   break;
	 
	 if( word.is_control ) begin //found control word in PRBS stream
	    FOUND_UNEXPECTED_CONTROL_WORD_IN_PRBS_SEQUENCE : assert (0);
	    break;
	 end

	 PRBS : assert ( prbs[7:0] == word.word ) else 
	   `uvm_error("WRONG_PRBS", $sformatf("Expected %h, Received %h", prbs[7:0], word.word) );
	 prbs = calculateNextPRBS(prbs);
	 
      end // do begin
      while(pkg.data.size);
   endfunction // receive_pkg

   //Function: calculateNextPRBS
   //It calculates next value for PRBS stream.
   virtual function bit[14:0] calculateNextPRBS(bit[14:0] prbs);
      return {prbs[13:0], ~ (prbs[14] ^ prbs[13]) };
   endfunction // calculateNextPRBS
   
   //Function: receive_charge_pkg
   //The function proceses charge injection transactions.
   virtual function void receive_charge_pkg( CLICpix2_Matrix_sequence_item pkg);
      //stores TOT clock divider
      totClkDiv = cfg_.spi_scoreboard.peek_mem_arr(`GLOBAL_CONTROL_REGISTER)[`GLOBAL_CONTROL_REGISTER_TOT_CLK_DIV];
      
      //store the pixels
      expected_pixels = pkg.pixels;

      //re-scale the TOT counters
      foreach( expected_pixels[r,c] )
	if( ! matrixInter.matrix[r][c].long_counter )
	  expected_pixels[r][c].tot /= 2 ** totClkDiv;
				     
   endfunction // receive_charge_pkg

   //Function: receive_MatrixConfig_pkg
   //The function processes matrix configuration packages
   virtual function void receive_MatrixConfig_pkg(CLICpix2_Matrix_sequence_item pkg);
      expected_pixels = pkg.pixels;
   endfunction // receive_MatrixConfig_pkg
   
   //Function: convert2string
   virtual function string convert2string();
      convert2string =$sformatf( {"\n SerDes readouts: %0d"},
				 total_cnt);
   endfunction // convert2string

   
   //Function: report_phase
   //It reports statistics.
   //This function prints only type of the scoreboard and should be defined in all inheriting classes. 
   virtual function void report_phase(uvm_phase phase);
      super.report_phase(phase);
      //Print statistics
      `uvm_info( {get_type_name(), " statistics"} , convert2string(), UVM_MEDIUM );
   endfunction // report_phase

   //Function: decode_serdes_pkg
   virtual function void decode_serdes_pkg(SerDes_sequence_item  pkg);
      //clear the matrix
      foreach( received_pixels[r,c] )
	received_pixels[r][c] = '0;

      do begin
	 decode_serdes_header( pkg.data.pop_front() ); //Header
	 if( extract_rcr_columns( pkg.data ) )       //extract data of the double column
	   while( pkg.data.pop_front() != DELIMETER &&  pkg.data.size); //in case of error look for a next DELIMETER
      end
      while(pkg.data.size && ! (pkg.data.size() == 1 && pkg.data[0] == DELIMETER) ); //continue until whole package is processed (it can be double aligning DIAMETER at the end of the packet)
      decode_counters(); //decode LSFR counters
      //       
   endfunction // decode_serdes_pkg

   //Function: decode_serdes_header
   virtual function void decode_serdes_header( WORD_TYPE word);
      
      PACKET_HEADER_SHOULD_BE_A_REGULAR_DATA_WORD: assert( word.is_control == 1'b0);
      rcr = word.word[7:6];
      RCR_IN_PACKET_HEADER_DIFFERRS_FROM_REGISTER : assert ( rcr == cfg_.spi_scoreboard.peek_mem_arr( `READOUT_CONTROL_REGISTER_ADDRESS )[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] );
      UNSUPPORTED_RCR_IN_PACKET_HEADER: assert ( rcr == 2'b01 || rcr == 2'b10 || rcr == 2'b11) else `uvm_error("ASSERTION", $sformatf("Unsupported RCR (0x%h) in a the packet header", rcr) );

      firstColumn = word.word[5:0];

   endfunction // decode_serdes_header

   //Function: extract_rcr_columns
   virtual function bit extract_rcr_columns( ref WORD_TYPE data[$]);
      PIXEL_T pixels_dc[CLICPIX2_ROW *2][8];  //stores configuration of the double columns (in single packet can be maximum 8 double-columns)
      int  row_index[8] = '{ 8{0} }; //8 independent (in case of compression) counters navigating through the <pixels_dc>
      int  row_slice[8] = '{ 8{ $bits(PIXEL_T)-1 } }; //8 independent (in case of compression) counters navigating through the <pixels_dc>
      int  dc_counter[8] = '{ 8{ 2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL ) + 1 } }; //8 independent (in case of compression) 
      // counters indicating number of bits processed for the given double-column 
      //value 3601 indicates beginning of the double-column
      int  sp_counter[8] = '{ 8{ 0 }}; //number of pixels in the processed super-pixel for the given column
      
      WORD_TYPE word;

      extract_rcr_columns = 0;

      do
	begin
	   word = data.pop_front();

	   if(word == DELIMETER)  //end of RCR column
	     break;

	   if( word.is_control ) begin //found control word different than delimiter
	      FOUND_UNEXPECTED_CONTROL_WORD_IN_PACKET : assert (0);
	      extract_rcr_columns = 1;
	      break;
	   end

	   uniterleave_dc( pixels_dc, dc_counter, sp_counter, row_index, row_slice, word.word );
	   
	end // do begin
      while(data.size); //end readout transaction

      for(int i=0; i<2**rcr; i++)	
	PARTIAL_DOUBLE_COLUMN: assert(dc_counter[i] == 3600);
      
      //remove snake pattern
      foreach(pixels_dc[r]) begin
	 for(int unsigned c=0; c<2**rcr; c++) begin
	    case(r%4)
	      0 : received_pixels[r/2][c*2*64/2**rcr + firstColumn*2    ] = pixels_dc[r][c]; //left column
	      1 : received_pixels[r/2][c*2*64/2**rcr + firstColumn*2 + 1] = pixels_dc[r][c]; //right column
	      2 : received_pixels[r/2][c*2*64/2**rcr + firstColumn*2 + 1] = pixels_dc[r][c]; //right column
	      3 : received_pixels[r/2][c*2*64/2**rcr + firstColumn*2  ] = pixels_dc[r][c]; //left column
	    endcase // case (r%4)
	 end
      end
      
   endfunction // extract_double_column

   //Function: uniterleave_dc
   //Function distributes  data word to the <process_dc_bit>.
   virtual function void uniterleave_dc(
					ref PIXEL_T pixels_dc [CLICPIX2_ROW *2][8], //double-column array
					ref int dc_counter[8], //number of processed bits for the given double column
					ref int sp_counter[8], //number of processed bits for the given super-pixel
					ref int row_index[8], 
					ref int row_slice[8], //bit of the given word
					input byte data);

      //un-interleave the double-columns
      case(rcr)
	2'b01 :
	  for(int i=0; i < $bits(data); i+=2) begin
	     process_dc_bit( pixels_dc, dc_counter[0], sp_counter[0], row_index[0], 0, row_slice[0], data[i] );
	     process_dc_bit( pixels_dc, dc_counter[1], sp_counter[1], row_index[1], 1, row_slice[1], data[i+1] );
	  end
	2'b10 :
	  for(int i=0; i < $bits(data); i+=4) begin
	     process_dc_bit( pixels_dc, dc_counter[0], sp_counter[0], row_index[0], 0, row_slice[0], data[i] );
	     process_dc_bit( pixels_dc, dc_counter[1], sp_counter[1], row_index[1], 1, row_slice[1], data[i+1] );
	     process_dc_bit( pixels_dc, dc_counter[2], sp_counter[2], row_index[2], 2, row_slice[2], data[i+2] );
	     process_dc_bit( pixels_dc, dc_counter[3], sp_counter[3], row_index[3], 3, row_slice[3], data[i+3] );
	  end
	2'b11 :
	  for(int i=0; i < $bits(data); i+=8) begin
	     process_dc_bit( pixels_dc, dc_counter[0], sp_counter[0], row_index[0], 0, row_slice[0], data[i] );
	     process_dc_bit( pixels_dc, dc_counter[1], sp_counter[1], row_index[1], 1, row_slice[1], data[i+1] );
	     process_dc_bit( pixels_dc, dc_counter[2], sp_counter[2], row_index[2], 2, row_slice[2], data[i+2] );
	     process_dc_bit( pixels_dc, dc_counter[3], sp_counter[3], row_index[3], 3, row_slice[3], data[i+3] );
	     process_dc_bit( pixels_dc, dc_counter[4], sp_counter[4], row_index[4], 4, row_slice[4], data[i+4] );
	     process_dc_bit( pixels_dc, dc_counter[5], sp_counter[5], row_index[5], 5, row_slice[5], data[i+5] );
	     process_dc_bit( pixels_dc, dc_counter[6], sp_counter[6], row_index[6], 6, row_slice[6], data[i+6] );
	     process_dc_bit( pixels_dc, dc_counter[7], sp_counter[7], row_index[7], 7, row_slice[7], data[i+7] );
	  end
      endcase // case (rcr)
   endfunction // uniterleave_dc


   //Function: process_dc_bit
   //The function processes double column data bit
   virtual function void process_dc_bit(
					ref PIXEL_T pixels_dc [CLICPIX2_ROW *2][8], //double-column array
					ref int dc_counter, //number of processed bits for the given double column
					ref int sp_counter, //number of processed bits for the given super-pixel
					ref int row_index, 
					input int col_index,
					ref int row_slice, //bit of the given word
					input bit data);

      //middle of the double-column
      if( dc_counter < 2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL )  ) begin 

	 //super-pixel bit
	 if( sp_counter == 0 ) begin 
	    if(data ||  //not empty super-pixel
	       ! cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_SP_COMP_BIT] ) begin  //or sp compression disabled
	       
	       dc_counter++;
	       sp_counter++;
	    end
	    else begin //empty super-pixel
	       
	       repeat(CLICPIX2_SUPERPIXEL) 
		 pixels_dc[ row_index++ ][ col_index ] = 14'b0;
	       
	       dc_counter += CLICPIX2_SUPERPIXEL * $bits(PIXEL_T) + 1;

	    end
	 end // super-pixel
	 
	 
	 //pixel bit
	 else if( row_slice == $bits(PIXEL_T)-1 ) begin
	    
	    if (data || //not empty pixel
		! cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_COMP_BIT] ) begin  //or pixel compression disabled

	       pixels_dc[ row_index ][ col_index ][row_slice--] = data;
	       dc_counter++;
	       sp_counter++;
	    end
	    else begin//empty pixel
	       pixels_dc[ row_index++ ][col_index] = 14'b0;
	       dc_counter += $bits(PIXEL_T);
	       sp_counter += $bits(PIXEL_T);
	    end
	    
	 end // pixel bit
	 
	 //pixel payload
	 else begin
	    
	    pixels_dc[ row_index ][ col_index ][row_slice--] = data; 
	    dc_counter++;
	    sp_counter++;
	    
	    if(row_slice < 0 ) begin
	       row_index++;
	       row_slice = $bits(PIXEL_T)-1;
	    end
	    
	 end // pixel payload

	 if( sp_counter > CLICPIX2_SUPERPIXEL * $bits(PIXEL_T) ) //reset sp_counter
	   sp_counter = 0;
	 
      end

      //beginning of the double-column (3601)
      else begin 
	 
	 if( dc_counter == 2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL ) + 1 ) begin
	    if(data ||  //not empty double-column
	       ! cfg_.spi_scoreboard.peek_mem_arr(`READOUT_CONTROL_REGISTER_ADDRESS)[`READOUT_CONTROL_REGISTER_SP_COMP_BIT] )  //or collumn compression disabled

	      dc_counter = 0;
	    else begin //empty double-column
	       repeat( 2 * CLICPIX2_ROW)
		 pixels_dc[ row_index++ ][ col_index ] = 14'b0;
	       dc_counter =  2 * ( CLICPIX2_ROW * $bits(PIXEL_T) + CLICPIX2_ROW / CLICPIX2_SUPERPIXEL ); //3600
	    end
	 end

      end
      
   endfunction // process_dc_bit

   //Function: decode_counters
   //The functions decodes LFSR values
   virtual function void decode_counters();
      foreach(received_pixels[r,c]) begin
	 if( matrixInter.matrix[r][c].long_counter ) begin
	    {received_pixels[r][c].tot, received_pixels[r][c].toa} = lfsr13[ {received_pixels[r][c].tot, received_pixels[r][c].toa} ];
	 end
	 else begin
	    received_pixels[r][c].tot = lfsr5[received_pixels[r][c].tot];
	    received_pixels[r][c].toa = lfsr8[received_pixels[r][c].toa];
	 end
      end
   endfunction // deocode_counters

   //Function: compare_pixels
   //The function compares <expected_pixels> against <received_pixels>
   virtual function void compare_pixels();
      foreach(received_pixels[r,c]) begin
	 READOUT_VS_EXPECTED_FLAG: assert (received_pixels[r][c].flag == expected_pixels[r][c].flag ) 
	   else `uvm_error("READOUT flag", $sformatf("For pixel [%0d][%0d] the expected flag was %h, received %h", r,c, expected_pixels[r][c].flag, received_pixels[r][c].flag) );

	 if(matrixInter.matrix[r][c].long_counter) begin
	    
	    READOUT_VS_EXPECTED_LONG: assert ( {received_pixels[r][c].tot, received_pixels[r][c].toa}    == { expected_pixels[r][c].tot, expected_pixels[r][c].toa} ||
					       {received_pixels[r][c].tot, received_pixels[r][c].toa} +1 == { expected_pixels[r][c].tot, expected_pixels[r][c].toa} ||
					       {received_pixels[r][c].tot, received_pixels[r][c].toa} -1 == { expected_pixels[r][c].tot, expected_pixels[r][c].toa} )
	      else
		if(! matrixInter.matrix[r][c].count_mode) begin
		   `uvm_error("READOUT TOT", 
			      $sformatf("For pixel [%0d][%0d] the expected long TOA value was %d (or +/-1), received %d", r,c, 
					{ expected_pixels[r][c].tot, expected_pixels[r][c].toa} , {received_pixels[r][c].tot, received_pixels[r][c].toa} ) );
		end
		else begin
		   `uvm_error("READOUT COUNTER", 
			      $sformatf("For pixel [%0d][%0d] the expected long counter value was %d (or +/-1), received %d", r,c, 
					{ expected_pixels[r][c].tot, expected_pixels[r][c].toa} , {received_pixels[r][c].tot, received_pixels[r][c].toa} ) );
		end
	 end
	 else begin
	    
	    READOUT_VS_EXPECTED_TOT: assert (received_pixels[r][c].tot == expected_pixels[r][c].tot || 
					     received_pixels[r][c].tot + 1 == expected_pixels[r][c].tot || 
					     received_pixels[r][c].tot - 1 == expected_pixels[r][c].tot)
	      else
		`uvm_error("READOUT TOT", 
			   $sformatf("For pixel [%0d][%0d] the expected TOT  was %d (or +/-1), received %d", 
				     r,c, expected_pixels[r][c].tot, received_pixels[r][c].tot ) );

	    READOUT_VS_EXPECTED_TOA: assert (received_pixels[r][c].toa == expected_pixels[r][c].toa || 
					     received_pixels[r][c].toa + 1 == expected_pixels[r][c].toa || 
					     received_pixels[r][c].toa - 1 == expected_pixels[r][c].toa)
	      else
		if(! matrixInter.matrix[r][c].count_mode) begin
		   `uvm_error("READOUT TOA", 
			      $sformatf("For pixel [%0d][%0d] the expected short TOA  was %d (or +/-1), received %d", 
					r,c, expected_pixels[r][c].toa, received_pixels[r][c].toa ) );
		end
		else begin
		   `uvm_error("READOUT COUNTER", 
			      $sformatf("For pixel [%0d][%0d] the expected short counter  was %d (or +/-1), received %d",
					r,c, expected_pixels[r][c].toa, received_pixels[r][c].toa ) );
		end
	    
	 end // else: !if(matrixInter.matrix[r][c].long_counter)
      end
      `uvm_info( get_type_name(), "SerDes packet checked", UVM_MEDIUM );
   endfunction // compare_pixels
   
endclass // CLICpix2_Readout_scoreboard

