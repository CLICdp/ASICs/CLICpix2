//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_MatrixConfiguration_Coverage_scoreboard.sv
// Description     : The coverage class of the Matrix Configuration scoreboard.
// Author          : Adrian Fiergolski
// Created On      : Wed Jul 13 15:50:37 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_MatrixConfiguration_Coverage_scoreboard
//The class expands the Matrix Configuration scoreboard with coverage metrics.
class CLICpix2_MatrixConfiguration_Coverage_scoreboard extends CLICpix2_MatrixConfiguration_scoreboard;

   //Typedef: MATRIX_CONFIGURATION_SCENARIO_T
   typedef enum  {ZEROS, ONES, MIXED} MATRIX_CONFIGURATION_SCENARIO_T;

   //Covergroup: MatrixConfiguration_cg
   covergroup MatrixConfiguration_cg;
      matrix_configuration_scenario: coverpoint matrix_configuration_scenario();
   endgroup // MatrixConfiguration_cg
   
   `uvm_component_utils_begin(CLICpix2_MatrixConfiguration_Coverage_scoreboard)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_MatrixConfiguration_Coverage_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
      MatrixConfiguration_cg = new();
      MatrixConfiguration_cg.set_inst_name( "MatrixConfiguration_cg" );
   endfunction // new

   //Task: receive_matrixConfig
   //Update the coverage.
   virtual task receive_matrixConfig(CLICpix2_Matrix_sequence_item tr);
      super.receive_matrixConfig(tr);
      MatrixConfiguration_cg.sample();
   endtask // receive_matrixConfig

   //Function: is_matrix_full_of_zeros
   //Returns true if the configuration matrix is full of zeros.
   protected function MATRIX_CONFIGURATION_SCENARIO_T matrix_configuration_scenario();
      bit zeros = 1;
      bit ones = 1;
      CLICpix2_Matrix_sequence_item tr_;

      assert( $cast(tr_, tr) );
      
      foreach( tr_.pixels [r,c] ) begin
	 zeros &= ( tr_.pixels[r][c] == '0);
	 ones  &= ( tr_.pixels[r][c].c.mask == '1 && tr_.pixels[r][c].c.th_adj == '1 && tr_.pixels[r][c].c.count_mode == '1 &&
		    tr_.pixels[r][c].c.TP_en == '1 && tr_.pixels[r][c].c.long_counter == '1 );
      end
      unique if(zeros)
	matrix_configuration_scenario = ZEROS;
	     else if(ones)
	       matrix_configuration_scenario = ONES;
		  else
		    matrix_configuration_scenario = MIXED;

   endfunction // matrix_configuration_scenario

endclass // CLICpix2_MatrixConfiguration_Coverage_scoreboard
