//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_Simulation_top.sv
// Description     : Top module of the CLCIpix2 simulation.
// Author          : Adrian Fiergolski
// Created On      : Wed Jan 27 17:55:56 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

timeunit 1ps;
timeprecision 10fs;

import uvm_pkg::*;
`include <uvm_macros.svh>

`include "BasicBlocks_pkg.sv"
`include "SPI_pkg.sv"
`include "SerDes_pkg.sv"
`include "CLICpix2_Matrix_pkg.sv"
`include "CLICpix2_pkg.sv"
`include "CLICpix2Test_pkg.sv"

module automatic CLICpix2_Simulation_top;

   import CLICpix2_pkg::SPI_SS_WIDTH;
   import CLICpix2_pkg::vCLICpix2_ClockReset;
   import CLICpix2_pkg::vCLICpix2_outputs;
   import SerDes_pkg::vSerDes_inter;
   import CLICpix2_pkg::vCLICpix2_matrix;
   import CLICpix2_Matrix_pkg::vCLICpix2_Matrix_inter;
   
   import CLICpix2Test_pkg::*;
   
   //////////////
   //Interfaces
   //////////////

   wire [(SPI_SS_WIDTH-1) : 0] SS;
   logic 		       SCK_ext = 0;
   wire 		       MOSI;
   wire 		       MISO;
   //SPI_clock
   initial 
     forever
       #(CLICpix2_pkg::SPI_PERIOD/2) SCK_ext = ! SCK_ext;
   //Instantiating SPI Master QVIP
   spi_master #(
                .SPI_SS_WIDTH  (SPI_SS_WIDTH),
                .IF_NAME       ("spi_interface")
		)
   spi_mstr_qvip
     (
      .sys_clk   (!SCK_ext),
      .reset     (!clockReset.reset_n),
      .SS        (SS),
      .SCK       (),
      .MOSI   (MOSI),
      .MISO   (MISO),
      .MOSI1(), .MOSI2(), .MOSI3(),
      .MISO1(), .MISO2(), .MISO3()
      );

   CLICpix2_ClockReset clockReset();

   CLICpix2_outputs outputs();
   assign outputs.reset = !clockReset.reset_n;

   SerDes_inter serial();
   //generate sample clock ( x2 clockReset.clk )
   initial begin
      @(clockReset.clk); //add some delay
      serial.sample_clk_ext <= clockReset.clk;
      forever begin
	 #(CLICpix2_pkg::CLOCK_PERIOD / 4.0) serial.sample_clk_ext <= ~ serial.sample_clk_ext;
      end
   end
   //disable UNDEFINED_X_STATE_ON_THE_LINK after reset
   initial
     forever begin
	@(clockReset.reset_n == 1'b0); //wait for low reset
	$assertoff( 2, serial.UNDEFINED_X_STATE_ON_THE_LINK);
	@(posedge clockReset.reset_n);
	#200ns;
	$asserton( 2, serial.UNDEFINED_X_STATE_ON_THE_LINK);
     end
   

   CLICpix2_Matrix_inter matrix();
   assign matrix.clock = SCK_ext;
   assign matrix.reset_n = clockReset.reset_n;
   
   //Connect matrix registers to the interface
   genvar doubleColumn;
   genvar superPixel;
   for ( doubleColumn =  0; doubleColumn < 64; doubleColumn++) begin
      for ( superPixel = 0; superPixel < 16; superPixel++) begin
`ifndef netlist
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.mask ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.config_reg ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.event_count ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.TP ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.mask ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.config_reg ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.event_count ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.TP ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.mask ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.config_reg ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.event_count ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.TP ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.mask ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.config_reg ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.event_count ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.TP ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.mask ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.config_reg ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.event_count ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.TP ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.mask ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.config_reg ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.event_count ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.TP ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.mask ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.config_reg ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.event_count ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.TP ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.mask ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.config_reg ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.event_count ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.TP ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr.pr.long_counter ;

	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.mask ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.config_reg ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.event_count ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.TP ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.mask ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.config_reg ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.event_count ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.TP ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.mask ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.config_reg ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.event_count ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.TP ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.mask ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.config_reg ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.event_count ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.TP ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.mask ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.config_reg ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.event_count ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.TP ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.mask ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.config_reg ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.event_count ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.TP ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.mask ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.config_reg ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.event_count ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.TP ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr.pr.long_counter ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.mask ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].th_adj = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.config_reg ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.event_count ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.TP ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr.pr.long_counter ;
`endif //  `ifndef netlist

`ifdef netlist
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl8.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl7.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl6.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl5.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl4.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl3.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl2.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pl1.pr_pr.long_counter_reg.mod.qint ;

	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+0][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr8.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+1][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr7.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+2][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr6.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+3][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr5.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+4][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr4.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+5][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr3.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+6][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr2.pr_pr.long_counter_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].mask = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_cm_mask_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].th_adj[0] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.\config_reg_reg[0] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].th_adj[1] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.\config_reg_reg[1] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].th_adj[2] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.\config_reg_reg[2] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].th_adj[3] = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.\config_reg_reg[3] .mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].count_mode = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.event_count_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].TP_en = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.TP_reg.mod.qint ;
	 assign matrix.matrix[8*superPixel+7][2*doubleColumn + 1].long_counter = CLICpix2.genblk1[ doubleColumn ].double_column.genblk1[ superPixel ].superpixel.pr1.pr_pr.long_counter_reg.mod.qint ;

	 //This hold timing violation can be ignored during acquisition
	 always @(matrix.shutter)
	   if(matrix.shutter) begin
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_0_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_1_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_2_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_3_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_4_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_5_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_6_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_7_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_8_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_9_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_10_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_11_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_12_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_13_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_14_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_15_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_16_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_17_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_18_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_19_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_20_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_21_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_22_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_23_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_24_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_25_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_26_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_27_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_28_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_29_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_30_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_31_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_32_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_33_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_34_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_35_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_36_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_37_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_38_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_39_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_40_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_41_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_42_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_43_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_44_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_45_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_46_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_47_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_48_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_49_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_50_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_51_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_52_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_53_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_54_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_55_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_56_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_57_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_58_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_59_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_60_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_61_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_62_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_63_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} OFF ON") );
	   end // if (matrix.shutter)
	   else begin
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_0_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_1_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_2_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_3_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_4_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_5_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_6_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_7_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_8_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_9_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_10_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_11_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_12_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_13_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_14_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_15_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_16_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_17_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_18_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_19_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_20_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_21_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_22_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_23_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_24_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_25_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_26_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_27_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_28_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_29_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_30_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_31_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_32_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_33_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_34_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_35_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_36_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_37_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_38_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_39_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_40_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_41_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_42_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_43_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_44_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_45_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_46_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_47_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_48_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_49_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_50_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_51_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_52_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_53_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_54_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_55_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_56_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_57_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_58_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_59_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_60_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_61_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_62_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	      assert ( ! mti_fli::mti_Cmd("tcheck_set /CLICpix2_Simulation_top/CLICpix2/periphery/genblk1_63_eoc {( HOLD (posedge  datain_column) (posedge  clkin_divided) )} ON ON") );
	   end
`endif //  `ifdef netlist
	 
      end
   end   


   /////////////
   //DUT
   ////////////
   CLICpix2 CLICpix2(
		     //Basic I/O interfaces
		     .clk(clockReset.clk),
		     .reset_n(clockReset.reset_n),
		     .power_pin(matrix.powerPulse),
		     .shutter(matrix.shutter),
		     .TPswitch(matrix.testPulse),
		     .clk_out(matrix.clkOutput),
		     .dataout_serial(serial.link),

		     //SPI-like interface
		     .clk_slow(SCK_ext), //also used for acquisition
		     .chip_select(SS),	
		     .MOSI(MOSI),
		     .MISO(MISO),

		     //Analogue outputs from columns
		     .ppulse_enable_gated(),
		     .ppulse_enable_gated_n(),
		     
		     //Matrix
		     .discriminator_input(matrix.discriminator_input), .testpulse_output(matrix.testpulse_output),
		     
		     //Signals to the analog periphery	
		     .bgap_enable(outputs.bgap_enable), .bgap_enable_n(outputs.bgap_enable_n),
		     .bgap_control_R3(outputs.bgap_control_R3), .bgap_control_R1(outputs.bgap_control_R1),
		     
		     .bias_disc_N(outputs.bias_disc_N), .bias_disc_P(outputs.bias_disc_P), .bias_ikrum(outputs.bias_ikrum),.bias_preamp(outputs.bias_preamp),
		     .bias_DAC(outputs.bias_DAC), .bias_buffer(outputs.bias_buffer), .casc_preamp(outputs.casc_preamp),.casc_DAC(outputs.casc_DAC),
		     .casc_Nmirror(outputs.casc_Nmirror), .vfbk(outputs.vfbk), .vthMSB(outputs.vthMSB), .vthLSB(outputs.vthLSB),
		     .test_cap1_LSB(outputs.test_cap1_LSB), .test_cap1_MSB(outputs.test_cap1_MSB), .test_cap2(outputs.test_cap2), .bias_disc_N_OFF(outputs.bias_disc_N_OFF),
		     .bias_disc_P_OFF(outputs.bias_disc_P_OFF), .bias_preamp_OFF(outputs.bias_preamp_OFF), .output_mux_DAC(outputs.output_mux_DAC), 
		     
		     .bias_disc_N_n(outputs.bias_disc_N_n), .bias_disc_P_n(outputs.bias_disc_P_n), .bias_ikrum_n(outputs.bias_ikrum_n), .bias_preamp_n(outputs.bias_preamp_n),
		     .bias_DAC_n(outputs.bias_DAC_n), .bias_buffer_n(outputs.bias_buffer_n), .casc_preamp_n(outputs.casc_preamp_n), .casc_DAC_n(outputs.casc_DAC_n),
		     .casc_Nmirror_n(outputs.casc_Nmirror_n), .vfbk_n(outputs.vfbk_n), .vthMSB_n(outputs.vthMSB_n), .vthLSB_n(outputs.vthLSB_n),
		     .test_cap1_LSB_n(outputs.test_cap1_LSB_n), .test_cap1_MSB_n(outputs.test_cap1_MSB_n), .test_cap2_n(outputs.test_cap2_n), .bias_disc_N_OFF_n(outputs.bias_disc_N_OFF_n),
		     .bias_disc_P_OFF_n(outputs.bias_disc_P_OFF_n), .bias_preamp_OFF_n(outputs.bias_preamp_OFF_n), .output_mux_DAC_n(outputs.output_mux_DAC_n)
		     );

   initial begin
      uvm_config_db #(vCLICpix2_ClockReset)::set(null, "uvm_test_top", "clockReset_interface", clockReset );
      uvm_config_db #(vCLICpix2_outputs)::set(null, "uvm_test_top", "outputs_interface", outputs );
      uvm_config_db #(vSerDes_inter)::set(null, "uvm_test_top", "serial_interface", serial );
      uvm_config_db #(vCLICpix2_Matrix_inter)::set(null, "uvm_test_top", "matrix_interface", matrix );
      run_test();
   end

   ////////////////////
       //Assertions   
   ///////////////////
   
   //Verify clock output
   bit disableClkOutput_reg_ass = 0;
   bit disableClkOutput_conf_ass = 0;
   bit reset_ass = 0;
   
   always begin
      @(`CLK_DIV_REG); 
      disableClkOutput_reg_ass = 1'b1;
      #50ns;
      disableClkOutput_reg_ass = 1'b0;
   end

   always begin
      @(matrix.matrixIsBeingConfigured);
      disableClkOutput_conf_ass = 1'b1;
      if(matrix.matrixIsBeingConfigured) //beginning of configuration
	#50ns;
      else                               //end of configuration
	#220ns;
      disableClkOutput_conf_ass = 1'b0;
   end

   
   always begin
      @(clockReset.reset_n == 1'b0);
      reset_ass = 1'b1;
      @(posedge clockReset.reset_n);
      #100ns;
      reset_ass = 1'b0;
   end

   time clkOutPeriod_ass = CLICpix2_pkg::SPI_PERIOD;

   always_comb
     if(matrix.matrixIsBeingConfigured) //Matrix is being configured
       clkOutPeriod_ass = CLICpix2_pkg::SPI_PERIOD;
     else
       case(`CLK_DIV_REG)
	 2'b10: clkOutPeriod_ass = CLICpix2_pkg::CLOCK_PERIOD*4;
	 2'b01: clkOutPeriod_ass = CLICpix2_pkg::CLOCK_PERIOD*2;
	 2'b00: clkOutPeriod_ass = CLICpix2_pkg::CLOCK_PERIOD;
       endcase // case (`CLK_DIV_REG)


   property period_detection(signal, equalTo, reset = 0);
      time FirstEdge;
      @(posedge signal) disable iff(reset) (1, FirstEdge=$time ) ##1 $time - FirstEdge == equalTo;
      //      @(posedge signal) disable iff(reset) (1, FirstEdge=$time, $display("Started: %d", FirstEdge) ) ##1 (1, $display("Finished: %d %d", $time-FirstEdge, equalTo) );
   endproperty // glitch_detection
   clock_output_period: assert property ( period_detection(matrix.clkOutput, clkOutPeriod_ass , disableClkOutput_reg_ass || disableClkOutput_conf_ass || reset_ass ) ) else `uvm_error("CLKOUT PERIOD", $sformatf("Expected period: %0d timeunits", clkOutPeriod_ass));

   
   clock_output_alive: assert property ( @(posedge clockReset.clk) disable iff(disableClkOutput_reg_ass || disableClkOutput_conf_ass ||reset_ass ) matrix.clkOutput |-> ##[1:10] ~matrix.clkOutput );

   
endmodule // CLICpix2_Simulation_top

config CLICpix2_Simulation;
   design CLICpix2_Simulation_top;
endconfig
