//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_scoreboard.sv
// Description     : THe main CLICpix2's scoreboard.
// Author          : Adrian Fiergolski
// Created On      : Thu Jan 28 18:44:13 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Title: CLICpix2_scoreboard

//Class: CLICpix2_scoreboard_config
//Alias for <scoreboard_main_base_config>
class CLICpix2_scoreboard_config extends scoreboard_main_base_config;
   
   `uvm_object_utils_begin(CLICpix2_scoreboard_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_scoreboard_config> with the given ~name~.
   function new(string name="CLICpix2_scoreboard_config");
      super.new(name);
   endfunction // new
   
endclass // CLICpix2_scoreboard_config

//Class: CLICpix2_scoreboard
//The class performing analysis. It contains a dedicated sub-scoreboards.
class CLICpix2_scoreboard extends scoreboard_main_base;
   
   `uvm_component_utils_begin(CLICpix2_scoreboard)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_scoreboard> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

endclass // CLICpix2_scoreboard
