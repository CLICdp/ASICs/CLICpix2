//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_MatrixConfiguration_monitor.sv
// Description     : The monitor extracting matrix configuration from the SPI traffic.
// Author          : Adrian Fiergolski
// Created On      : Tue Jul 12 14:40:45 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_MatrixConfiguration_monitor_config
//Configuration class for <CLICpix2_MatrixConfiguration>
class CLICpix2_MatrixConfiguration_monitor_config extends VIP_generic_monitor_config;

   vCLICpix2_Matrix_inter matrixInter;
   
   `uvm_object_utils_begin(CLICpix2_MatrixConfiguration_monitor_config)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_MatrixConfiguration_monitor_config> with the given ~name~.
   function new(string name="CLICpix2_MatrixConfiguration_monitor_config");
      super.new(name);
   endfunction // new
   
endclass // CLICpix2_MatrixConfiguration_monitor_config


//Class: CLICpix2_MatrixConfiguration_monitor
//The SPI sub-monitor extracting matrix configuration from SPI traffic.
class CLICpix2_MatrixConfiguration_monitor extends VIP_generic_monitor;

   //Variable: matrixInter
   vCLICpix2_Matrix_inter matrixInter;
   
   //Variable: confStream_64
   //64 bit words inserted to double-columns
   protected bit [63:0] confStream_64 [ $size(CLICPIX2_CONIGURATION_SPI_STREAM)/8 ];

   //Variable: confStream_64_index
   protected int unsigned confStream_64_index = 0;

   //Variable: confStream_64_slice
   protected int unsigned confStream_64_slice = 0;

   //Variable: configuration_completed
   //It is asserted matrix is configured.
   //Reset after subsequent write access.
   protected bit configuration_completed = 0;

   
   `uvm_component_utils_begin(CLICpix2_MatrixConfiguration_monitor)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_MatrixConfiguration_monitor> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new
   
   //Function: build_phase
   //Retrieve the configuration and the interfaces.
   function void build_phase(uvm_phase phase);
      CLICpix2_MatrixConfiguration_monitor_config monCfg_;

      super.build_phase(phase);

      assert( $cast(monCfg_, monCfg) );
      matrixInter = monCfg_.matrixInter;

   endfunction // build_phase

   
   //Task: receive
   //The task handles the following SPI access
   virtual task receive();
      CLICpix2_Matrix_sequence_item tr_ = CLICpix2_Matrix_sequence_item::type_id::create("tr");

      assert($cast(tr, tr_) );
      
      tr_.knob = CLICpix2_Matrix_sequence_item::CONFIGURATION;


      fork
	 begin
	    fork
	       receive_SPIconfiguration();
	       receive_BackdoorConfiguration();
	    join_any
	    disable fork;
	 end
      join

   endtask // receive

   //Task: receive_SPIconfiguration
   virtual task receive_SPIconfiguration();
      CLICpix2_spi_master_data_transfer mvc_tr_;
      assert($cast(mvc_tr_, mvc_tr) );

      while( ! is_configuration_completed() ) begin
	 mvc_tr_.receive( mvc_pkg::mvc_config_base::get_config(this) );
	 if(mvc_tr_.address == `MATRIX_PROGRAMMING_ADDRESS && mvc_tr_.slave_id == 0) begin
            writeConfigurationSPI(mvc_tr_.data);
	 end
      end
   endtask // receive_SPIconfiguration

   //Task: receive_BackdoorConfiguration
   virtual task receive_BackdoorConfiguration();
      CLICpix2_Matrix_sequence_item tr_;
      assert($cast(tr_, tr) );
      @( matrixInter.b_matrix_load );
   
      foreach( tr_.pixels[r,c] )
	tr_.pixels[r][c].c = matrixInter.b_matrix[r][c];
	 
   endtask // receive_BackdoorConfiguration

   //Task: run_phase
   //It spawns in background <resetTrack> tasks.
   virtual task run_phase(uvm_phase phase);
      fork
	 resetTrack();
      join_none;
      super.run_phase(phase);
   endtask // run_phase
   
   //Function: writeConfigurationSPI
   //It builds configuration basing on SPI words.
   //Every 4 ~data~ bytes it rebuilds ~matrix~ as it is implemented in the chip
   //The function takes care of ~confStream_64_index~ and ~confStream_64_slice~.
   function void writeConfigurationSPI(input byte data);
      confStream_64[confStream_64_index][8*confStream_64_slice++ +: 8] = data;
      matrixInter.matrixIsBeingConfigured = 1'b1;
      if( confStream_64_slice == $size(confStream_64, 2) / $bits(data) ) begin
	 confStream_64_slice = 0;
	 confStream_64_index++;
	 if(confStream_64_index == $size(confStream_64,1) ) begin
	    configuration_completed = 1;
	    matrixInter.matrixIsBeingConfigured = 1'b0;
	    confStream_64_index = 0;
	    generateMatrix();
	 end
      end
   endfunction // writeConfigurationSPI
   
   //Function: generateMatrix
   //Basing on ~confStream_64~ it generates ~matrix~.
   function void generateMatrix();
      PIXEL_T matrix_dc[CLICPIX2_ROW *2][CLICPIX2_COL /2];  //stores configuration of the double columns
      int unsigned confStream_64_index = 0; //index used to navigate through confStream64, do not overwrite the one from class
      CLICpix2_Matrix_sequence_item tr_;
      assert( $cast(tr_, tr) );

      confStream_64_index++;  //end of column don't care value
      for(int r = 0; r < $size(matrix_dc,1); r++) begin
	 if(r%16 == 0)                
	   confStream_64_index++; //don't care every super-pixel
	 for(int b = $bits(PIXEL_T) - 1; b >= 0; b--) begin 
	    for(int c = 0; c < $size(confStream_64,2); c++)
	      matrix_dc[r][c][b] = confStream_64[confStream_64_index][c];
	    confStream_64_index++;
	 end
      end

      foreach(tr_.pixels[r,c]) begin
	 if(c%2) begin //odd column
	 end
	 else begin//
	    if(r%2) begin //odd row
	       tr_.pixels[r][c+1] = matrix_dc[2*r][c/2]; //move left to right
	       tr_.pixels[r][c] = matrix_dc[2*r+1][c/2];
	    end
	    else begin   //even row
	       tr_.pixels[r][c] = matrix_dc[2*r][c/2];   //move right to left
	       tr_.pixels[r][c+1] = matrix_dc[2*r+1][c/2];
	    end
	 end//
      end
   endfunction // generateMatrix

   //Function:: is_configuration_completed
   //Returns <configuration_completed>.
   function bit is_configuration_completed();
      if(configuration_completed) begin
	 is_configuration_completed = 1'b1;
	 configuration_completed = 0;
      end
      else
	is_configuration_completed = 1'b0;
   endfunction // is_configuration_completed

   //Task: resetTrack
   task resetTrack();
      forever begin
	 @(negedge matrixInter.reset_n);
	 reset();
      end
   endtask // resetTrack

   //Function: reset
   //It resets the matrix configuration
   function void reset();
      confStream_64_index = 0;
      confStream_64_slice = 0;
   endfunction // reset

   
endclass // CLICpix2_MatrixConfiguration_monitor
