//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2Test_pkg.sv
// Description     : The package contains available sequences.
// Author          : Adrian Fiergolski
// Created On      : Wed Jan 27 18:44:01 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef CLICPIX2TEST_PKG_SV
 `define CLICPIX2TEST_PKG_SV

//Title: CLICpix2Test_pkg
//The package contains available sequences.
package CLICpix2Test_pkg;
   import uvm_pkg::*;
 `include <uvm_macros.svh>

   import SPI_pkg::*;
   import SerDes_pkg::*;
   import CLICpix2_pkg::*;
   import CLICpix2_Matrix_pkg::*;
   import mvc_pkg::mvc_sequence;

 `include "CLICpix2_seq.sv"
 `include "CLICpix2_SPI_lib_seq.sv"
 `include "CLICpix2_SPI_seq.sv"
 `include "CLICpix2_chargeInjection_seq.sv"
 `include "CLICpix2_chargeInjectionPierpaoloColumnChange_seq.sv"
   
 `include "genericTest.sv"
 `include "spiTest.sv"
 `include "chargeInjectionTest.sv"
 `include "chargeInjection_TOA_LIMIT_Test.sv"
 `include "chargeInjectionPierpaoloColumnChangeTest.sv"
 `include "chargeInjectionLongCounterOverflowTest.sv"
   
endpackage // CLICpix2Test_pkg
   

`endif
