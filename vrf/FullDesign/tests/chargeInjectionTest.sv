//                              -*- Mode: Verilog -*-
// Filename        : chargeInjectionTest.sv
// Description     : The charge injection test of the CLICpix2.
// Author          : Adrian Fiergolski
// Created On      : Thu Jun 30 15:01:20 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


//Class: chargeInjectionTest
//Test performing verification of the charge injection and readout test.
class chargeInjectionTest extends genericTest;

   //Variable: NoOfPackets
   //Number of injected charge packets.
   int unsigned NoOfPackets = 5;
   
   `uvm_component_utils_begin(chargeInjectionTest)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <chargeInjectionTest> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Task: start_CLICpix2_sequence
   //It spawns <CLICpix2_SPI_seq>
   virtual task start_CLICpix2_sequence(uvm_phase phase);
      CLICpix2_chargeInjection_seq chargeInjection = CLICpix2_chargeInjection_seq::type_id::create("chargeInjection_master");
      if( ! chargeInjection.randomize() with { NoOfPackets == local::NoOfPackets; })
	`uvm_error("RAND_ERROR", "Randomisation failed");
      chargeInjection.start( env.sqr );

   endtask // start_CLICpix2_sequence

   //Task: timeout
   //Timeout of the whole test. 
   //By default: 1000s.
   virtual task timeout();
      # 10s;
   endtask // timeout

   
endclass // chargeInjectionTest
