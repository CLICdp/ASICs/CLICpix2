//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_chargeInjectionPierpaoloColumnChange_seq.sv
// Description     : Column change sequence
// Author          : Adrian Fiergolski
// Created On      : Tue Nov 22 11:53:57 2016
// Last Modified By: .
// Last Modified On: .
// Update Count    : 0
// Status          : Unknown, Use with caution!

//Class: CLICpix2_chargeInjection_seq
//8 coulmns followed by 4 columns
class CLICpix2_chargeInjectionPierpaoloColumnChange_seq extends CLICpix2_chargeInjection_seq;

   `uvm_object_utils_begin(CLICpix2_chargeInjectionPierpaoloColumnChange_seq)
   `uvm_object_utils_end

   //Function: new
   //Creates a new <CLICpix2_chargeInjectionPierpaoloColumnChange_seq> with the given ~name~.
   function new(string name="CLICpix2_chargeInjectionPierpaoloColumnChange_seq");
      super.new(name);
   endfunction // new

   //Task: body
   //8 column followed by 4 collumns readout.
   task body();
      CLICpix2_Matrix_sequence_item matrixConf_backdoor = CLICpix2_Matrix_sequence_item::type_id::create("matrixConf_backdoor");
      CLICpix2_spi_ReadoutConfig readoutConfig = CLICpix2_spi_ReadoutConfig::type_id::create("spi_start_readout");
      CLICpix2_spi_readout spi_readout = CLICpix2_spi_readout::type_id::create("spi_start_readout");
      CLICpix2_Matrix_sequence_item charge = CLICpix2_Matrix_TOA_LIMIT_sequence_item::type_id::create("charge");
      CLICpix2_virtual_sqr p_sequencer_;
      SPI_CLICpix2_sqr spi_sqr;
      uvm_sequence_item dumy;

      assert( $cast(p_sequencer_, p_sequencer) );

      matrixConf_backdoor.knob.rand_mode(0);
      matrixConf_backdoor.knob = CLICpix2_Matrix_sequence_item::CONFIGURATION;
      matrixConf_backdoor.readout_constaints.constraint_mode(0);

      
      repeat( NoOfPackets ) begin
	 
      ////////////////
      //8 columns
      ////////////////

      //Matrix Configuration
      if( ! matrixConf_backdoor.randomize() )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      p_sequencer_.cfg.matrix_inter.matrix_backdoorConfig(matrixConf_backdoor);
      repeat(10) @(posedge p_sequencer_.cfg.clockReset_inter.clk);
      `uvm_info( get_type_name(), "Matrix backdoor configuration applied", UVM_MEDIUM);
      
      assert( $cast( spi_sqr, p_sequencer_.spi_sqr.sqr["master"] ) );
      spi_sqr.matrixConfigured = 1'b1;
      
      //Try different configuration of readout
      `uvm_info( get_type_name(), "Configuring different readout mode", UVM_MEDIUM);
      readoutConfig.readoutControlRegisterEnforce = 1'b1;
      if( ! readoutConfig.randomize() with {
	       readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b11;} ) //8 columns
	`uvm_error("RAND_ERROR", "Randomisation failed");
      readoutConfig.start( p_sequencer.spi_sqr.sqr["master"] );
      `uvm_info( get_type_name(), "Readout mode configured", UVM_MEDIUM);

      //Inject charge
      `uvm_info( get_type_name(), "Starting charge injection", UVM_MEDIUM);
      p_sequencer_.cfg.matrix_inter.powerPulse = 1'b1; //enable power for pixels
      #1us;
      p_sequencer_.cfg.matrix_inter.shutter = 1'b1; //enable shutter
      #20ns;
      start_item( charge, .sequencer( p_sequencer.charge_sqr ) );
      if( ! charge.randomize() with
	  { knob == CLICpix2_Matrix_sequence_item::READOUT;}  )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      finish_item(charge);
      p_sequencer_.cfg.matrix_inter.shutter = 1'b0; //close shutter
      #20ns;
      p_sequencer_.cfg.matrix_inter.powerPulse = 1'b0; //disable power for pixels
      #1us;
      `uvm_info( get_type_name(), "Finishing charge injection", UVM_MEDIUM);

      //Start readout
      `uvm_info( get_type_name(), "Starting readout", UVM_MEDIUM);
      if( ! spi_readout.randomize() )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      spi_readout.start( p_sequencer.spi_sqr.sqr["master"] );
      p_sequencer_.port["Serial"].peek(dumy); //wait for SerDes to finish
      `uvm_info( get_type_name(), "Finishing readout", UVM_MEDIUM);

      ////////////////
      //4 columns
      ////////////////


      //Matrix Configuration
      if( ! matrixConf_backdoor.randomize() )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      p_sequencer_.cfg.matrix_inter.matrix_backdoorConfig(matrixConf_backdoor);
      repeat(10) @(posedge p_sequencer_.cfg.clockReset_inter.clk);
      `uvm_info( get_type_name(), "Matrix backdoor configuration applied", UVM_MEDIUM);
      
      assert( $cast( spi_sqr, p_sequencer_.spi_sqr.sqr["master"] ) );
      spi_sqr.matrixConfigured = 1'b1;
      
      //Try different configuration of readout
      `uvm_info( get_type_name(), "Configuring different readout mode", UVM_MEDIUM);
      readoutConfig.readoutControlRegisterEnforce = 1'b1;
      if( ! readoutConfig.randomize() with {
	       readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b10;} ) //4 columns
	`uvm_error("RAND_ERROR", "Randomisation failed");
      readoutConfig.start( p_sequencer.spi_sqr.sqr["master"] );
      `uvm_info( get_type_name(), "Readout mode configured", UVM_MEDIUM);

      //Inject charge
      `uvm_info( get_type_name(), "Starting charge injection", UVM_MEDIUM);
      p_sequencer_.cfg.matrix_inter.powerPulse = 1'b1; //enable power for pixels
      #1us;
      p_sequencer_.cfg.matrix_inter.shutter = 1'b1; //enable shutter
      #20ns;
      start_item( charge, .sequencer( p_sequencer.charge_sqr ) );
      if( ! charge.randomize() with
	  { knob == CLICpix2_Matrix_sequence_item::READOUT;}  )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      finish_item(charge);
      p_sequencer_.cfg.matrix_inter.shutter = 1'b0; //close shutter
      #20ns;
      p_sequencer_.cfg.matrix_inter.powerPulse = 1'b0; //disable power for pixels
      #1us;
      `uvm_info( get_type_name(), "Finishing charge injection", UVM_MEDIUM);

      //Start readout
      `uvm_info( get_type_name(), "Starting readout", UVM_MEDIUM);
      if( ! spi_readout.randomize() )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      spi_readout.start( p_sequencer.spi_sqr.sqr["master"] );
      p_sequencer_.port["Serial"].peek(dumy); //wait for SerDes to finish
      `uvm_info( get_type_name(), "Finishing readout", UVM_MEDIUM);
    end // repeat ( NoOfPackets )
    endtask // body
endclass // CLICpix2_chargeInjectionPierpaoloColumnChange_seq

  