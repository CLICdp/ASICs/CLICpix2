//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_SPI_seq.sv
// Description     : The SPI test sequence.
// Author          : Adrian Fiergolski
// Created On      : Wed Feb 24 13:55:01 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_SPI_seq
//The sequence performing SPI test.
class CLICpix2_SPI_seq extends CLICpix2_seq;

   //Variable: NoOfPackets
   //Number of injected SPI packets.
   rand int unsigned NoOfPackets = 1;
   
   `uvm_object_utils_begin(CLICpix2_SPI_seq)
   `uvm_object_utils_end

   //Function: new
   //Creates a new <CLICpix2_SPI_seq> with the given ~name~.
   function new(string name="CLICpix2_SPI_seq");
      super.new(name);
   endfunction // new

   //Task: body
   //The task generate number of random <CLICpix2_spi_top_sequence>
   task body();
      CLICpix2_spi_top_sequence spi_random = CLICpix2_spi_top_sequence::type_id::create("random_registers");
      CLICpix2_spi_MatrixConfig_readoutControl_seq matrixConf = CLICpix2_spi_MatrixConfig_readoutControl_seq::type_id::create("matrix_configuration");
      CLICpix2_spi_DummyAddress_seq dummyAddress = CLICpix2_spi_DummyAddress_seq::type_id::create("dummy_address");
      CLICpix2_virtual_sqr p_sequencer_;
      SPI_CLICpix2_sqr spi_sqr;
      uvm_sequence_item dumy;

      assert( $cast(p_sequencer_, p_sequencer) );
      assert( $cast( spi_sqr, p_sequencer.spi_sqr.sqr["master"] ) );
      
      //reset matrixConfiguration
      fork
	 forever begin
	    @( p_sequencer_.cfg.matrix_inter.reset_n == 0 );
	    spi_sqr.matrixConfigured = 1'b0;
	 end
      join_none

      repeat(NoOfPackets) begin
	 randcase
	   10: begin  //register access

	      `uvm_info( get_type_name(), "Starting random SPI access", UVM_MEDIUM);
	      
	      if( ! spi_random.randomize() )
		`uvm_error("RAND_ERROR", "Randomisation failed");
	      spi_random.start( spi_sqr );
	      `uvm_info( get_type_name(), "Finishing random SPI access", UVM_MEDIUM);
	      
	      if(spi_random.tr.address == `READOUT_ADDRESS ) //if started readout
		p_sequencer_.port["Serial"].peek(dumy); //wait for SerDes to finish
	      
	   end
	 
	   2: if (! spi_sqr.prbs_on ) begin //matrix configuration (can't be during PRBS sequence)
	      `uvm_info( get_type_name(), "Starting Matrix Configuration", UVM_MEDIUM);
	      if( ! matrixConf.randomize() )
		`uvm_error("RAND_ERROR", "Randomisation failed");
	      matrixConf.start( spi_sqr );
	      `uvm_info( get_type_name(), "Finishing Matrix Configuration", UVM_MEDIUM);
	   end

	   5: begin //SPI dummy address
	      `uvm_info( get_type_name(), "Starting dummy SPI address access", UVM_MEDIUM);
	      //address outside CLICpix address space
	      if( ! dummyAddress.randomize() )
		`uvm_error("RAND_ERROR", "Randomisation failed");
	      
	      dummyAddress.start( spi_sqr );
	      `uvm_info( get_type_name(), "Finishing dummy SPI address access", UVM_MEDIUM);
	   end

           endcase // randcase
	   #( $urandom_range(1,20) * 1ns);
      end
   
   endtask // body
   
endclass // CLICpix2_SPI_seq
