//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_chargeInjection_seq.sv
// Description     : The charge injection test sequence.
// Author          : Adrian Fiergolski
// Created On      : Thu Jun 30 15:04:52 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_chargeInjection_seq
//The sequence performs charge injection and data taking test.
class CLICpix2_chargeInjection_seq extends CLICpix2_seq;

   //Variable: NoOfPackets
   //Number of injected charge packets.
   rand int unsigned NoOfPackets = 1;

   //Variable: randomConfigurationProbability 
   int unsigned randomConfigurationProbability = 7;

   //Variable: allZerosConfigurationProbability
   int unsigned allZerosConfigurationProbability = 1;

   //Variable: allOnesConfigurationProbability
   int unsigned allOnesConfigurationProbability = 1;

   //Variable: backdoorConfiguratioNProbability
   int unsigned backdoorConfigurationProbability = 10;

   //Variable: spiConfiguratioNProbability
   int unsigned spiConfigurationProbability = 1;

   
   `uvm_object_utils_begin(CLICpix2_chargeInjection_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_chargeInjection_seq> with the given ~name~.
   function new(string name="CLICpix2_chargeInjection_seq");
      super.new(name);
   endfunction // new

   //Task: body
   //The task configures the matrix with zeros and start many charge injection followed by data acquisition sequences
   task body();
      CLICpix2_spi_MatrixConfig_seq matrixConf = CLICpix2_spi_MatrixConfig_seq::type_id::create("matrix_configuration");
      CLICpix2_Matrix_sequence_item charge = CLICpix2_Matrix_sequence_item::type_id::create("charge");
      CLICpix2_spi_readout spi_readout = CLICpix2_spi_readout::type_id::create("spi_start_readout");
      CLICpix2_spi_ReadoutConfig readoutConfig = CLICpix2_spi_ReadoutConfig::type_id::create("spi_start_readout");
      CLICpix2_Matrix_sequence_item matrixConf_backdoor = CLICpix2_Matrix_sequence_item::type_id::create("matrixConf_backdoor");
      CLICpix2_reset_seq reset = CLICpix2_reset_seq::type_id::create("reset");
      CLICpix2_virtual_sqr p_sequencer_;
      SPI_CLICpix2_sqr spi_sqr;
      uvm_sequence_item dumy;
      
      assert( $cast(p_sequencer_, p_sequencer) );

      matrixConf_backdoor.knob.rand_mode(0);
      matrixConf_backdoor.knob = CLICpix2_Matrix_sequence_item::CONFIGURATION;
      matrixConf_backdoor.readout_constaints.constraint_mode(0);
      
      repeat( NoOfPackets ) begin

	 if($urandom_range(1,10) == 1) begin //randomly perform reset
	    if( ! reset.randomize() )
	      `uvm_error("RAND_ERROR", "Randomisation failed");
	    reset.start( p_sequencer );
	 end
	   
	 randcase
	   backdoorConfigurationProbability: begin
	      //Apply backodoor  matrix configuration
	      `uvm_info( get_type_name(), "Applying matrix backdoor configuration", UVM_MEDIUM);
	      randcase
		randomConfigurationProbability: begin //random configuration
		  if( ! matrixConf_backdoor.randomize() )
		    `uvm_error("RAND_ERROR", "Randomisation failed");
		end
		allZerosConfigurationProbability: begin  //All zeros
		   if( ! matrixConf_backdoor.randomize() with {
		      foreach( pixels[r,c] ) {
			 pixels[r][c].c == '0; } 
		   } )
		     `uvm_error("RAND_ERROR", "Randomisation failed");
		end
		allOnesConfigurationProbability: begin //All ones
		   if( ! matrixConf_backdoor.randomize() with {
		      foreach( pixels[r,c] ) {
			 pixels[r][c].c.mask == '1;
			 pixels[r][c].c.th_adj == '1;
			 pixels[r][c].c.count_mode == '1;
			 pixels[r][c].c.TP_en == '1;
			 pixels[r][c].c.long_counter == '1; } 
		   } )
		     `uvm_error("RAND_ERROR", "Randomisation failed");
		end
	      endcase // randcase
	      p_sequencer_.cfg.matrix_inter.matrix_backdoorConfig(matrixConf_backdoor);
	      repeat(10) @(posedge p_sequencer_.cfg.clockReset_inter.clk);
	      `uvm_info( get_type_name(), "Matrix backdoor configuration applied", UVM_MEDIUM);

	      assert( $cast( spi_sqr, p_sequencer_.spi_sqr.sqr["master"] ) );
	      spi_sqr.matrixConfigured = 1'b1;
	   end // case: repeat( NoOfPackets ) begin...
	 spiConfigurationProbability: begin
	      //Apply matrix configuration over SPI
	      `uvm_info( get_type_name(), "Applying SPI matrix Configuration", UVM_MEDIUM);
	      if( ! matrixConf.randomize() )
		`uvm_error("RAND_ERROR", "Randomisation failed");
	      matrixConf.start( p_sequencer.spi_sqr.sqr["master"] );
	      `uvm_info( get_type_name(), "Matrix SPI configuration applied", UVM_MEDIUM);

	      //Start readout to flush TOT and TOA registers
	      `uvm_info( get_type_name(), "Starting configuration readout", UVM_MEDIUM);
	      if( ! spi_readout.randomize() )
		`uvm_error("RAND_ERROR", "Randomisation failed");
	      spi_readout.start( p_sequencer.spi_sqr.sqr["master"] );
	      p_sequencer_.port["Serial"].peek(dumy); //wait for SerDes to finish
	      `uvm_info( get_type_name(), "Finishing configuration readout", UVM_MEDIUM);

	   end
         endcase // randcase
	 
	 
	 //Try different configuration of readout
	 `uvm_info( get_type_name(), "Configuring different readout mode", UVM_MEDIUM);
	 if( ! readoutConfig.randomize() )
	  `uvm_error("RAND_ERROR", "Randomisation failed");
	 readoutConfig.start( p_sequencer.spi_sqr.sqr["master"] );
	 `uvm_info( get_type_name(), "Readout mode configured", UVM_MEDIUM);

	 //Inject charge
	 `uvm_info( get_type_name(), "Starting charge injection", UVM_MEDIUM);
	 p_sequencer_.cfg.matrix_inter.powerPulse = 1'b1; //enable power for pixels
	 #1us;
	 p_sequencer_.cfg.matrix_inter.shutter = 1'b1; //enable shutter
	 #20ns;
	 start_item( charge, .sequencer( p_sequencer.charge_sqr ) );
	 if( ! charge.randomize() with
	     { knob == CLICpix2_Matrix_sequence_item::READOUT;}  )
	   `uvm_error("RAND_ERROR", "Randomisation failed");
	 finish_item(charge);
	 p_sequencer_.cfg.matrix_inter.shutter = 1'b0; //close shutter
	 #20ns;
	 p_sequencer_.cfg.matrix_inter.powerPulse = 1'b0; //disable power for pixels
	 #1us;
	 `uvm_info( get_type_name(), "Finishing charge injection", UVM_MEDIUM);

	 //Start readout
	 `uvm_info( get_type_name(), "Starting readout", UVM_MEDIUM);
	 if( ! spi_readout.randomize() )
	   `uvm_error("RAND_ERROR", "Randomisation failed");
	 spi_readout.start( p_sequencer.spi_sqr.sqr["master"] );
	 fork
	    begin
	       fork
		  p_sequencer_.port["Serial"].peek(dumy); //wait for SerDes to finish
		  begin
		     #500us;
		     `uvm_error("NO DATA", "Data haven not arrived.");
		  end
	       join_any;
	       disable fork;
	    end // fork begin
	 join;
	 `uvm_info( get_type_name(), "Finishing readout", UVM_MEDIUM);

      end // repeat ( NoOfPackets )
      
      #100ns;
   endtask // body
   
   
endclass // CLICpix2_chargeInjection_seq
