//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_seq.sv
// Description     : Sequences used by CLICpix2.
// Author          : Adrian Fiergolski
// Created On      : Wed Feb 24 10:25:26 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_seq
//Generic C3PD sequence executed on <CLICpix2_virtual_sqr>.
class CLICpix2_seq extends uvm_sequence;

   `uvm_declare_p_sequencer( CLICpix2_virtual_sqr )
   `uvm_object_utils_begin(CLICpix2_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_seq> with the given ~name~.
   function new(string name="CLICpix2_seq");
      super.new(name);
   endfunction // new

   //Task: pre_body
   //It raises objection.
   task pre_body();
      if(starting_phase != null )
	starting_phase.raise_objection(this, $sformatf("Starting %s", get_name() ) );
   endtask // pre_body
   
   //Task: post_body
   //It drops objection.
   task post_body();
      if(starting_phase != null )
	starting_phase.drop_objection(this, $sformatf("Ending %s", get_name() ) );
   endtask // pre_body

endclass // CLICpix2_seq

//Class: CLICpix2_reset_seq
//Generate reset.
class CLICpix2_reset_seq extends CLICpix2_seq;
   
   `uvm_object_utils_begin(CLICpix2_reset_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_reset_seq> with the given ~name~.
   function new(string name="CLICpix2_reset_seq");
      super.new(name);
   endfunction // new

   //Task: body
   //It generates a reset sequence with a random duration (0-500ns)
   task body();
      `uvm_info( get_name(), "Starting reset sequence", UVM_MEDIUM);
      p_sequencer.cfg.clockReset_inter.reset_n <= 1'b1;
      p_sequencer.cfg.clockReset_inter.reset_n <= 1'b0;
      #( $urandom_range(0,500) * 1ns );
      p_sequencer.cfg.clockReset_inter.cb.reset_n <= 1'b1;
      #1us; //wait additional time (specification)
      `uvm_info( get_name(), "Finishing reset sequence", UVM_MEDIUM);
   endtask // body
   
endclass // CLICpix2_reset_seq

