//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_SPI_lib_seq.sv
// Description     : Library of the SPI sequence to control the CLICpix2.
// Author          : Adrian Fiergolski
// Created On      : Wed Feb 24 14:04:47 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: CLICpix2_spi_top_sequence
//The sequence item to access register of the CLICpix2.
class CLICpix2_spi_top_sequence extends mvc_sequence;

   static int unsigned MAXIMUM_NUMBER_OF_TRANSACTIONS = 64;

   //Variable: m_slave_id
   //Slave id
   const bit [4:0] m_slave_id = 5'b0;
      
   //Variable: tr
   //Single SPI transactions
   CLICpix2_spi_master_data_transfer tr;

   //Variable: count
   //Number of SPI packets.
   rand int unsigned count;

   //Variable: SPI_index
   //Number of executed packets so far.
   int   unsigned SPI_index;


   `uvm_declare_p_sequencer(SPI_CLICpix2_sqr)
   `uvm_object_utils_begin(CLICpix2_spi_top_sequence)
   `uvm_object_utils_end

   //Constraint: CLICPIX2_count_constraint
   constraint CLICPIX2_count_constraint{
      count > 0;
      count < MAXIMUM_NUMBER_OF_TRANSACTIONS;
   }
   
   //Function: new
   //Creates a new <CLICpix2_spi_top_sequence> with the given ~name~.
   function new(string name="CLICpix2_spi_top_sequence");
      super.new(name);
   endfunction // new

   //Task: body
   task body();
      spi_vip_config#(SPI_SS_WIDTH) cfg = spi_vip_config#(SPI_SS_WIDTH)::get_config(m_sequencer);
      cfg.wait_for_clock();
      tr = CLICpix2_spi_master_data_transfer::type_id::create( "tr" );

      SPI_index = 0;
      repeat( count )  begin
	 start_item( tr );
	 randomize_tr( tr );
	 finish_item( tr );
	 SPI_index++;

	 //Readout address must be the last SPI transaction
	 if( tr.address ==`READOUT_ADDRESS )
	   break;
      end
   endtask // body

   //Task: randomize_tr
   virtual task randomize_tr( CLICpix2_spi_master_data_transfer tr );
      if( ! tr.randomize() with {
	 slave_id == m_slave_id;
	 ! ( address inside {`MATRIX_PROGRAMMING_ADDRESS} ); }  )
	`uvm_error("RAND_ERROR", "Randomisation failed");
   endtask // randomize_tr
   
endclass // CLICpix2_spi_top_sequence

//Class: CLICpix2_spi_readout
//The sequence launches readout.
class CLICpix2_spi_readout extends CLICpix2_spi_top_sequence;
   
   `uvm_object_utils_begin(CLICpix2_spi_readout)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_spi_readout> with the given ~name~.
   function new(string name="CLICpix2_spi_readout");
      super.new(name);
   endfunction // new

   //Function: pre_randomize
   function void pre_randomize();
      super.pre_randomize();
      count = 1;
   endfunction // pre_randomize

   //Task: randomize_tr
   virtual task randomize_tr( CLICpix2_spi_master_data_transfer tr );
      if( ! tr.randomize() with {
	 slave_id == m_slave_id;
	 address == `READOUT_ADDRESS; } )
	`uvm_error("RAND_ERROR", "Randomisation failed");
   endtask // randomize_tr
   
endclass // CLICpix2_spi_readout


//Class: CLICpix2_spi_DummyAddress_seq
//The sequence generates access to registers outside CLICpix2 address space.
class CLICpix2_spi_DummyAddress_seq extends CLICpix2_spi_top_sequence;
   
   `uvm_object_utils_begin(CLICpix2_spi_DummyAddress_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_spi_DummyAddress_seq> with the given ~name~.
   function new(string name="CLICpix2_spi_DummyAddress_seq");
      super.new(name);
   endfunction // new

   //Task: randomize_tr
   virtual task randomize_tr( CLICpix2_spi_master_data_transfer tr );
      tr.CLICpix2_valid_address_constraint.constraint_mode(0);
      if( ! tr.randomize() with {
	 slave_id == m_slave_id;
	 ! ( address inside {`CLICPIX2_REGISTERS_VALID_ADDRESSES} ); } )
	`uvm_error("RAND_ERROR", "Randomisation failed");
   endtask // randomize_tr
   
endclass // CLICpix2_spi_DummyAddress_seq


//Class: CLICpix2_spi_MatrixConfig_seq
//The sequence translates <CLICpix2_Matrix_sequence_item> to SPI traffic.
class CLICpix2_spi_MatrixConfig_seq extends CLICpix2_spi_top_sequence;

   //Variable: matrix
   //It hold the actual matrix configuration.
   rand CLICpix2_Matrix_sequence_item matrix;

   //Variable: confStream
   //Configuration stream
   protected CLICPIX2_CONIGURATION_SPI_STREAM confStream;

   `uvm_object_utils_begin(CLICpix2_spi_MatrixConfig_seq)
   `uvm_object_utils_end

   //Function: new
   //Creates a new <CLICpix2_spi_MatrixConfig_seq> with the given ~name~.
   function new(string name="CLICpix2_spi_MatrixConfig_seq");
      super.new(name);
      matrix = CLICpix2_Matrix_sequence_item::type_id::create("matrix");
   endfunction // new

   //Function: pre_randomize
   //It disables <tr> randomisation.
   function void pre_randomize();
      super.pre_randomize();
      CLICPIX2_count_constraint.constraint_mode(0);
      matrix.knob.rand_mode(0);
      matrix.knob = CLICpix2_Matrix_sequence_item::CONFIGURATION;
      matrix.readout_constaints.constraint_mode(0);
   endfunction // pre_randomize

   //Task: body
   //Add dead time of 16 clock cycles after matrix configuration and sets <p_sequencer.matrixConfigured>
   virtual task body();
      spi_vip_config#(SPI_SS_WIDTH) cfg = spi_vip_config#(SPI_SS_WIDTH)::get_config(m_sequencer);
      super.body();
      repeat(16)
	cfg.wait_for_clock();
      p_sequencer.matrixConfigured = 1'b1;
   endtask // body


   //Task: randomize_tr
   virtual task randomize_tr( CLICpix2_spi_master_data_transfer tr );

      spi_vip_config#(SPI_SS_WIDTH) cfg = spi_vip_config#(SPI_SS_WIDTH)::get_config(m_sequencer);

      if( ! tr.randomize() with {
	 slave_id == m_slave_id;
	 address == `MATRIX_PROGRAMMING_ADDRESS;
         data == confStream[SPI_index]; })
	`uvm_error("RAND_ERROR", "Randomisation failed");

      //Add delay between configuration words.
      repeat( $urandom_range(20, 0) )
	cfg.wait_for_clock();
   endtask // randomize_tr
   
   //Function: post_randomize
   //Basing on ~matrix~ it generates <confStream>.
   function void post_randomize();
      CLICPIX2_CONIGURATION_SPI_STREAM confStream;
      
      getConigurationSPIArray(confStream);
      count = $size(confStream);

      super.post_randomize();
   endfunction // post_randomize

   //Function: getConigurationSPIArray
   //It extracts from <matrix> array of SPI words containing configuration
   function void getConigurationSPIArray(ref CLICPIX2_CONIGURATION_SPI_STREAM confStream);

      //64 bit words inserted to double-columns
      bit [63:0] confStream_64 [ $size(CLICPIX2_CONIGURATION_SPI_STREAM)/8 ];
      PIXEL_T matrix_dc [CLICPIX2_ROW *2] [CLICPIX2_COL /2]; //stores configuration of the double columns
      int unsigned confStream_64_index = 0; //index used to navigate through confStream64


      foreach(matrix.pixels[r,c]) begin
	 if(c%2) begin //odd column
	 end
	 else begin//
	    if(r%2) begin //odd row
	       matrix_dc[2*r][c/2] = matrix.pixels[r][c+1]; //move left to right
	       matrix_dc[2*r+1][c/2] = matrix.pixels[r][c];
	    end
	    else begin   //even row
	       matrix_dc[2*r][c/2] = matrix.pixels[r][c];   //move right to left
	       matrix_dc[2*r+1][c/2] = matrix.pixels[r][c+1];
	    end
	 end
      end

      confStream_64[confStream_64_index++] = '0;    //end of column don't care value
      
      for(int r = 0; r < $size(matrix_dc,1); r++) begin
	 if(r%16 == 0)                 //add super-pixel every 16 pixels
	   confStream_64[confStream_64_index++] = '0; //don't care
	 for(int b = $bits(PIXEL_T) - 1; b >= 0; b--) begin 
	    for(int c = 0; c < $size(confStream_64,2); c++)
	      confStream_64[confStream_64_index][c] = matrix_dc[r][c][b];
	    confStream_64_index++;
	 end
      end
      confStream_64[confStream_64_index++] = '0;    //last don't care bits

      
      //Repack confStream_64 to confStream
      foreach(confStream_64[i])
	for(int unsigned confStream_64_slice = 0; confStream_64_slice < 8 ; confStream_64_slice++)
	  confStream[i*8+confStream_64_slice] = confStream_64[i][8*confStream_64_slice +:8];

   endfunction // getConigurationSPIArray

   
   constraint matrixConstraint{
      matrix.knob == CLICpix2_Matrix_sequence_item::CONFIGURATION;
   }
   
endclass // CLICpix2_spi_MatrixConfig_seq


//Class: CLICpix2_spi_MatrixConfig_readoutControl_seq
//The sequence performs CLICpix configuration.
class CLICpix2_spi_MatrixConfig_readoutControl_seq extends CLICpix2_spi_MatrixConfig_seq;

   //Variable: readoutControlRegister
   CLICpix2_spi_master_data_transfer readoutControlRegister;
   
   `uvm_object_utils_begin(CLICpix2_spi_MatrixConfig_readoutControl_seq)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_spi_MatrixConfig_readoutControl_seq> with the given ~name~.
   function new(string name="CLICpix2_spi_MatrixConfig_readoutControl_seq");
      super.new(name);
   endfunction // new

   //Task: body
   //Add readoutControlRegister in advance.
   virtual task body();

      spi_vip_config#(SPI_SS_WIDTH) cfg = spi_vip_config#(SPI_SS_WIDTH)::get_config(m_sequencer);
      cfg.wait_for_clock();

      readoutControlRegister = CLICpix2_spi_master_data_transfer::type_id::create("readoutControlRegister") ;

      start_item( readoutControlRegister );
      if( ! readoutControlRegister.randomize() with {
	 slave_id == m_slave_id;
	 address == `READOUT_CONTROL_REGISTER_ADDRESS;
	 data[ `READOUT_CONTROL_REGISTER_COMP_BIT ] == 0;
	 data[ `READOUT_CONTROL_REGISTER_SP_COMP_BIT ] == 0; })
	
	`uvm_error("RAND_ERROR", "Randomisation failed");

      finish_item( readoutControlRegister );

      super.body();
   
   endtask // body
   
endclass // CLICpix2_spi_MatrixConfig_readoutControl_seq

//Class: CLICpix2_spi_ReadoutConfig
//The sequence tries different configuration settings which influences readout
class CLICpix2_spi_ReadoutConfig extends CLICpix2_spi_top_sequence;


   //Variable: readoutControlRegisterEnforce
   bit readoutControlRegisterEnforce = 0;
   //Variable: readoutControlRegisterEnforceReg
   rand bit [7:0] readoutControlRegisterEnforceReg = 0;

   //Constraint: CLICpix2_RCR_constraint
   constraint CLICpix2_RCR_constraint{
      ( readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] == 2'b00 ) -> ( readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b01 );
      ( readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] == 2'b01 ) -> ( readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b10 );
      ( readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] == 2'b10 ) -> ( readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_PARAL_COLS_BIT] == 2'b11 );
      readoutControlRegisterEnforceReg[`READOUT_CONTROL_REGISTER_CLK_DIV_BIT] inside { 2'b00, 2'b01, 2'b10 };
   }

   
   //Variable: globalControlRegisterEnforce
   bit globalControlRegisterEnforce = 0;
   //Variable: globalControlRegisterEnforceReg
   rand bit [7:0] globalControlRegisterEnforceReg = 0;
   
   `uvm_object_utils_begin(CLICpix2_spi_ReadoutConfig)
   `uvm_object_utils_end
   
   //Function: new
   //Creates a new <CLICpix2_spi_ReadoutConfig> with the given ~name~.
   function new(string name="CLICpix2_spi_ReadoutConfig");
      super.new(name);
   endfunction // new

   //Constraint: CLICPIX2_spiReadoutConfig_constraint
   constraint CLICPIX2_count_constraint{
      count == 2;
   }
   
   //Task: randomize_tr
   virtual task randomize_tr( CLICpix2_spi_master_data_transfer tr );
      case( SPI_index )
	0: begin
	   if( ! tr.randomize() with {
	      slave_id == m_slave_id;
              if(readoutControlRegisterEnforce){
                 data == readoutControlRegisterEnforceReg;}
	      address == `READOUT_CONTROL_REGISTER_ADDRESS; }  )
	     `uvm_error("RAND_ERROR", "Randomisation failed");
	end
	1: begin
	   if( ! tr.randomize() with {
	      slave_id == m_slave_id;
	      address == `GLOBAL_CONTROL_REGISTER; //different clock divider for TOT measurement
              if(globalControlRegisterEnforce) {
                data == globalControlRegisterEnforceReg;}
              if(!globalControlRegisterEnforce) {
                data[`GLOBAL_CONTROL_REGISTER_TP_GEN_EN] == 1'b0; }}) //do not enable TP generator
	     `uvm_error("RAND_ERROR", "Randomisation failed");
	   end
      endcase // case ( SPI_index )
   endtask // randomize_tr

      
endclass // CLICpix2_spi_ReadoutConfig

