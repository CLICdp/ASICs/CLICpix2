//                              -*- Mode: Verilog -*-
// Filename        : genericTest.sv
// Description     : The generic test of the CLICpix2.
// Author          : Adrian Fiergolski
// Created On      : Wed Jan 27 18:04:03 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: genericTest
//It is the generic test, which doesn't run any sequence on any sequencer.
//Can be used as a base class of Test executed in <CLICpix2_env>.
class genericTest extends uvm_test;

   //Variable: env
   //CLICpix2 environment. 
   CLICpix2_env env;

   //Variable: cfg
   //CLICpix2 configuration (<CLICpix2_config>)
   CLICpix2_config cfg;

   `uvm_component_utils_begin(genericTest)
      `uvm_field_object(env, UVM_DEFAULT | UVM_REFERENCE)
      `uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <genericTest> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Group: Run 
   
   //Function: build_phase
   //
   //It creates and configures the environment.
   //The function calls the subsequent ~*_config~ functions.
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);

      //Create the environment
      env = CLICpix2_env::type_id::create("env",this);

      //Create the configuration
      cfg = CLICpix2_config::type_id::create("CLICpix2 configuration", this);
      cfg.createDefault(env);

      //Assign virtual interfaces
      if(!uvm_config_db #( vCLICpix2_ClockReset )::get( this , "", "clockReset_interface" , cfg.clockReset_inter ))
	`uvm_error("Config Error" , "uvm_config_db #( vCLICpix2_ClockReset )::get cannot find interface CLICpix2_ClockReset" );

      if(!uvm_config_db #( vCLICpix2_outputs )::get( this , "", "outputs_interface" , cfg.outputs_inter ))
	`uvm_error("Config Error" , "uvm_config_db #( vCLICpix2_outputs )::get cannot find interface CLICpix2_outputs" );

      if(!uvm_config_db #( vCLICpix2_Matrix_inter )::get( this , "", "matrix_interface" , cfg.matrix_inter ))
	`uvm_error("Config Error" , "uvm_config_db #( vCLICpix2_Matrix_inter )::get cannot find interface CLICpix2_matrix_inter" );

      if(!uvm_config_db #( vSerDes_inter )::get( this , "", "serial_interface" , cfg.serial_inter ))
	`uvm_error("Config Error" , "uvm_config_db #( vSerDes_inter )::get cannot find interface SerDes_inter" );

      if(!uvm_config_db #( vSPI_inter )::get( this , "", "spi_interface" , cfg.spi_inter ))
	`uvm_error("Config Error" , "uvm_config_db #( vSPI_inter )::get cannot find interface spi" );

      
      //Configure environment's agents
      spi_config(cfg.spiCfg);
      serial_config(cfg.serialCfg);
      charge_config(cfg.chargeCfg);
      
      //Configure CLICpix2's scoreboard
      CLICpix2_score_config(cfg.scoreCfg);
      
      //set the configuration
      set_config_object( "env", "cfg", cfg, .clone(0) );
   endfunction // build_phase

   //Function: connect_phase
   //It connects the SPI scoreboard to the Readout scoreboard (register values).
   //In perfect scenario, the verification environment should come with register model.
   virtual function void connect_phase(uvm_phase phase);
      CLICpix2_Readout_scoreboard_config cfg_readout_;
      CLICpix2_SPI_scoreboard_config cfg_spi_;
      super.connect_phase(phase);
      assert( $cast( cfg_readout_, cfg.scoreCfg.sub_scoreboards["Readout_scoreboard"].cfg ) );
      assert( $cast( cfg_readout_.spi_scoreboard, env.scoreboard.scoreboards["SPI_scoreboard"] ) );
      assert( $cast( cfg_readout_.spi_sqr, this.env.spi_env.sqr.sqr["master"] ) );
      
      
      assert( $cast( cfg_spi_, cfg.scoreCfg.sub_scoreboards["SPI_scoreboard"].cfg ) );
      assert( $cast( cfg_spi_.spi_sqr, this.env.spi_env.sqr.sqr["master"] ) );
   endfunction // connect_phase

   //Function: end_of_elaboration
   //
   //Implement UVM end of elaboration phase.
   //Prints topology of the test ans set the sequences.
   virtual function void end_of_elaboration_phase(uvm_phase phase);
      uvm_top.print_topology();
   endfunction

   //Task: run_phase
   //It calls:
   //- <wait_for_CLICpix2>
   //and later in parallel
   //- <start_CLICpix2_sequence>
   //- <timeout>
   virtual task run_phase(uvm_phase phase);
      phase.raise_objection(this, "Start test");

      wait_for_CLICpix2();
      fork
	 start_CLICpix2_sequence(phase);
	 timeout();
      join_any

      phase.drop_objection(this,"Test done");
   endtask // run_phase

   //Task: wait_for_CLICpix2
   //Task should wait for the CLICpix2 to become functional.
   //It runs reset sequence.
   extern virtual task wait_for_CLICpix2();

   //Task: start_CLICpix2_sequence
   //User should spawn in this task <CLICpix2_seq>s
   virtual task start_CLICpix2_sequence(uvm_phase phase);
      `uvm_warning("start_CLICpix2_sequence", "No sequence to launch");
   endtask // start_CLICpix2_sequence

   //Task: timeout
   //Timeout of the whole test. 
   //By default: 100us.
   virtual task timeout();
      # 100us;
   endtask // timeout

   //Group: CLICpix2 configuration

   //Function: CLICpix2_score_config
   //User should configure CLICpix2's scoreboard in this function.
   virtual function void CLICpix2_score_config(CLICpix2_scoreboard_config cfg);
      CLICpix2_SPI_scoreboard_config spi_cfg;
      CLICpix2_Readout_scoreboard_config readout_cfg;
      CLICpix2_MatrixConfiguration_scoreboard_config matrix_cfg;
      BasicBlocks_pkg::subscoreboard_config::port_assingment_config readoutPorts[3];
	
      //SPI
      spi_cfg = CLICpix2_SPI_scoreboard_config::type_id::create("spi_cfg");
      spi_cfg.resetInter = this.cfg.clockReset_inter;
      cfg.add_subscoreboard( "SPI_scoreboard", CLICpix2_SPI_Coverage_scoreboard::get_type(), spi_cfg,
			     "SPI.registers", "SPI.registers");

      //MatrixConfigration
      matrix_cfg = CLICpix2_MatrixConfiguration_scoreboard_config::type_id::create("matrix_cfg");
      matrix_cfg.matrixInter = this.cfg.matrix_inter;
      cfg.add_subscoreboard( "MatrixConfiguration_scoreboard", CLICpix2_MatrixConfiguration_Coverage_scoreboard::get_type(), matrix_cfg,
			     "SPI.config", "SPI.config");

      //Readout
      readout_cfg = CLICpix2_Readout_scoreboard_config::type_id::create("readout_cfg");
      readout_cfg.resetInter = this.cfg.clockReset_inter;
      readout_cfg.matrixInter= this.cfg.matrix_inter;
      readoutPorts = '{ '{"SER.master", "SER.master"},  '{"CHA.master", "CHA.master"}, '{"SPI.config", "SPI.config"} };
      cfg.add_subscoreboard_array( "Readout_scoreboard", CLICpix2_Readout_Coverage_scoreboard::get_type(), readout_cfg, readoutPorts );

   endfunction // CLICpix2_score_config
      
   //Group SPI configuration

   //Function: spi_config
   //In this function user should configure SPI environment.
   extern virtual function void spi_config(ref SPI_config cfg);

   //Function: spi_master_config
   //In this function user should configure SPI master agent.
   extern virtual function void spi_master_config(ref spi_CLICpix2_vip_config cfg);
  
   //Function: spi_slave_config
   //In this function user should configure SPI slave agent.
   extern virtual function void spi_slave_config(ref spi_CLICpix2_vip_config cfg);

   //Function: spi_common_config
   //In this function user should configure include common parameters of SPI agents.
   extern virtual function void spi_common_config(ref spi_CLICpix2_vip_config cfg);

   //Function: spi_monitor_config
   //In this function user should configure monitor of the SPI environment.
   extern virtual function void spi_monitor_config(ref VIP_monitor_config cfg);

   //Group Serial configuration
   
   //Function: serial_config
   //In this function user should configure Serial environment.
   extern virtual function void serial_config(ref SerDes_config cfg);

   //Function: serial_monitor_config
   //In this function user should configure monitor of the serial agent.
   extern virtual function void serial_monitor_config(ref SerDes_monitor_config cfg);

   //Function: charge_config
   //In this function user should configure charge injection agent.
   extern virtual function void charge_config(ref CLICpix2_ChargeInjection_config cfg);
   
   //Function: charge_monitor_config
   //In this function user should configure monitor of the charge agent.
   extern virtual function void charge_monitor_config(ref CLICpix2_ChargeInjection_monitor_config cfg);

   
endclass // genericTest


task genericTest::wait_for_CLICpix2();
   CLICpix2_reset_seq reset = CLICpix2_reset_seq::type_id::create("Reset");
   if( ! reset.randomize() )
     `uvm_error("RAND_ERROR", "Randomisation failed");
   reset.start( env.sqr );
 
endtask // wait_for_CLICpix2


function void genericTest::spi_config(ref SPI_config cfg);

   spi_CLICpix2_vip_config master = spi_CLICpix2_vip_config::type_id::create("master", env);
   spi_CLICpix2_vip_config slave = spi_CLICpix2_vip_config::type_id::create("slave", env);
   
   spi_master_config(master);
   spi_slave_config(slave);
   spi_monitor_config(cfg.monCfg);
		      
   cfg.agentCfg["master"] = master;
   cfg.agentCfg["slave"] = slave;

endfunction // spi_config

function void genericTest::spi_master_config(ref spi_CLICpix2_vip_config cfg);

   spi_common_config(cfg);

   cfg.agent_cfg.is_active   = 1;
   cfg.agent_cfg.agent_type  = SPI_MSTR;
   cfg.agent_cfg.en_cvg      = 1;  


   //Enable coverage
   cfg.agent_cfg.en_cvg = 1;
   cfg.set_coverage_instance_name("master_cov");

   // Set monitor item
   void'(cfg.set_monitor_item( "trans_ap" , 
			       CLICpix2_spi_master_data_transfer::type_id::get() ));
   
   // Adds a listener on the master side for reporting purposes 
   // ( don't need one on the slave side as well ).
   cfg.set_analysis_component( "trans_ap" , "listener" ,
			       mvc_item_listener #( CLICpix2_spi_master_data_transfer )::get_type() );

   uvm_factory::get().set_inst_override_by_type(BasicBlocks_pkg::VIP_generic_monitor::get_type(), 
						CLICpix2_MatrixConfiguration_monitor::get_type(),
						"uvm_test_top.env.spi_env.monitor.config");
endfunction // spi_master_config

function void genericTest::spi_slave_config(ref spi_CLICpix2_vip_config cfg);

   spi_common_config(cfg);

   cfg.agent_cfg.is_active   = 0;
   cfg.agent_cfg.agent_type  = SPI_SLV;

   cfg.agent_cfg.en_cvg      = 0;  
   cfg.m_my_slave_id = 0;

   //Enable coverage
   cfg.agent_cfg.en_cvg = 1;
   cfg.set_coverage_instance_name("slave_cov");

   // Set monitor item
   void'(cfg.set_monitor_item( "trans_ap" , 
			       CLICpix2_spi_master_data_transfer::type_id::get() ));

endfunction // spi_slave_config

function void genericTest::spi_common_config(ref spi_CLICpix2_vip_config cfg);
   longint clk_half_period;
   
   cfg.m_bfm = this.cfg.spi_inter;
   
   cfg.m_bfm.set_config_num_bits_in_transfer(16);
   cfg.m_bfm.set_config_baudrate_divisor(1);
   cfg.m_bfm.set_config_LSBFE(0); //MSB first
   cfg.m_bfm.set_config_CPOL(0);  //stay of the SCK in IDLE
   cfg.m_bfm.set_config_CPHA(0);  //capture data on the first clock transition.
   cfg.m_bfm.set_config_min_idle_time_btwn_xfer_in_cpha0(1); //inset SS pulse between two transfers
   
   // clk_half_period = QUESTA_MVC::questa_mvc_sv_convert_to_precision( 5, QUESTA_MVC::QUESTA_MVC_TIME_X);
   // cfg.m_bfm.set_config_clk_phase_shift(clk_half_period);
   // cfg.m_bfm.set_config_clk_init_value(0);
   // cfg.m_bfm.set_config_clk_1st_time(clk_half_period);
   // cfg.m_bfm.set_config_clk_2nd_time(clk_half_period);

   cfg.agent_cfg.num_samples = 2;
   cfg.agent_cfg.spi_mode    = SPI_MOTO;
   cfg.agent_cfg.spi_ti_submode = SPI_PRECEDE;
   cfg.agent_cfg.ext_clock = 1;
   cfg.agent_cfg.en_txn_ltnr = 0; //monitor's listener will print it anyway
   
endfunction // spi_common_config

function void genericTest::spi_monitor_config(ref VIP_monitor_config cfg);
   CLICpix2_MatrixConfiguration_monitor_config matrixMonitor_cfg = CLICpix2_MatrixConfiguration_monitor_config::type_id::create("matrixMonitor_cfg");
   
   cfg.add_submonitor("registers", CLICpix2_spi_master_data_transfer::get_type(), .associated_agent("master") );

   matrixMonitor_cfg.matrixInter = this.cfg.matrix_inter;
   cfg.add_submonitor("config", CLICpix2_spi_master_data_transfer::get_type(), matrixMonitor_cfg, "master" );
   uvm_factory::get().set_inst_override_by_type(mvc_pkg::mvc_sequencer::get_type(), 
					   SPI_CLICpix2_sqr::get_type(),
					   "uvm_test_top.env.spi_env.master_agent.sequencer");
endfunction // spi_monitor_config

function void genericTest::serial_config(ref SerDes_config cfg);
   
   cfg.inter = this.cfg.serial_inter;
   
   cfg.is_active = UVM_PASSIVE;
   
   SerDes_monitor::type_id::set_type_override( SerDes_monitor_8b10b::get_type() );

   cfg.monCfg.inter = cfg.inter;
   serial_monitor_config(cfg.monCfg);
   
endfunction // serial_config

function void genericTest::serial_monitor_config(ref SerDes_monitor_config cfg);
endfunction // charge_config

function void genericTest::charge_config(ref CLICpix2_ChargeInjection_config cfg);
   
   cfg.inter = this.cfg.matrix_inter;
   
   cfg.is_active = UVM_ACTIVE;
   
   cfg.monCfg.inter = cfg.inter;
   charge_monitor_config(cfg.monCfg);
   
endfunction // charge_config

function void genericTest::charge_monitor_config(ref CLICpix2_ChargeInjection_monitor_config cfg);
endfunction // charge_config

