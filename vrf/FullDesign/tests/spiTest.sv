//                              -*- Mode: Verilog -*-
// Filename        : spiTest.sv
// Description     : Test of the SPI interface.
// Author          : Adrian Fiergolski
// Created On      : Wed Feb 24 13:50:48 2016
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Class: spiTest
//Test performing SPI interface verification.
class spiTest extends genericTest;

   //Variable: NoOfPackets
   //Number of injected SPI transactions.
   int unsigned NoOfPackets = 30;

   
   `uvm_component_utils_begin(spiTest)
   `uvm_component_utils_end
   
   //Function: new
   //Creates a new <spiTest> with the given ~name~ and ~parent~.
   function new(string name="", uvm_component parent);
      super.new(name, parent);
   endfunction // new

   //Task: start_CLICpix2_sequence
   //It spawns <CLICpix2_SPI_seq>
   virtual task start_CLICpix2_sequence(uvm_phase phase);
      CLICpix2_SPI_seq spi = CLICpix2_SPI_seq::type_id::create("spi_master");
      if( ! spi.randomize() with  { NoOfPackets == local::NoOfPackets; } )
	`uvm_error("RAND_ERROR", "Randomisation failed");
      spi.start( env.sqr );

   endtask // start_CLICpix2_sequence

   //Task: timeout
   //Timeout of the whole test. 
   //By default: 1s.
   virtual task timeout();
      # 10s;
   endtask // timeout

   
endclass // spiTest
