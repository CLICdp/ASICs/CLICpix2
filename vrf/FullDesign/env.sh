#!/bin/bash

#### Mentor Graphics
# ModelSim
export MTI_VCO_MODE=64
export MENTOR_ROOTDIR=/cerneda/mentor/2017/afspecial/questa_sv_afv_10.6c/questasim/
export PATH=$PATH:$MENTOR_ROOTDIR/bin
export QUESTA_MVC_HOME=/cerneda/mentor/2017/afspecial/questa_verification_ip_10.6a/
export HDLMAKE_MODELSIM_PATH=$MENTOR_ROOTDIR/bin

#### Libre Office
export LIBREOFFICE_ROOTDIR=/cerneda/mentor/2017/afspecial/LibreOffice_5.4.0.3_Linux_x86-64/
export PATH=$PATH:$LIBREOFFICE_ROOTDIR/opt/libreoffice5.4/program/

#export LIBRARY_PATH=$LIBRARY_PATH:/usr/lib/x86_64-linux-gnu
#export DPI_NO_BUILTIN_GCC=1 #use for DPI local compiler

# add ReqTracer
export PATH=$PATH:/eda/mentor/2014-15/RHELx86/REQTRACER_2014.1/bin.pclinux

#Python2.7
export PATH=/cerneda/mentor/2017/afspecial/python2.7.14/bin:$PATH

#hdlmake
export HDLMAKE="$(pwd)/hdl-make"
if [ ! -d "$HDLMAKE" ]; then
    git clone git://ohwr.org/misc/hdl-make.git
    #temporay patch, until develop is pushed to some release
    git --git-dir=$HDLMAKE/.git  --work-tree=$HDLMAKE checkout d0d037d3c0db263f76b433180f460a069db962a5
fi

export hdlmake_path=$HDLMAKE/
hdlmake() {
    PYTHONPATH=$hdlmake_path python2.7 -m hdlmake "$@"
}
export -f hdlmake

#kinit
#aklog

### Start shell
echo "Working environment has been set"
bash
