# CLICpix2

## Introduction

CLICpix2 is a readout chip for a hybrid pixel detector targeted to vertex detector of the Compact LInear Collider (CLIC). The chip is the second version of the original CLICpix (aka [CLICpix1](https://cds.cern.ch/record/1610583/files/CERN-THESIS-2013-156.pdf) ), with some corrections and improvements; for instance, the pixel matrix has now 128x128 instead of 64x64 pixels as in the previous design.

### Content of the project:
 * RTL and netlist code of the CLICpix2 chip (exluding proprietary technology files)
 * UVM-based verification environment


## CLICpix2 user manual
The latest manual of the chip can be found [here](https://www.overleaf.com/read/bmcvcbvckksc).

## Verification (simulation)
The simulation environment depends on:
 * Questa from  Mentor Graphics (tested with version 10.5c_1)
 * Questa Verification IP from Mentor Graphics (tested with version 10.5b_1)
 * [HDLmake](http://www.ohwr.org/projects/hdl-make) for Makefile generation (enviornment should provide `hdlmake` command, check [example](vrf/FullDesign/env.sh)) 
    * Python 2.7 
 * [HDL Verification Library](https://gitlab.cern.ch/CLICdp/HDLVerificationLibrary)
 * Libre Office for translation of the verification plan from .ods to .xml format (tested with vesion 5.2)

After clone of the repository one needs to initialize the required submodules:
```
git submodule update --init --recursive
```
In order to run a single simulation eneter `vrf/FullDesign/sim/` directory and call one of the tests (files with *.do extension), i.e.
```
vsim -do spi.do &
```