`timescale 100ps/1ps
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/readout_control.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/end_of_column.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/DAC_control.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/control_logic.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/powerpulsing.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/pulse_generator.v"

module periphery (

//Basic I/O interfaces
	input wire clk,
	input wire reset_n,
	input wire power_pin,
	input wire shutter,
	input wire TPswitch,
	output wire clk_out,
	output wire dataout_serial,
	
//SPI-like interface
	input wire clk_slow, //also used for acquisition
	input wire chip_select,	
	input wire MOSI,
	output wire MISO,

//Signals to/from columns		
	input wire [63:0] datain_column,
	input wire [63:0] fastor,
	output wire [63:0] dataout_column,
	output wire [63:0] clk_column,
	output wire [63:0] clock_div0_multi,
	output wire [63:0] clock_div1_multi,
	output wire [63:0] use_compression_multi,
	output wire [63:0] use_cluster_compression_multi,
	output wire [63:0] polarity_multi,
	output wire [63:0] configure_matrix_multi,
	output wire [63:0] shift_multi,
	output wire [63:0] TPswitch_multi,
	output wire [63:0] shutter_multi,

	output wire [63:0] ppulse_enable_gated,
	output wire [63:0] ppulse_enable_gated_n,
	
//Signals to the analog periphery	
	output wire bgap_enable,
	output wire bgap_enable_n,
	output wire [2:0] bgap_control_R3,
	output wire [2:0] bgap_control_R1,
	
	output wire [7:0] bias_disc_N,
	output wire [7:0] bias_disc_P,
	output wire [7:0] bias_ikrum,
	output wire [7:0] bias_preamp,
	output wire [7:0] bias_DAC,
	output wire [7:0] bias_buffer,
	output wire [7:0] casc_preamp,
	output wire [7:0] casc_DAC,
	output wire [7:0] casc_Nmirror,
	output wire [7:0] vfbk,
	output wire [7:0] vthMSB,
	output wire [7:0] vthLSB,
	output wire [7:0] test_cap1_LSB,	//New signal
	output wire [7:0] test_cap1_MSB,
	output wire [7:0] test_cap2, //New signal
	output wire [7:0] bias_disc_N_OFF,
	output wire [7:0] bias_disc_P_OFF,
	output wire [7:0] bias_preamp_OFF,
	output wire [21:0] output_mux_DAC,	 //Updated size!
	
	output wire [7:0] bias_disc_N_n,
	output wire [7:0] bias_disc_P_n,
	output wire [7:0] bias_ikrum_n,
	output wire [7:0] bias_preamp_n,
	output wire [7:0] bias_DAC_n,
	output wire [7:0] bias_buffer_n,
	output wire [7:0] casc_preamp_n,
	output wire [7:0] casc_DAC_n,
	output wire [7:0] casc_Nmirror_n,
	output wire [7:0] vfbk_n,
	output wire [7:0] vthMSB_n,
	output wire [7:0] vthLSB_n,
	output wire [7:0] test_cap1_LSB_n,	//New signal
	output wire [7:0] test_cap1_MSB_n,	//New signal
	output wire [7:0] test_cap2_n, //New signal
	output wire [7:0] bias_disc_N_OFF_n,
	output wire [7:0] bias_disc_P_OFF_n,
	output wire [7:0] bias_preamp_OFF_n,
	output wire [21:0] output_mux_DAC_n	 //Updated size!
	);

//wire clk;//*/
//wire reset;//*/
wire enable_encoder;//
wire [7:0] datain_encoder;//
wire [1:0] parallel_bits, clk_divider_config;//
//wire shutter, TPswitch;
wire sync_8bits, sync_16bits;
wire pause, pause_delayed;//
wire [39:0] dataout;//
//wire SPI_output;
wire [7:0] programming_word;

//wire clk_slow;//*/
//wire clk_out;//
wire programming;//
wire carrier_extend;
//wire dataout_serial;//*/

//wire [63:0] datain_column, dataout_column;
wire [63:0] dataout_column_buffer, done_column;//*/
wire configure_matrix;//
wire [63:0] column_token, datain_eoc;
//wire [63:0] fastor, clk_column;//

//wire data_ready;
wire data_strobe;//*/
wire [4:0] ADDRESS;//
wire [7:0] DATA;//
wire use_compression, use_cluster_compression, shift, polarity;//*/
//wire power_pin, datain_serial;
wire [63:0] ppulse_enable;
//wire [63:0] ppulse_enable_gated, ppulse_enable_gated_n;//
wire [1:0] ppulse_divider, TOT_divider;//

//wire [7:0] bias_disc_N, bias_disc_P, bias_ikrum, bias_preamp, bias_DAC, bias_buffer, casc_preamp, casc_DAC;//*/
//wire [7:0] casc_Nmirror, vfbk, vthMSB, vthLSB, bias_disc_N_OFF, bias_disc_P_OFF, bias_preamp_OFF, test_cap1_LSB, test_cap1_MSB, test_cap2;//*/
//wire [7:0] bias_disc_N_n, bias_disc_P_n, bias_ikrum_n, bias_preamp_n, bias_DAC_n, bias_buffer_n, casc_preamp_n, casc_DAC_n;//*/
//wire [7:0] casc_Nmirror_n, vfbk_n, vthMSB_n, vthLSB_n, bias_disc_N_OFF_n, bias_disc_P_OFF_n, bias_preamp_OFF_n, test_cap1_LSB_n, test_cap1_MSB_n, test_cap2_n;//*/
wire [7:0] config_global, poweron_timer, poweroff_timer, readout_config;//
wire [7:0] pulsegen_counts_LSB, pulsegen_counts_MSB, pulsegen_delay_LSB, pulsegen_delay_MSB, dac_out_encoded;
wire [12:0] pulsegen_counts, pulsegen_delay;
wire TP_generated, PRBS_start;
//wire bgap_enable, bgap_enable_n;
//wire [2:0] bgap_control_R3, bgap_control_R1;

assign parallel_bits = readout_config[1:0];
assign clk_divider_config = readout_config[3:2];
assign use_compression = readout_config[4];
assign use_cluster_compression = readout_config[5];
assign ppulse_divider = poweron_timer[7:6];
assign polarity = config_global[2];
assign TOT_divider = config_global[1:0];
assign ppulse_enable_gated = poweroff_timer[6] ? 64'hFFFF_FFFF_FFFF_FFFF : ppulse_enable;
assign ppulse_enable_gated_n = ~ppulse_enable_gated;
assign pulsegen_counts = {pulsegen_counts_MSB[4:0], pulsegen_counts_LSB};
assign pulsegen_delay = {pulsegen_delay_MSB[4:0], pulsegen_delay_LSB};
assign bgap_enable = config_global[6];
assign bgap_enable_n = ~bgap_enable;
assign bgap_control_R3 = config_global[5:3];
assign bgap_control_R1 = 3'b111;


//integer i;
//wire [63:0] clock_div0_multi;
//wire [63:0] clock_div1_multi;
//wire [63:0] use_compression_multi;
//wire [63:0] use_cluster_compression_multi;
//wire [63:0] polarity_multi;
//wire [63:0] configure_matrix_multi;
//wire [63:0] shift_multi;
//wire [63:0] TPswitch_multi;
//wire [63:0] shutter_multi;

/*ADD FOUR 2-stage-ff resync fro
-reset (slow clock)
-reset (fast clock)
-shutter (slow clock)
-power (slow clock)

with this style (and set_false_path in the sdc file):
reg EN_fast_1, EN_fast;
always @(posedge clk) begin
	EN_fast_1 <= EN_slow;
	EN_fast <= EN_fast_1;
end
*/

reg shift_fast, shift_fast_1, reset_fast_1, reset_fast, reset_slow_1, reset_slow_2, reset_slow, shutter_slow_1, shutter_slow, power_slow_1, power_slow;
always @(posedge clk) begin
	reset_fast_1 <= reset_slow_2;
	reset_fast <= reset_fast_1;
	shift_fast_1 <= shift;
	shift_fast <= shift_fast_1;
end

always @(posedge clk_slow) begin
	reset_slow_1 <= reset_n;
	reset_slow <= reset_slow_1;
	reset_slow_2 <= reset_slow;
	shutter_slow_1 <= shutter;
	shutter_slow <= shutter_slow_1;
	power_slow_1 <= power_pin;
	power_slow <= power_slow_1;
end



periphery_control command_interpreter (
	.data(MOSI),
	.clk(clk_slow),
	.data_ready(chip_select),
	.data_strobe(data_strobe),
	.RESET(reset_slow),
	.ADDRESS(ADDRESS),
	.DATA(DATA),
	.bias_disc_N(bias_disc_N),
	.bias_disc_P(bias_disc_P),
	.bias_ikrum(bias_ikrum),
	.bias_preamp(bias_preamp),
	.bias_DAC(bias_DAC),
	.bias_buffer(bias_buffer),
	.casc_preamp(casc_preamp),
	.casc_DAC(casc_DAC),
	.casc_Nmirror(casc_Nmirror),
	.vfbk(vfbk),
	.vthMSB(vthMSB),
	.vthLSB(vthLSB),
	.test_cap1_LSB(test_cap1_LSB), //New signal
	.test_cap1_MSB(test_cap1_MSB), //New signal
	.test_cap2(test_cap2), //New signal
	.bias_disc_N_OFF(bias_disc_N_OFF),
	.bias_disc_P_OFF(bias_disc_P_OFF),
	.bias_preamp_OFF(bias_preamp_OFF),
	.dac_out_encoded(dac_out_encoded),
	.config_global(config_global),
	.poweron_timer(poweron_timer),
	.poweroff_timer(poweroff_timer),
	.pulsegen_counts_LSB(pulsegen_counts_LSB),
	.pulsegen_counts_MSB(pulsegen_counts_MSB),
	.pulsegen_delay_LSB(pulsegen_delay_LSB),  
	.pulsegen_delay_MSB(pulsegen_delay_MSB),  
	.readout_config(readout_config),
	.programming_word(programming_word),
	.SPI_out(MISO)
	);

pulse_generator pulse_generator (
	.clk(clk_slow), //slow clock
	.counts_setting(pulsegen_counts),
	.delay_setting(pulsegen_delay),
	.shutter(shutter_slow),
	.pulse_generated(TP_generated)
	);


dac_control dac_control(
	.clk(clk_slow),
	.data_ready(data_strobe),
	.ADDRESS(ADDRESS),
	.DATA(DATA),
	.reset(reset_slow),
	.bias_disc_N(bias_disc_N),
	.bias_disc_P(bias_disc_P),
	.bias_ikrum(bias_ikrum),
	.bias_preamp(bias_preamp),
	.bias_DAC(bias_DAC),
	.bias_buffer(bias_buffer),
	.casc_preamp(casc_preamp),
	.casc_DAC(casc_DAC),
	.casc_Nmirror(casc_Nmirror),
	.vfbk(vfbk),
	.vthMSB(vthMSB),
	.vthLSB(vthLSB),
	.test_cap1_LSB(test_cap1_LSB), //New signal
	.test_cap1_MSB(test_cap1_MSB), //New signal
	.test_cap2(test_cap2), //New signal
	.bias_disc_N_OFF(bias_disc_N_OFF),
	.bias_disc_P_OFF(bias_disc_P_OFF),
	.bias_preamp_OFF(bias_preamp_OFF),
	.output_mux_DAC(output_mux_DAC),    //Updated size!

	.bias_disc_N_n(bias_disc_N_n),
	.bias_disc_P_n(bias_disc_P_n),
	.bias_ikrum_n(bias_ikrum_n),
	.bias_preamp_n(bias_preamp_n),
	.bias_DAC_n(bias_DAC_n),
	.bias_buffer_n(bias_buffer_n),
	.casc_preamp_n(casc_preamp_n),
	.casc_DAC_n(casc_DAC_n),
	.casc_Nmirror_n(casc_Nmirror_n),
	.vfbk_n(vfbk_n),
	.vthMSB_n(vthMSB_n),
	.vthLSB_n(vthLSB_n),
	.test_cap1_LSB_n(test_cap1_LSB_n), //New signal
	.test_cap1_MSB_n(test_cap1_MSB_n), //New signal
	.test_cap2_n(test_cap2_n), //New signal
	.bias_disc_N_OFF_n(bias_disc_N_OFF_n),
	.bias_disc_P_OFF_n(bias_disc_P_OFF_n),
	.bias_preamp_OFF_n(bias_preamp_OFF_n),
	.output_mux_DAC_n(output_mux_DAC_n),    //Updated size!
	
	.dac_out_encoded(dac_out_encoded),
	.config_global(config_global),
	.poweron_timer(poweron_timer),
	.poweroff_timer(poweroff_timer),
	.pulsegen_counts_LSB(pulsegen_counts_LSB),	//New
	.pulsegen_counts_MSB(pulsegen_counts_MSB),	//New
	.pulsegen_delay_LSB(pulsegen_delay_LSB),    //New
	.pulsegen_delay_MSB(pulsegen_delay_MSB),    //New
	.readout_config(readout_config),    //New signal
	.programming_word(programming_word)
	);

ppulse_enabler ppulse (
	.clk(clk_slow),
	.count_start(poweron_timer[5:0]),
	.count_stop(poweroff_timer[5:0]),
	.enable(ppulse_enable),
	.reset(reset_slow),
	.power(power_slow),
	.select(ppulse_divider)
	);

//Logic to combine programming and shift_data
reg programming_divider;
always @(posedge clk_slow) begin
  if (~reset_slow)
    programming_divider <= 0;
  else
    if (configure_matrix && ~programming)
      programming_divider <= 0;
    else if(programming && ~configure_matrix)
      programming_divider <= 1;
end

/*
wire programming_divider;
assign programming_divider = programming || shift_programming; //from readout_control
*/

readout readout_control (
	.clk(clk_slow),
	.clk_divided(clk_out),		   
	.reset_fast(reset_fast),
	.reset_slow(reset_slow),
	.parallel_columns(parallel_bits),
	.programming_data(MOSI),
	.programming_word(programming_word),
	.datain(datain_eoc),	//64-bit
	.column_done(done_column),	//64-bit
	.pause(pause_delayed),
	.sync_8bits(sync_8bits),
	.sync_16bits(sync_16bits),
	.configure_matrix(configure_matrix),	//At the end of the programming, to latch data
	.enable_encoder(enable_encoder),		//Enable signal for the 8b10b encoder
	.column_token(column_token),	//Enable for the EOC block
	.column_dataout(dataout_column_buffer),	//Programming data to columns
	.dataout(datain_encoder),
	.send_clk(send_clk),
	
	.start_PRBS(PRBS_start),
	.data_ready(data_strobe),
	.ADDRESS(ADDRESS),
//	.DATA(DATA),
	.shift_data(shift),
	.programming(programming),
	.carrier_extend(carrier_extend)
	);

/*
wire [63:0] datain_column_gated;
assign datain_column_gated = shutter_slow ? 64'b0 : datain_column[63:0];
*/
wire TP_gated;
assign TPgated = config_global[7] ? TP_generated : TPswitch;

reg send_clk_fast_1, send_clk_fast;
always @(posedge clk) begin
	send_clk_fast_1 <= send_clk;
	send_clk_fast <= send_clk_fast_1;
end

genvar j;
generate
	for (j=0; j < 64; j=j+1) begin
	end_of_column eoc (
	.clkin_divided(clk_out), 	//I might need to add a clock_undelayed to be sent to the matrix to equalize delay
	.clkin_slow(clk_slow), 
	.select_clock(ppulse_enable[j]), 	//only for acquisition. Slow clock (delayed) is handled by the clock divider for programming
	.shift_data(shift_fast),	//both for programming and readout
	.datain_column(datain_column[j]),
	.fastor(fastor[j]),
	.token(column_token[j] || send_clk_fast),    //No need for pause signals, only advance while token is 1
	.use_compression(use_compression),
	.use_cluster_compression(use_cluster_compression),
	.reset(reset_slow),
	.dataout(datain_eoc[j]),
	.column_done(done_column[j]),
	.clkout (clk_column[j]),
	.clock_div0_multi(clock_div0_multi[j]),
	.clock_div1_multi(clock_div1_multi[j]),
	.use_compression_multi(use_compression_multi[j]),
	.use_cluster_compression_multi(use_cluster_compression_multi[j]),
	.polarity_multi(polarity_multi[j]),
	.configure_matrix_multi(configure_matrix_multi[j]),
	.shift_multi(shift_multi[j]),
	.TPswitch_multi(TPswitch_multi[j]),
	.shutter_multi(shutter_multi[j]),
	.clock_div0(TOT_divider[0]),
	.clock_div1(TOT_divider[1]),
	.polarity(polarity),
	.configure_matrix(configure_matrix),
	.TPswitch(TPgated),
	.shutter(shutter)
	);
	end
endgenerate

encoder encoder (
	.clk (clk_out),
	.reset (reset_fast),
	.enable (enable_encoder),
	.sync_8bits(sync_8bits),
	.sync_16bits(sync_16bits),
	.datain (datain_encoder),
	.parallel_bits (parallel_bits),
  .clk_divider_config(clk_divider_config),
	.PRBS_start(PRBS_start),	//
	.carrier_extend(carrier_extend),
	.pause (pause),
	.pause_delayed(pause_delayed),
	.dataout (dataout)
	);

clock_divider clock_divider (
	.clk (clk),
	.clk_slow (clk_slow),
	.reset(reset_slow),
	.control (clk_divider_config),
	.programming (programming_divider),
	.clkout (clk_out)
	);

serializer serializer (
	.clk (clk),
	.reset (reset_fast),
	.datain (dataout),
	.dataout (dataout_serial)
	);
  
genvar k;
generate
	for (k=0; k < 64; k=k+1) begin
	CKBD24 dataout_multibuffer (.I (dataout_column_buffer[k]), .Z (dataout_column[k]));
	end
endgenerate
endmodule
