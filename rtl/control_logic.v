`timescale 100ps/1ps

// Command interpreter module.
// It reads the input data serially and stores them in the ADDRESS and DATA registers when it receives a strobe signal

module periphery_control (
	input data,
	input clk,
	input data_ready,	//Now active low
	output reg data_strobe,
	input RESET,
	output reg [4:0] ADDRESS,
	output reg [7:0] DATA,
	output wire SPI_out,
	
	input [7:0] bias_disc_N,
	input [7:0] bias_disc_P,
	input [7:0] bias_ikrum,
	input [7:0] bias_preamp,
	input [7:0] bias_DAC,
	input [7:0] bias_buffer,
	input [7:0] casc_preamp,
	input [7:0] casc_DAC,
	input [7:0] casc_Nmirror,
	input [7:0] vfbk,
	input [7:0] vthMSB,
	input [7:0] vthLSB,
	input [7:0] test_cap1_LSB,
	input [7:0] test_cap1_MSB,
	input [7:0] test_cap2,
	input [7:0] bias_disc_N_OFF,
	input [7:0] bias_disc_P_OFF,
	input [7:0] bias_preamp_OFF,
	input [7:0] config_global,
	input [7:0] poweron_timer,
	input [7:0] poweroff_timer,
	input [7:0] dac_out_encoded,
	input [7:0] pulsegen_counts_LSB,
	input [7:0] pulsegen_counts_MSB,
	input [7:0] pulsegen_delay_LSB,
	input [7:0] pulsegen_delay_MSB,
	input [7:0] readout_config,	
	input [7:0] programming_word
	);

//reg [4:0] ADDRESS;
//reg [7:0] DATA;

reg [4:0] state;	//I have to go to 4 bits to get to 16 states (grr...)
reg [4:0] next_state;
reg address_enable;
reg load_enable;

reg [2:0]spare_bits;

reg load_data, load_address, output_data, load_output;
//reg data_strobe;

reg [7:0] spi_output;
//wire SPI_out;
assign SPI_out = spi_output[7];

always @(*) begin

//        if (~RESET) begin
//		data_strobe = 0;
//		load = 0;
//        end else begin
                case (state)
                0: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 0;
                      load_data = 0;
                      load_enable = 0;
                   end
                1: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 0;
                      load_data = 0;
                      load_enable = 0;
                   end
                2: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 1;
                      load_data = 0;
                      load_enable = 0;
                   end
                3: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 1;
                      load_data = 0;
                      load_enable = 0;
                   end
                4: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 1;
                      load_data = 0;
                      load_enable = 0;
                   end
                5: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 1;
                      load_data = 0;
                      load_enable = 0;
                  end
                6: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 1;
                      load_data = 0;
                      load_enable = 0;
                  end
                7: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 0;
                      load_data = 0;
                      load_enable = 1;
                   end
                8: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = address_enable;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                   end
                9: begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                   end
                10:begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                   end
                11:begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                  end
                12: begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                  end
                13: begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                   end
                14: begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                   end
                15: begin
                      data_strobe = 0;
                      output_data = address_enable;
                      load_output = 0;
                      load_address = 0;
                      load_data = address_enable;
                      load_enable = 0;
                  end
                16: begin
                      data_strobe = address_enable;
                      output_data = 0;
                      load_output = 0;
                      load_address = 0;
                      load_data = 0;
                      load_enable = 0;
                  end
                default: begin
                      data_strobe = 0;
                      output_data = 0;
                      load_output = 0;
                      load_address = 0;
                      load_data = 0;
                      load_enable = 0;
                   end
                endcase
//        end
end
/*
always @(posedge clk) begin
	if (~RESET) begin
                ADDRESS <=#0.1 5'b00000;
                DATA <=#0.1 8'b00000000;
        end else if (load) begin 
                ADDRESS <=#0.1 {ADDRESS[3:0], DATA[7]};
                DATA <=#0.1 {DATA[6:0], data}; 
        end
end
*/
/*
always @(posedge clk) begin	//ADDRESS
	if (~RESET) begin
                address_enable <=#0.1 'b0;
        end else if (load_enable) begin 
                address_enable <=#0.1 ~data;
        end
end
*/
always @(posedge clk) begin	//ADDRESS
	if (~RESET) begin
                ADDRESS <=#0.1 5'b00000;
        end else if (load_address) begin 
                ADDRESS <=#0.1 {ADDRESS[3:0], data};
        end
end

always @(posedge clk) begin	//DATA
	if (~RESET) begin
                DATA <=#0.1 8'b00000000;
        end else if (load_data) begin 
                DATA <=#0.1 {DATA[6:0], data};
        end
end


always @(posedge clk) begin
	if (~RESET) begin
		spi_output <= 8'b0;
	end else if (load_output) begin
		case (ADDRESS)
		        'b00101: begin
				spi_output <= bias_disc_N[7:0];
			   end
		        'b00110: begin
				spi_output <= bias_disc_P[7:0];
		           end
        		'b00111: begin
				spi_output <= bias_ikrum[7:0];
			   end
       			'b01000: begin
				spi_output <= bias_preamp[7:0];
			   end
		        'b01001: begin
				spi_output <= bias_DAC[7:0];
			   end
		        'b01010: begin
				spi_output <= bias_buffer[7:0];
			   end
		        'b01011: begin
				spi_output <= casc_preamp[7:0];
			   end
		        'b01100: begin
				spi_output <= casc_DAC[7:0];
			   end
		        'b01101: begin
				spi_output <= casc_Nmirror[7:0];
			   end
		        'b01110: begin
				spi_output <= vfbk[7:0];
			   end
		        'b10011: begin
				spi_output <= vthMSB[7:0];
			   end
		        'b10010: begin
				spi_output <= vthLSB[7:0];
			   end
		        'b11110: begin
			        spi_output <= config_global[7:0];
			   end
		        'b11000: begin
			        spi_output <= poweron_timer[7:0];
			   end
		        'b11001: begin
			        spi_output <= poweroff_timer[7:0];
			   end
		        'b10111: begin
			        spi_output <= dac_out_encoded[7:0]; //out_mux
			   end
			'b01111: begin
			        spi_output <= bias_disc_N_OFF[7:0];
			   end
		        'b10000: begin
			        spi_output <= bias_disc_P_OFF[7:0];
			   end
		        'b10001: begin
			        spi_output <= bias_preamp_OFF[7:0];
			   end
		        'b10100: begin
			        spi_output <= test_cap1_LSB[7:0];
			   end
		        'b10101: begin
			        spi_output <= test_cap1_MSB[7:0];
			   end
		        'b10110: begin
			        spi_output <= test_cap2[7:0];
			   end
		        'b11111: begin
			        spi_output <= readout_config[7:0];
			   end
		        'b11010: begin
			        spi_output <= pulsegen_counts_LSB[7:0];
			   end
		        'b11011: begin
			        spi_output <= pulsegen_counts_MSB[7:0];
			   end
		        'b11100: begin
			        spi_output <= pulsegen_delay_LSB[7:0];
			   end
		        'b11101: begin
			        spi_output <= pulsegen_delay_MSB[7:0];
			   end
		        'b00010: begin  //PROGRAMMING
				spi_output <= programming_word[7:0];
			   end
		        'b00001: begin	//READOUT
				spi_output <= 8'b0;
			   end
		        default: begin
				spi_output <= 8'b0;
		           end
		        endcase
	end else if (output_data) begin
		spi_output[7:0] <= {spi_output[6:0], 1'b0};
	end
end
		


always @(*) begin 
        if (state == 0 && data_ready == 0) begin
                next_state = 1;
        end else begin
                if (state > 0 && state < 16) begin
                        next_state = state + 1;
                end else begin
                        next_state = 0;
                end
        end
end

always @(posedge clk) begin
        case (state)
            0: spare_bits[0] = data;
            1: spare_bits[1] = data;
            7: spare_bits[2] = data;
        endcase
end

assign address_enable = ~(spare_bits[0] || spare_bits[1] || spare_bits[2]);

always @(posedge clk) begin
        if (~RESET) begin
                state <=#0.1 0;
        end else begin
                state <=#0.1 next_state;
        end
end


endmodule
