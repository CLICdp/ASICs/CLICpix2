module pulse_generator (
	input clk, //slow clock
	input [12:0] counts_setting,
	input [12:0] delay_setting,
	input shutter,
	output reg pulse_generated
	);


reg [12:0] counts, delay;
//reg pulse_generated;

always @(posedge clk) begin
	if (~shutter) begin
		delay <= 0;
	end else begin
		if (delay == delay_setting) begin
			delay <= 0;
		end else begin
			delay <= delay + 1;
		end
	end
end

always @(posedge clk) begin
	if (~shutter) begin
		counts <= 0;
		pulse_generated <= 0;
	end else begin
		if ((counts != counts_setting) && (delay == delay_setting)) begin
			if (pulse_generated == 0) begin
				pulse_generated <= 1;
			end else begin
				pulse_generated <= 0;
				counts <= counts + 1;
			end
		end
	end
end

endmodule
