module clock_divider (
	input clk,
	input clk_slow,
	input [1:0] control,
	input programming,
	input reset,
	output reg clkout);

//reg clkout, clkout2, clkout4, clkout8;
reg clkout2, clkout4, clkout8;
wire clkin, clkoutn2, clkoutn4, clkoutn8;

assign clkin = ~clk;
assign clkoutn2 = ~clkout2;
assign clkoutn4 = ~clkout4;
assign clkoutn8 = ~clkout8;

always @(posedge clkin or negedge reset) begin
if (~reset) begin
	clkout2 <= 0;
end
else begin
	clkout2 <= clkoutn2;
end
end
always @(posedge clkoutn2 or negedge reset) begin 
if (~reset) begin
	clkout4 <= 0;
end
else begin
	clkout4 <= clkoutn4;
end
end
always @(posedge clkoutn4 or negedge reset) begin 
if (~reset) begin
	clkout8 <= 0;
end
else begin
	clkout8 <= clkoutn8;
end
end


// Now I'll have to generate the enable signal for this...
// EN needs to go down when clkout is down and go up when clkout_prelatch is down
// I can latch programming and control to see when they change and generate a pulse, which is used to control a state machine?
wire clkout_slow, clkout_1, clkout_2, clkout_4, clkout_8;
wire EN_slow;
reg programming_delayed, reset_delayed, reset_delayed_1, reset_delayed_2, change_delayed, change_delayed_1, change_delayed_2;
reg [1:0] control_delayed;
reg change;
always @(posedge clk_slow) begin
	programming_delayed <= programming;
	control_delayed <= control;
	change_delayed_1 <= change;
	change_delayed_2 <= change_delayed_1;
	reset_delayed_1 <= reset_delayed_2;
	reset_delayed <= reset_delayed_1;
	reset_delayed_2 <= reset;
end
always @(*) begin
	if (programming_delayed != programming || control_delayed != control || reset_delayed != reset)
		change = 1;
	else
		change = 0;
end

//assign EN_slow = change_delayed || change;
always @(*) begin
	change_delayed = change_delayed_1 || change_delayed_2;
end
OR2D1 or_EN (.A1(change_delayed), .A2(change), .Z(EN_slow));

reg EN_fast_1, EN_fast;
always @(posedge clk) begin
	EN_fast_1 <= EN_slow;
	EN_fast <= EN_fast_1;
end

CKLNQD1 clklatch_slow (.TE (~EN_slow), .E (~EN_slow), .CP (clk_slow), .Q (clkout_slow));
CKLNQD1 clklatch_1 (.TE (~EN_fast), .E (~EN_fast), .CP (clk), .Q (clkout_1));
CKLNQD1 clklatch_2 (.TE (~EN_fast), .E (~EN_fast), .CP (clkout2), .Q (clkout_2));
CKLNQD1 clklatch_4 (.TE (~EN_fast), .E (~EN_fast), .CP (clkout4), .Q (clkout_4));
CKLNQD1 clklatch_8 (.TE (~EN_fast), .E (~EN_fast), .CP (clkout8), .Q (clkout_8));



always @(*) begin
	if (programming_delayed || ~reset_delayed) begin
		clkout = clkout_slow; //THIS NEEDS TO BE GLITCH FREE, NOT LIKE IT IS NOW
	end else begin
	case(control_delayed)
		0 : clkout = clkout_1;
		1 : clkout = clkout_2;
		2 : clkout = clkout_4;
		3 : clkout = clkout_8;
		default : clkout = clkout_1;
	endcase
	end//
end
endmodule

//Change to async_clear
module lfsr_prbs(
    input clk,
    input reset,
    input ce,	//This time I need this
    output reg [14:0] lfsr,
    output reg lfsr_done);

//reg [14:0] lfsr;
wire d0,lfsr_equal;

xnor(d0,lfsr[14],lfsr[13]);
assign lfsr_equal = (lfsr == 15'h4000);

always @(posedge clk) begin
    if(reset) begin
        lfsr <= 0;
        lfsr_done <= 0;
    end
    else begin
        if(ce)
          lfsr <= lfsr_equal ? 15'h0 : {lfsr[13:0],d0};
        lfsr_done <= lfsr_equal;
    end
end
endmodule

// Introduce a sync clear
module lfsr_counter_3602(
    input clk,
    input reset,
    input ce,	//This time I need this
    output reg lfsr_done);

reg [11:0] lfsr;
wire d0,lfsr_equal;
//reg lfsr_done;

xnor(d0,lfsr[11],lfsr[5],lfsr[3],lfsr[0]);
assign lfsr_equal = (lfsr == 12'h38);

always @(posedge clk) begin
    if(reset) begin
        lfsr <= 0;
        lfsr_done <= 0;
    end
    else begin
        if(ce)
	        lfsr <= lfsr_equal ? 12'h0 : {lfsr[10:0],d0};
        lfsr_done <= lfsr_equal;
    end
end
endmodule

module mux_8(
	input [7:0] datain,
	input [7:0] control,
	output reg dataout);
	
//reg dataout;

always @(*) begin
	case (control)
	'b00000001: begin
		dataout = datain[0];
	     end
	'b00000010: begin
		dataout = datain[1];
	     end
	'b00000100: begin
		dataout = datain[2];
	     end
	'b00001000: begin
		dataout = datain[3];
	     end	
	'b00010000: begin
		dataout = datain[4];
	     end
	'b00100000: begin
		dataout = datain[5];
	     end
	'b01000000: begin
		dataout = datain[6];
	     end
	'b10000000: begin
		dataout = datain[7];
	     end
	default: begin
		dataout = 0; //Is this necessary?
	     end
	endcase
end
endmodule


module lfsr_counter_20(
    input clk,
    input reset,
    output reg [4:0] lfsr
    );

reg lfsr_done;
//reg [4:0] lfsr;
wire d0,lfsr_equal;

xnor(d0,lfsr[4],lfsr[2]);
assign lfsr_equal = (lfsr == 5'h1D);

always @(posedge clk) begin
    if(~reset) begin
        lfsr <= 0;
        lfsr_done <= 0;
    end
    else begin
        lfsr <= lfsr_equal ? 5'h0 : {lfsr[3:0],d0};
        lfsr_done <= lfsr_equal;
    end
end
endmodule


module serializer ( 	//The 8/10b encoder can be placed before this.
			//Input is 40bits (for 32bits of data)
	input clk,
	input reset,
	input [39:0] datain,
	output wire dataout);
	
reg [39:0] buffer_reg_positive, buffer_reg_negative;
wire [4:0] cnt;
//wire dataout;

lfsr_counter_20 lfsr_counter_20(
	.clk(clk),
	.reset(reset),
	.lfsr(cnt)
	);

always @(posedge clk) begin
	case (cnt) 
	5'd0: begin
		buffer_reg_positive <= datain;
	      end
	default: begin
		buffer_reg_positive <= {buffer_reg_positive[37:0], 2'b0};
	     end
	endcase
end

always @(negedge clk) begin
	case (cnt) 
	5'd1: begin
		buffer_reg_negative[39:0] <= {buffer_reg_positive[38:0], 1'b0};
	      end
	default: begin
		buffer_reg_negative <= {buffer_reg_negative[37:0], 2'b0};
	     end
	endcase
end

assign dataout = clk ? buffer_reg_negative[39] : buffer_reg_positive[39];

endmodule

module encoder (
	input clk,
	input reset,
	input enable, //This needs to stay up for a few more cycles after the readout!
	input [7:0] datain,
	input [1:0] parallel_bits,
  input [1:0] clk_divider_config,
	input carrier_extend,
	input PRBS_start,
	output wire pause,	//IMPORTANT!!
	output reg pause_delayed,
	output reg [39:0] dataout,
	output wire sync_8bits,
	output wire sync_16bits
	);

reg [2:0] state, state_next;
//reg pause_delayed;

reg [4:0] byte_out;
reg [7:0] buffer_reg;
//reg [7:0] buffer_encoder;
reg [9:0] encoded_1, encoded_2, encoded_3, encoded_4;
wire [9:0] encoder_out;
//wire pause;
//reg [39:0] dataout;

//reg enable_encoding;

reg [37:0] delay_line;

assign sync_8bits = state ? 'b0 : 'b1;
//wire sync_16bits;
assign sync_16bits = byte_out[0] || byte_out[2] || byte_out[3];

//delay pause signal! 2-7-17-37 cycles
always @(posedge clk) begin
	delay_line <= {delay_line[36:0], pause};
end	

reg [1:0] delay_line_enable;
always @(posedge clk) begin
	delay_line_enable <= {delay_line_enable[0], enable};
end	
wire enable_delayed;
assign enable_delayed = delay_line_enable [1];

always @(*) begin
	case (parallel_bits)
	0: pause_delayed = delay_line[36];	//3 clock cycle earlier = 37/17/7/3 delay
	1: pause_delayed = delay_line[16];
	2: pause_delayed = delay_line[6];
	3: pause_delayed = delay_line[2];
	endcase
end
reg pause_delayed_2;
always @(*) begin
	case (parallel_bits)
	0: pause_delayed_2 = delay_line[37];	//3 clock cycle earlier = 37/17/7/3 delay
	1: pause_delayed_2 = delay_line[17];
	2: pause_delayed_2 = delay_line[7];
	3: pause_delayed_2 = delay_line[3]; //
	endcase
end


always @(posedge clk) begin
	if (~reset) begin
		state <= 0;
	end else begin
		state <= state_next;
	end
end

always @(*) begin
	case (state)
	0: begin
		case (parallel_bits)
		2'd0: begin
			state_next = 1;
		   end
		2'd1: begin
			state_next = 2;
		   end
		2'd2: begin
			state_next = 4;
		   end
		2'd3: begin
			state_next = 0;
		   end
		endcase
//		enable_encoding = ~pause;
	   end
	1: begin
		state_next = 2;
//		enable_encoding = 0;
	   end
	2: begin
		case (parallel_bits)
		2'd0: begin
			state_next = 3;
		   end
		2'd1: begin
			state_next = 4;
		   end
		2'd2: begin
			state_next = 0;
		   end
		2'd3: begin
			state_next = 0;	
		   end	   
		endcase
//		enable_encoding = 0;
	   end
	3: begin
		state_next = 4;
//		enable_encoding = 0;
	   end
	4: begin
		case (parallel_bits)
		2'd0: begin
			state_next = 5;
		   end
		2'd1: begin
			state_next = 6;
		   end
		2'd2: begin
			state_next = 0;
		   end
		2'd3: begin
			state_next = 0;	
		   end	   
		endcase
//		enable_encoding = 0;
	   end
	5: begin
		state_next = 6;
//		enable_encoding = 0;
	   end
	6: begin
		case (parallel_bits)
		2'd0: begin
			state_next = 7;
		   end
		2'd1: begin
			state_next = 0;
		   end
		2'd2: begin
			state_next = 0;
		   end
		2'd3: begin
			state_next = 0;	
		   end	   
		endcase
//		enable_encoding = 0;
	   end
	7: begin
		state_next = 0;
//		enable_encoding = 0;
	   end
	endcase
end

always @(posedge clk) begin
	case (state)
	0: begin
	if (parallel_bits == 3) begin
		case (byte_out)
		'b00100: begin
//			encoded_1 <= {encoder_out[0], encoder_out[1], encoder_out[2], encoder_out[3], encoder_out[4], encoder_out[5], encoder_out[6], encoder_out[7], encoder_out[8], encoder_out[9]};
			encoded_1 <= encoder_out;
		   end
		'b01000: begin
			encoded_2 <= encoder_out;
		   end
		'b10000: begin
			encoded_3 <= encoder_out;
		   end
		'b00001: begin
			encoded_4 <= encoder_out;
		   end
		endcase
	end else begin
		case (byte_out)
		'b00001: begin
		        encoded_1 <= encoder_out;
		   end
		'b00010: begin
		        encoded_2 <= encoder_out;
		   end
		'b00100: begin
		        encoded_3 <= encoder_out;
		   end
		'b01000: begin
			encoded_4 <= encoder_out;
		   end
		endcase
	end
		if (~pause_delayed_2) begin
			buffer_reg[7:0] <= {datain[7], datain[6], datain[5], datain[4], datain[3], datain[2], datain[1], datain[0]};
		end
	      end
	1: begin
		if (~pause_delayed_2) begin
			buffer_reg[1] <= {datain[0]};
		end
	   end
	2: begin
		if (~pause_delayed_2) begin
			buffer_reg[3:2] <= {datain[1], datain[0]};
		end
	   end
	3: begin
		if (~pause_delayed_2) begin
			buffer_reg[3] <= {datain[0]};
		end
	   end
	4: begin
		if (~pause_delayed_2) begin
			buffer_reg[7:4] <= {datain[3], datain[2], datain[1], datain[0]};
		end
	   end
	5: begin
		if (~pause_delayed_2) begin
			buffer_reg[5] <= {datain[0]};
		end
	   end
	6: begin
		if (~pause_delayed_2) begin
			buffer_reg[7:6] <= {datain[1], datain[0]};
		end
	   end
	7: begin
		if (~pause_delayed_2) begin
			buffer_reg[7] <= {datain[0]};
		end
	   end
	endcase
end

always @(posedge clk) begin //Counter modulo 5
	if (~reset) begin
		byte_out <= 5'b10000;
	end else begin
//		if ((state==0) && enable) begin
		if (state==0) begin
			byte_out <= {byte_out[3:0], byte_out[4]};
		end
	end
end

assign pause = byte_out[4];

always @(posedge clk) begin
	if (pause) begin
		if (parallel_bits == 3) begin
			dataout <= {encoded_3, encoded_4, encoded_1, encoded_2}; // I MIGHT NEED TO INVERT THE WORDS (LITTLE ENDIAN)!!!
		end else begin
			dataout <= {encoded_1, encoded_2, encoded_3, encoded_4}; // I MIGHT NEED TO INVERT THE WORDS (LITTLE ENDIAN)!!!
		end
	end
end

reg ki;
//assign ki = ~enable_delayed;
wire disparity;
reg [7:0] control_pattern;
// I need to delay carrier_extend, I'm 100% sure

reg [15:0] carrier_extend_delayline;
reg carrier_extend_delayed;
always @(posedge clk) begin
	carrier_extend_delayline <= {carrier_extend_delayline[14:0], carrier_extend};
end
always @(*) begin
if (parallel_bits == 3) begin
	carrier_extend_delayed = carrier_extend_delayline[1];	
end else begin
	carrier_extend_delayed = carrier_extend_delayline[2];
end
end

/*
state machine for PRBS
takes a start_prbs signal from the command interpreter which gets disabled with a command
starts and stops when aligned with byte_out (this only works when parallel_bits = 3)
PRBS signal switches to PRBS output. When it's down, it resets the LFSR
*/

//input PRBS_start
reg PRBS_enable, enable_count_prbs;
wire [14:0] lfsr;
always @(posedge clk) begin
	if (byte_out == 5'b10000) begin
		PRBS_enable <= PRBS_start;
	end
end

lfsr_prbs lfsr_prbs(
    .clk(clk),
    .reset(~PRBS_enable),
    .ce(enable_count_prbs),	//This time I need this
    .lfsr(lfsr),
    .lfsr_done()
    );
    

always @(*) begin
	if (carrier_extend_delayed) begin
		control_pattern = 8'b111_10111;
		ki = 'b1;
	end else begin	//put here the check for PRBS
	if (parallel_bits == 3 && clk_divider_config == 2) begin
		if (PRBS_enable) begin
		case (byte_out)
		5'b00001: begin
			enable_count_prbs = 1;
			control_pattern = lfsr[7:0];
			ki = 0;
		  end
		5'b00010: begin
			ki = 0;
			enable_count_prbs = 1;
			control_pattern = lfsr[7:0];
		  end
		5'b00100: begin
			enable_count_prbs = 1;
			control_pattern = lfsr[7:0];
			ki = 0;
		  end
		5'b01000: begin
			ki = 0;
			enable_count_prbs = 1;
			control_pattern = lfsr[7:0];
		  end
		default: begin
			enable_count_prbs = 0;
			control_pattern = lfsr[7:0];
			ki = 0;
		  end
		endcase
		end else begin
		case (byte_out)
		5'b00001: begin
			control_pattern = 8'b101_11100;
			enable_count_prbs = 0;
			ki = ~enable_delayed;
		  end
		5'b00010: begin
			ki = 0;
			enable_count_prbs = 0;
			if (~disparity) 
			control_pattern = 8'b110_00101;
			else
			control_pattern = 8'b010_10000;
		  end
		5'b00100: begin
			control_pattern = 8'b101_11100;
			ki = ~enable_delayed;
			enable_count_prbs = 0;
		  end
		5'b01000: begin
			ki = 0;
			enable_count_prbs = 0;
			if (~disparity) 
			control_pattern = 8'b110_00101;
			else
			control_pattern = 8'b010_10000;
		  end
		default: begin
			control_pattern = 8'b101_11100;
			ki = ~enable_delayed;
			enable_count_prbs = 0;
		  end
		endcase
		end
	end else begin
	case (byte_out)
	5'b10000: begin
		control_pattern = 8'b101_11100;
		enable_count_prbs = 0;
		ki = ~enable_delayed;
	  end
	5'b00001: begin
		ki = 0;
		enable_count_prbs = 0;
		if (~disparity) 
		control_pattern = 8'b110_00101;
		else
		control_pattern = 8'b010_10000;
	  end
	5'b00010: begin
		control_pattern = 8'b101_11100;
		enable_count_prbs = 0;
		ki = ~enable_delayed;
	  end
	5'b00100: begin
		ki = 0;
		enable_count_prbs = 0;
		if (~disparity) 
		control_pattern = 8'b110_00101;
		else
		control_pattern = 8'b010_10000;
	  end
	default: begin
		control_pattern = 8'b101_11100;
		enable_count_prbs = 0;
		ki = ~enable_delayed;
	  end
	endcase
	end
	end
end

wire send_control;
assign send_control = ~enable_delayed || carrier_extend_delayed;

wire [7:0] buffer_comma;
assign buffer_comma = send_control ? control_pattern : buffer_reg; //8'b101_11100
//wire enable_encoding_2;
//assign enable_encoding_2 = pause ? 0 : enable_encoding;


reg [39:0] delay_line_encoding;
reg enable_encoding_delayed;
wire enable_encoding_input;


assign enable_encoding_input = ~pause_delayed_2 && (state == 0);
/*
always @(negedge clk) begin
	delay_line_encoding <= {delay_line_encoding[38:0], enable_encoding_input};
end
always @(*) begin
	enable_encoding = delay_line_encoding[0];
	enable_encoding_delayed = delay_line_encoding[39];
end

encoder_8b_10b_VG encoder_8b_10b (
	.SBYTECLK (clk),
	.RESET (~reset),
	.KI (ki),
	.AI (buffer_comma[0]),
	.BI (buffer_comma[1]),
	.CI (buffer_comma[2]),
	.DI (buffer_comma[3]),
	.EI (buffer_comma[4]),
	.FI (buffer_comma[5]),
	.GI (buffer_comma[6]),
	.HI (buffer_comma[7]),
	.JO (encoder_out[9]),
	.HO (encoder_out[8]),
	.GO (encoder_out[7]),
	.FO (encoder_out[6]),
	.IO (encoder_out[5]),
	.EO (encoder_out[4]),
	.DO (encoder_out[3]),
	.CO (encoder_out[2]),
	.BO (encoder_out[1]),
	.AO (encoder_out[0]),
	.ENABLE (enable_encoding),
	.ENABLE_delayed (enable_encoding_delayed)
	);
*/
reg enable_encoding;
always @(posedge clk) begin
	delay_line_encoding <= {delay_line_encoding[38:0], enable_encoding_input};
end
always @(*) begin
if (parallel_bits == 3) begin
	enable_encoding_delayed = delay_line_encoding[1];
	enable_encoding = delay_line_encoding[0];
end else begin
	enable_encoding_delayed = delay_line_encoding[0];
	enable_encoding = enable_encoding_input;
end
end

encoder_8b10b_opencore encoder_8b10b (
 
   .reset(~reset),
   .SBYTECLK(clk),	//I can gate this with an enable which is delayed by 1 clock cycle for the output latches
   .enable(enable_encoding),
   .enable_delayed(enable_encoding_delayed),
   .K(ki),
   .ebi(buffer_comma),
   .tbi(encoder_out),
   .disparity(disparity)
   );

endmodule


module encoder_8b10b_opencore (
 
   // --- Resets
   input reset,
 
   // --- Clocks
   input SBYTECLK,	//I can gate this with an enable which is delayed by 1 clock cycle for the output latches
 
   // --- Control (K) input	  
   input K,
 
   // --- Eight Bt input bus	  
   input [7:0] ebi,
   
   input enable,
   input enable_delayed,
   
 
   // --- TB (Ten Bt Interface) output bus
   output [9:0] tbi,
 
   output reg disparity
   );
 
 
   // Figure 3 - Encoder: 5B/6B classification, L functions
   wire   L40, L04, L13, L31, L22, AeqB, CeqD;
 
   // Figure 5 - 5B/6B Encoder: disparity classifications
   wire   PD_1S6, NDOS6, PDOS6, ND_1S6;
 
   // Figure 5 - 3B/4B Encoder: disparity classifications
   wire   ND_1S4, PD_1S4, NDOS4, PDOS4;
 
   // Figure 6 - Encoder: control of complementation
   wire   illegalk, DISPARITY6;
   reg    COMPLS6, COMPLS4;
 
   // Figure 7 - 5B/6B encoding 
   wire   NAO, NBO, NCO, NDO, NEO, NIO;
 
   // Figure 8: 3B/4B encoding
   wire   NFO, NGO, NHO, NJO;
 
   // 8B Inputs
   wire   A,B,C,D,E,F,G,H;      
 
   assign {H,G,F,E,D,C,B,A} = ebi[7:0];
 
   // 10B Outputs
   reg 	  a,b,c,d,e,i,f,g,h,j; 
 
   assign tbi[9:0] = {a,b,c,d,e,i,f,g,h,j};
 
   wire [9:0] tst;
 
   assign tst[9:0] = {NAO,NBO,NCO,NDO,NEO,NIO,NFO,NGO,NHO,NJO};
 
   // ******************************************************************************
   // Figures 7 & 8 - Latched 5B/6B and 3B/4B encoder outputs 
   // ******************************************************************************
 
   always @(posedge SBYTECLK, posedge reset)
     if (reset) 
       begin
	{a,b,c,d,e,i,f,g,h,j} <= 10'b0; 
//		  disparity <= 1'b0; 
       end 
     else begin
     	if (enable_delayed) begin
 
//	disparity <= (PDOS4 | NDOS4) ^ DISPARITY6;
 
	{a,b,c,d,e,i,f,g,h,j} <= { NAO^COMPLS6, NBO^COMPLS6, NCO^COMPLS6, 
				   NDO^COMPLS6, NEO^COMPLS6, NIO^COMPLS6,
				   NFO^COMPLS4, NGO^COMPLS4, 
				   NHO^COMPLS4, NJO^COMPLS4 };	
	end
     end // else: !if(reset)
 
   // ******************************************************************************
   // Figure 3 - Encoder: 5B/6B classification, L functions
   // ******************************************************************************
 
   assign AeqB = (A & B) | (!A & !B);
   assign CeqD = (C & D) | (!C & !D);
 
   assign L40 =  A & B & C & D ;
   assign L04 = !A & !B & !C & !D;
 
   assign L13 = (!AeqB & !C & !D) | (!CeqD & !A & !B);
   assign L31 = (!AeqB &  C &  D) | (!CeqD &  A &  B);
   assign L22 = (A & B & !C & !D) | (C & D & !A & !B) | ( !AeqB & !CeqD) ;
 
   // ******************************************************************************
   // Figure 5 - 5B/6B Encoder: disparity classifications
   // ******************************************************************************
 
   assign PD_1S6 = (E & D & !C & !B & !A) | (!E & !L22 & !L31) ;
 
   //assign PD_1S6 = (L13 & D & E) | (!E & !L22 & !L31) ;
 
   assign NDOS6  = PD_1S6 ;
   assign PDOS6  = K | (E & !L22 & !L13) ;
   assign ND_1S6 = K | (E & !L22 & !L13) | (!E & !D & C & B & A) ;
 
   // ******************************************************************************
   // Figure 5 - 3B/4B Encoder: disparity classifications
   // ******************************************************************************
 
   assign ND_1S4 = F & G ;
   assign NDOS4  = (!F & !G) ;
   assign PD_1S4 = (!F & !G) | (K & ((F & !G) | (!F & G)));
   assign PDOS4  = F & G & H ;
 
   // ******************************************************************************
   // Figure 6 - Encoder: control of complementation
   // ******************************************************************************
 
   // not K28.0->7 & K23/27/29/30.7
   assign illegalk = K & (A | B | !C | !D | !E) & (!F | !G | !H | !E | !L31); 
 
   assign DISPARITY6 = disparity ^ (NDOS6 | PDOS6) ;
 
   always @(posedge SBYTECLK, posedge reset)
     if(reset) begin
       disparity <= 1'b0; //
       COMPLS4 <= 0;
       COMPLS6 <= 0;
       end
     else begin
	if (enable) begin
	disparity <= (PDOS4 | NDOS4) ^ DISPARITY6; //
       COMPLS4 <= (PD_1S4 & !DISPARITY6) | (ND_1S4 & DISPARITY6);
       COMPLS6 <= (PD_1S6 & !disparity) | (ND_1S6 & disparity);
        end
       end
 
   // ******************************************************************************
   // Figure 7 - 5B/6B encoding 
   // ******************************************************************************
 
   reg tNAO, tNBOx, tNBOy, tNCOx, tNCOy, tNDO , tNEOx, tNEOy, tNIOw, tNIOx, tNIOy, tNIOz;
 
   always @(posedge SBYTECLK, posedge reset)
     if(reset) begin
       tNAO  <= 0;
       tNBOx <= 0;
       tNBOy <= 0;
       tNCOx <= 0;
       tNCOy <= 0;
       tNDO  <= 0;
       tNEOx <= 0;
       tNEOy <= 0;
       tNIOw <= 0;
       tNIOx <= 0;
       tNIOy <= 0;
       tNIOz <= 0;
       end
     else begin
       	if (enable) begin
       tNAO  <= A ;
 
       tNBOx <= B & !L40;
       tNBOy <= L04 ;
 
       tNCOx <= L04 | C ;
       tNCOy <= E & D & !C & !B & !A ;
 
       tNDO  <= D & ! (A & B & C) ;
 
       tNEOx <= E | L13;
       tNEOy <= !(E & D & !C & !B & !A) ;
 
       tNIOw <= (L22 & !E) | (E & L40) ;
       tNIOx <= E & !D & !C & !(A & B) ;
       tNIOy <= K & E & D & C & !B & !A ;
       tNIOz <= E & !D & C & !B & !A ;
	end
       end
 
   assign NAO = tNAO ;
   assign NBO = tNBOx | tNBOy ;
   assign NCO = tNCOx | tNCOy ;
   assign NDO = tNDO ;
   assign NEO = tNEOx & tNEOy ;
   assign NIO = tNIOw | tNIOx | tNIOy | tNIOz;
 
   // ******************************************************************************
   // Figure 8: 3B/4B encoding
   // ******************************************************************************
 
   reg alt7, tNFO, tNGO, tNHO, tNJO;
 
   always @(posedge SBYTECLK, posedge reset)
     if(reset) begin
       alt7 <= 0;
       tNFO <= 0;
       tNGO <= 0;
       tNHO <= 0;
       tNJO <= 0;
       end
     else begin
     	if (enable) begin
       alt7 <= F & G & H & (K | (disparity ? (!E & D & L31) : (E & !D & L13))) ;
       tNFO <= F;
       tNGO <= G | (!F & !G & !H) ;
       tNHO <= H ;
       tNJO <= !H & (G ^ F) ;
	end
       end
 
   assign NFO = tNFO & !alt7 ;
   assign NGO = tNGO ;
   assign NHO = tNHO ;
   assign NJO = tNJO | alt7 ;
 
endmodule
