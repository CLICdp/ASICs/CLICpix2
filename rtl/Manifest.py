files = [
    "techlib/tcbn65lp.v",
    "techlib/tcbn65lpbwp7thvt.v"
]

if 'netlist' in locals():
    #Netlist
    files.extend( [ "pixel_cluster/pixel_matrix_layout.v",
                    "pixel_cluster/pixel_cluster_layout.v",
                    "periphery_layout.v",
                    "end_of_column_layout.v" ] )

else:
    #RTL design
    files.extend( [
        "control_logic.v",
        "DAC_control.v",
        "modules.v",
        "powerpulsing.v",
        "pulse_generator.v",
        "readout_control.v",
        "readout_start.v",

        "pixel_cluster/pixel_matrix.v",
        "pixel_cluster/pixel_cluster.v",
        "pixel_cluster/clock_divider_sync.v",
        "pixel_cluster/pr_counters.v",
        "pixel_cluster/pixel_logic.v",
        "periphery.v",
        "end_of_column.v" ] )

    
vlog_opt = "-sv"



