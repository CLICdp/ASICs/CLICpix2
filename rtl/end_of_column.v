module set_ff (
	input in,
	output reg out,
	input set,
	input clk
	);

//reg out;
always @(posedge clk or posedge set) begin
if (set) begin
	out <= 1;
end else begin
	out <= in;
end
end
endmodule

module end_of_column (
	input clkin_divided, 	//I might need to add a clock_undelayed to be sent to the matrix to equalize delay
	input clkin_slow, 
	input select_clock, 	//only for acquisition. Slow clock (delayed) is handled by the clock divider for programming
	input shift_data,	//both for programming and readout. Where should this come from?
	input datain_column,
	input fastor,
	input token,	//No need for pause signals, only advance while token is 1
	input use_compression,
	input use_cluster_compression,
	input reset,
//	input pause,	//This shouldn't be needed, as we already have the token signal
//	input configure, //do I need this? Probably to tell it to shift no matter what.
	output wire dataout,
	output reg column_done,
	output wire clkout,
	
	output wire clock_div0_multi,
	output wire clock_div1_multi,
	output wire use_compression_multi,
	output wire use_cluster_compression_multi,
	output wire polarity_multi,
	output wire configure_matrix_multi,
	output wire shift_multi,
	output wire TPswitch_multi,
	output wire shutter_multi,
	input clock_div0,
	input clock_div1,
//	input use_compression,
//	input use_cluster_compression,
	input polarity,
	input configure_matrix,
//	input shift,
	input TPswitch,
	input shutter
	
	);

//reg column_done;
//wire clkout;
//wire dataout;

//wire clock_div0_multi, clock_div1_multi, use_compression_multi, use_cluster_compression_multi, polarity_multi, configure_matrix_multi, shift_multi, TPswitch_multi, shutter_multi;

CKBD24 cdiv0_multibuffer (.I (clock_div0), .Z (clock_div0_multi));
CKBD24 cdiv1_multibuffer (.I (clock_div1), .Z (clock_div1_multi));
CKBD24 compr0_multibuffer (.I (use_compression), .Z (use_compression_multi));
CKBD24 compr1_multibuffer (.I (use_cluster_compression), .Z (use_cluster_compression_multi));
CKBD24 pol_multibuffer (.I (polarity), .Z (polarity_multi));
CKBD24 config_multibuffer (.I (configure_matrix), .Z (configure_matrix_multi));
CKBD24 shift_multibuffer (.I (shift_data), .Z (shift_multi));
CKBD24 TP_multibuffer (.I (TPswitch), .Z (TPswitch_multi));
CKBD24 shutter_multibuffer (.I (shutter), .Z (shutter_multi));

/* CLOCK DELAY EQUALIZING, I PROBABLY NEED IT OUTSIDE OF THIS MODULE
wire clk_temp, clk_temp2, clk_temp3, clk_temp4, clk_temp5, clk_temp6, clk_temp7, clk_temp8, clk_temp9, clkout_delayed;

CKBD2 clkbuffer (.I (clkout), .Z (clk_temp));
CKBD2 clkbuffer2 (.I (clk_temp), .Z (clk_temp2));
CKBD2 clkbuffer3 (.I (clk_temp2), .Z (clk_temp3));
CKBD2 clkbuffer4 (.I (clk_temp3), .Z (clk_temp4));
CKBD2 clkbuffer5 (.I (clk_temp4), .Z (clk_temp5));
CKBD2 clkbuffer6 (.I (clk_temp5), .Z (clk_temp6));
CKBD2 clkbuffer7 (.I (clk_temp6), .Z (clk_temp7));
CKBD2 clkbuffer8 (.I (clk_temp7), .Z (clk_temp8));
CKBD2 clkbuffer9 (.I (clk_temp8), .Z (clk_temp9));
assign clkout_delayed = clk_temp9;
*/

wire clkout_gated;

CKMUX2D1 clkcolmux (.I0 (clkout_gated), .I1 (clkin_slow), .S (select_clock), .Z (clkout));
CKLNQD1 clkcollatch (.TE (token), .E (token), .CP (clkin_divided), .Q (clkout_gated));


//start column flag code
wire set_flag;
//wire fastor_or;
//assign fastor_or = (use_cluster_compression) ? fastor : 'b1;
//assign set_flag = (fastor_or && ~shift_data);
assign set_flag = (fastor && ~shift_data);
set_ff u (datain_column, dataout, set_flag, clkout);	//do I need to reset this? I don't think I need to because it gets reset during readout by shifting zeroes in it.
							// This does need to use the delayed clock
//end column flag code

reg [3:0] bit_counter;
reg [3:0] pixel_counter;
reg [3:0] cluster_counter;

reg count_pixel, count_cluster, count_bit, bit_reset, pixel_reset, cluster_reset;

reg [3:0] next_state, state;

always @(posedge clkin_divided) begin	//this should work as an async reset...
	if (~reset) begin
		state <= 0;
	end else begin
		if (token || ~shift_data) begin	//the state machine will run wildly during programming, but it shouldn't matter
			state <= next_state;
		end
	end
end

always @(*) begin
	case (state)
	0: begin	//column check
	if (~shift_data) begin
		next_state <= 0;
	end else begin
		if (~dataout && (use_cluster_compression)) begin
		next_state <= 8;
		end else begin
			next_state <= 5;
		end
	end
	end
	5: begin	//cluster check
	if (~dataout && (use_cluster_compression && cluster_counter==15)) begin
		next_state <= 8;
	end else if (~dataout && (use_cluster_compression)) begin
		next_state <= 5;
	end else begin
		next_state <= 6;
	end
	end
	6: begin	//pixel check
	if (~dataout && (use_compression && cluster_counter==15 && pixel_counter==15)) begin
		next_state <= 8;
	end else if (~dataout && (use_compression && pixel_counter==15)) begin
		next_state <= 5;
	end else if (~dataout && (use_compression)) begin
		next_state <= 6;
	end else begin
		next_state <= 3;
	end
	end
	3: begin	//bit check
	if (cluster_counter==15 && pixel_counter==15 && bit_counter==12) begin
		next_state <= 8;
	end else if (pixel_counter==15 && bit_counter==12) begin
		next_state <= 5;
	end else if (bit_counter==12) begin
		next_state <= 6;
	end else begin
		next_state <= 3;
	end
	end
	//I added a new state in order to keep column_done up until I end the readout...
	8: begin
	if (~shift_data) begin
		next_state <= 0;
	end else begin
		next_state <= 8;
	end
	end
	default:
	next_state <= 0;
	endcase
end

always @(*) begin
	case (state)
	8: begin	
	bit_reset <= 0;
//	column_reset <= 0;
	pixel_reset <= 0;
	cluster_reset <= 0;
	count_bit <= 0;
	column_done <= 1;
	count_pixel <= 0;
	count_cluster <= 0;
//	shift_data <= 0;
	end
	0: begin	
//	shift_data <= 1; //To be put in an OR with the configuration control signal
	if (~dataout && (use_cluster_compression)) begin
		column_done <= 1;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 0;
		cluster_reset <= 0;
	end else begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 0;
		cluster_reset <= 0;
	end
	end
	5: begin	
//	shift_data <= 1;
	if (~dataout && (use_cluster_compression && cluster_counter==15)) begin
		column_done <= 1;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 0;
		cluster_reset <= 1;
	end else if (~dataout && (use_cluster_compression)) begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 1;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 0;
		cluster_reset <= 1;
	end else begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 0;
		cluster_reset <= 1;
	end
	end
	6: begin	
//	shift_data <= 1;
	if (~dataout && (use_compression && cluster_counter==15 && pixel_counter==15)) begin
		column_done <= 1;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end else if (~dataout && (use_compression && pixel_counter==15)) begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 1;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end else if (~dataout && (use_compression)) begin
		column_done <= 0;
		count_pixel <= 1;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end else begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 0;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end
	end
	3: begin
//	shift_data <= 1;
	if (cluster_counter==15 && pixel_counter==15 && bit_counter==12) begin
		column_done <= 1;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 1;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end else if (pixel_counter==15 && bit_counter==12) begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 1;
		count_bit <= 0;
		bit_reset <= 1;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end else if (bit_counter==12) begin
		column_done <= 0;
		count_pixel <= 1;
		count_cluster <= 0;
		count_bit <= 0;
		bit_reset <= 1;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end else begin
		column_done <= 0;
		count_pixel <= 0;
		count_cluster <= 0;
		count_bit <= 1;
		bit_reset <= 1;
//		column_reset <= 1;
		pixel_reset <= 1;
		cluster_reset <= 1;
	end
	end
	default: begin
	bit_reset <= 1;
//	column_reset <= 1;
	pixel_reset <= 1;
	cluster_reset <= 1;
	count_bit <= 0;
	column_done <= 0;
	count_pixel <= 0;
	count_cluster <= 0;
//	shift_data <= 1;
	end
	endcase
end

always @(posedge clkin_divided or negedge cluster_reset)
begin
	if (~cluster_reset)
		cluster_counter <= 4'b0000;
	else if (count_cluster & token)
		cluster_counter <= cluster_counter + 1'b1;
end

always @(posedge clkin_divided or negedge pixel_reset)
begin
	if (~pixel_reset)
		pixel_counter <= 4'b0000;
	else if (count_pixel & token)
		pixel_counter <= pixel_counter + 1'b1;
end

always @(posedge clkin_divided or negedge bit_reset)
begin
	if (~bit_reset)
		bit_counter <= 4'b0000;
	else if (count_bit & token)
		bit_counter <= bit_counter + 1'b1;
end

/*
wire column_done_OR;
reg column_done_conf;
wire token_reset;
assign token_reset = (~reset || column_done_OR);
assign column_done_OR = column_done || column_done_conf;

always @(posedge clkin_fast or negedge reset)
begin
if (~reset)
	column_done_conf <= 0;
else begin
if (token && configure)
	column_done_conf <= 1;
else
	column_done_conf <= 0;
end
end

//column_done NEEDS to be put together with other column switching logic
always @(posedge clkin_fast or posedge token_reset) begin
	if (token_reset)
		token <= 0;
	else if (~configure) begin
		if (token_prev)
			token <= 1;
	end else begin
		if (token_prev2)
			token <= 1;
	end
end
*/

endmodule
