`timescale 100ps/1ps

module readout_interpreter (
	input clk,
	input data_ready,
	input [4:0] ADDRESS,
	//input [7:0] DATA,
	input reset,
	
	input lfsr_done,	//for the shift_data signal (programming)
	input enable_encoder,	//for the shift_data signal (readout)
	
	output reg send_clk,
	output reg start_PRBS,
	output reg start_readout,
	output reg start_programming,
	output wire shift_data,
	output reg programming	//for the clock divider
	);
	
reg [4:0] state_readout, state_readout_next;
reg [4:0] state_programming, state_programming_next;
//reg start_readout, start_programming, start_PRBS;
reg shift_readout, shift_programming;
//reg programming;
//wire shift_data;

assign shift_data = shift_readout || shift_programming;

/*I need to resync enable_encoder!*/

reg enable_encoder_slow;
wire enable_encoder_slow_1;
always @(posedge clk) begin
	enable_encoder_slow <= enable_encoder_slow_1;
end

DFQD4 enable_encoder_resync (.Q(enable_encoder_slow_1),
	.D(enable_encoder),
	.CP(clk));


	
always @(*) begin //CHECK ADDRESS BEFORE STARTING THE STATE MACHINE!
	case(state_readout)
	0: begin
		if (data_ready == 1 && ADDRESS == 'b00001) begin
			state_readout_next = 1;
		end else begin
			state_readout_next = 0;
		end
	   end
	1: begin
		state_readout_next = 2;
	   end
	2: begin
		if (enable_encoder_slow) begin
			state_readout_next = 3;
		end else begin
			state_readout_next = 2;
		end
	   end
	3: begin
		if (~enable_encoder_slow) begin
			state_readout_next = 4;
		end else begin
			state_readout_next = 3;
		end
	   end
	default: begin
			state_readout_next = state_readout + 1;
	   end
	endcase
end


always @(*) begin
	case (state_readout)
	0: begin
		send_clk = 0;
		shift_readout = 0;
		start_readout = 0;
	   end
	1: begin	
		send_clk = 0;
		shift_readout = 1;
		start_readout = 1;
	   end
	2: begin	
		send_clk = 0;
		shift_readout = 1;
		start_readout = 0;
	   end
	3: begin
		send_clk = 0;
		shift_readout = 1;
		start_readout = 0;
	   end
	28: begin
		send_clk = 0;
		shift_readout = 1;
		start_readout = 0;
	   end
	29: begin
		send_clk = 0;
		shift_readout = 1;
		start_readout = 0;
	   end
	30: begin
		send_clk = 0;
		shift_readout = 1;
		start_readout = 0;
	   end
	31: begin
		send_clk = 0;
		shift_readout = 1;
		start_readout = 0;
	   end
	default: begin
		send_clk = 1;
		shift_readout = 1;
		start_readout = 0;
	   end
	endcase
end

always @(posedge clk) begin
	if (~reset)
		state_readout <=#0.1 0;
	else
	        state_readout <=#0.1 state_readout_next;
end



always @(*) begin //CHECK ADDRESS BEFORE STARTING THE STATE MACHINE!
	case(state_programming)
	0: begin
		if (data_ready == 1 && ADDRESS == 'b00010) begin
			state_programming_next = 1;
		end else begin
			state_programming_next = 0;
		end
	   end
	18: begin
		if (data_ready == 1 && ADDRESS == 'b00010) begin
			state_programming_next = 1;
		end else begin
      state_programming_next = 0;
    end
    end
	default: begin
		if (data_ready == 1 && ADDRESS == 'b00010) begin
			state_programming_next = 1;
		end else begin
			state_programming_next = state_programming + 1;
		end
    end
	endcase
end

//I need to make shift_programming sequential, not combinatorial.
//- state 3/4 -> start
//- lfsr_done -> stop

//negedge lfsr_done
reg lfsr_delayed, lfsr_negedge;
always @(posedge clk) begin
	lfsr_delayed <= lfsr_done;
	if (lfsr_delayed && ~ lfsr_done)
		lfsr_negedge <= 1;
	else
		lfsr_negedge <= 0;
end

always @(posedge clk) begin
	if (state_programming == 4)
		shift_programming <= 1;
	else if (lfsr_negedge || ~reset)
		shift_programming <= 0;
end

always @(*) begin
	case (state_programming)
	0: begin
		start_programming = 0;
		programming = 0;
	   end
	1: begin	
		start_programming = 0;
		programming = 1;
	   end
	2: begin
		start_programming = 0;
		programming = 1;
	   end
	3: begin	
		start_programming = 0;
		programming = 1;
	   end
	4: begin	
		start_programming = 1;
		programming = 1;
	   end
	default: begin
		start_programming = 0;
		programming = 1;
	   end
	endcase
end

always @(posedge clk) begin
	if (~reset)
		state_programming <=#0.1 0;
	else
	        state_programming <=#0.1 state_programming_next;
end

always @(posedge clk) begin
	if (~reset)
		start_PRBS <= 0;
	else if (data_ready == 1 && ADDRESS == 'b00100)
		start_PRBS <= ~start_PRBS;
end

endmodule
