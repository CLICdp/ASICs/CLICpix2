//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/digital/periphery/syn/verilog/modules.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/digital/periphery/syn/verilog/readout_start.v"

module readout_control (
	input clk, //This is the output of the clock divider. It switches to the slow
		   //clock automatically in that block. When it switches, it COULD
		   //generate glitches. The logic MUST ignore pulses for a short while.
	input reset,
	input [1:0] parallel_columns,
	input programming_data,	//serial programming data (starting from column 0 lowest bit)
	input [7:0] programming_word,
	input [63:0] datain,
	input [63:0] column_done,
	input start_programming_input,	//I need this in order to be synchronized with the slower
				//clock. It will work like this:
				// - Write the programming command
				// - The periphery block will command the clock divider
				//	to switch clocks
				// - After a couple of clock cycles (to allow the clock
				//	divider to switch) I will set this in order to
				//	start programmming
				//This way I start the programming a known time after the
				//command is sent
//	output programming_clock_enable,	//Control for the clock divider, but not
						//in this block!
	input start_readout_input,
	input pause,	//From the encoder!! Also check that I can start the state machine while pause==1
	input sync_8bits,
	input sync_16bits,
	output reg configure_matrix,	//At the end of the programming, to latch data
	output reg enable_encoder,		//Enable signal for the 8b10b encoder. This could be used as (a part of) the shift signal for eoc blocks
	output wire [63:0] column_token,	//Enable for the EOC block
	output reg [63:0] column_dataout,	//Programming data to columns
	output reg [7:0] dataout,
	output wire lfsr_done,	//to stop the shift and programming signals in the command block while programming
	output reg carrier_extend	//to tell the encoder to put a carrier_extend word between columns
	);

wire [7:0] buffer_reg;
//reg carrier_extend;
reg [7:0] header_reg;
reg [7:0] buffer_reg_2;
reg header;	//control if I send a header or not;
reg start_header;	//control the start of the header phase
//reg enable_encoder;	//this is actually one clock cycle early. I could delay it by one if needed (it probably is).
reg move_token;
reg enable_readout; 
reg reset_counters;

//reg [63:0] column_dataout;

reg [7:0] mux_control_0;	//Control registers for serializer bits
reg [7:0] mux_control_1;
reg [7:0] mux_control_2;
reg [7:0] mux_control_3;
reg [7:0] mux_control_4;
reg [7:0] mux_control_5;
reg [7:0] mux_control_6;
reg [7:0] mux_control_7;

reg [7:0] token_select;	//Reg storing the position of the token in a 8-columns block

wire [7:0] blocks_used;
wire [7:0] column_select_0;
wire [7:0] column_select_1;
wire [7:0] column_select_2;
wire [7:0] column_select_3;
wire [7:0] column_select_4;
wire [7:0] column_select_5;
wire [7:0] column_select_6;
wire [7:0] column_select_7;

reg all_columns_done;
wire [7:0] blocks_done;
wire end_readout;

//reg [7:0] dataout;

reg [7:0] programming_token;	//Shift register dictating which columns will program. 8
				//columns at a time will be active (hence the 8 bits
				//instead of 64) Should stay at 0 when the chip is not
				//programming (in order to be used during readout)

assign blocks_used = mux_control_0 | mux_control_1 | mux_control_2 | mux_control_3 | mux_control_4 | mux_control_5 | mux_control_6 | mux_control_7;
assign column_select_0 = blocks_used[0] ? token_select : 8'h00;
assign column_select_1 = blocks_used[1] ? token_select : 8'h00;
assign column_select_2 = blocks_used[2] ? token_select : 8'h00;
assign column_select_3 = blocks_used[3] ? token_select : 8'h00;
assign column_select_4 = blocks_used[4] ? token_select : 8'h00;
assign column_select_5 = blocks_used[5] ? token_select : 8'h00;
assign column_select_6 = blocks_used[6] ? token_select : 8'h00;
assign column_select_7 = blocks_used[7] ? token_select : 8'h00;


// I need to resync start_programming and start_readout;
reg start_programming, start_readout;
reg start_programming_buffer, start_readout_buffer;
reg start_programming_buffer_2, start_readout_buffer_2;
wire reset_start_programming, reset_start_readout;

assign reset_start_programming = ~start_programming;
assign reset_start_readout = ~start_readout;
always @(posedge start_programming_input or negedge reset_start_programming or negedge reset) begin
	if (~reset_start_programming || ~reset) begin
		start_programming_buffer = 0;
	end else begin
		start_programming_buffer = 1;
	end
end
always @(posedge start_readout_input or negedge reset_start_readout or negedge reset) begin
	if (~reset_start_readout || ~reset) begin
		start_readout_buffer = 0;
	end else begin
		start_readout_buffer = 1;
	end
end
always @(posedge clk) begin
	start_programming_buffer_2 <= start_programming_buffer;
	start_readout_buffer_2 <= start_readout_buffer;
	start_programming <= start_programming_buffer_2;
	start_readout <= start_readout_buffer_2;
end


always @(*) begin	//Calculating when all columns are done reading
	if (blocks_done == blocks_used) begin
		all_columns_done = 1'b1;
	end else begin
		all_columns_done = 1'b0;
	end
end

assign end_readout = token_select[7] & blocks_done[7] & blocks_used[7];	//End of readout


always @(posedge clk) begin //Shift registers for mux controls
	if (reset_counters || ~reset) begin
		case (parallel_columns)	//starting from a place before so that it increases as soon as readout starts...
		2'd0: begin
			mux_control_0 <= 'b10000000;
			mux_control_1 <= 'b00000000;
			mux_control_2 <= 'b00000000;
			mux_control_3 <= 'b00000000;
			mux_control_4 <= 'b00000000;
			mux_control_5 <= 'b00000000;
			mux_control_6 <= 'b00000000;
			mux_control_7 <= 'b00000000;
		   end
	        2'd1: begin
	        	mux_control_0 <= 'b10000000;
	        	mux_control_1 <= 'b00000000;
	        	mux_control_2 <= 'b00000000;
	        	mux_control_3 <= 'b00000000;
	        	mux_control_4 <= 'b00001000;
	        	mux_control_5 <= 'b00000000;
	        	mux_control_6 <= 'b00000000;
	        	mux_control_7 <= 'b00000000;
	           end
 		2'd2: begin
 			mux_control_0 <= 'b10000000;
 			mux_control_1 <= 'b00000000;
 			mux_control_2 <= 'b00000010;
 			mux_control_3 <= 'b00000000;
 			mux_control_4 <= 'b00001000;
 			mux_control_5 <= 'b00000000;
 			mux_control_6 <= 'b00100000;
 			mux_control_7 <= 'b00000000;
 		   end
  		2'd3: begin
  			mux_control_0 <= 'b10000000;	
  			mux_control_1 <= 'b00000001;
  			mux_control_2 <= 'b00000010;
  			mux_control_3 <= 'b00000100;
  			mux_control_4 <= 'b00001000;
  			mux_control_5 <= 'b00010000;
  			mux_control_6 <= 'b00100000;
  			mux_control_7 <= 'b01000000;
  		   end
		endcase
	end else begin
//		if (all_columns_done & token_select[7]) begin //this will need to change, from all_columns_done to something generated by the state machine
		if (move_token & token_select[7] & ~pause) begin
			mux_control_0 <= {mux_control_0[6:0], mux_control_0[7]};
			mux_control_1 <= {mux_control_1[6:0], mux_control_1[7]};
			mux_control_2 <= {mux_control_2[6:0], mux_control_2[7]};
			mux_control_3 <= {mux_control_3[6:0], mux_control_3[7]};
			mux_control_4 <= {mux_control_4[6:0], mux_control_4[7]};
			mux_control_5 <= {mux_control_5[6:0], mux_control_5[7]};
			mux_control_6 <= {mux_control_6[6:0], mux_control_6[7]};
			mux_control_7 <= {mux_control_7[6:0], mux_control_7[7]};
		end
	end
end


always @(posedge clk) begin //Ring shift register for tokens
	if (reset_counters || ~reset) begin
		token_select <= 'b10000000;
	end else begin
//		if (all_columns_done) begin //this will need to change, from all_columns_done to something generated by the state machine
		if (move_token & ~pause) begin
			token_select <= {token_select[6:0], token_select[7]};
		end
	end
end


//HEADER GENERATION STATE MACHINE


// This should act as pause control too
// Should I also gate the column_done signal??
// This should be updated also to include the programming part...
assign column_token[7:0] = (enable_readout & ~pause) ? column_select_0 : programming_token;
assign column_token[15:8] = (enable_readout & ~pause) ? column_select_1 : programming_token;
assign column_token[23:16] = (enable_readout & ~pause) ? column_select_2 : programming_token;
assign column_token[31:24] = (enable_readout & ~pause) ? column_select_3 : programming_token;
assign column_token[39:32] = (enable_readout & ~pause) ? column_select_4 : programming_token;
assign column_token[47:40] = (enable_readout & ~pause) ? column_select_5 : programming_token;
assign column_token[55:48] = (enable_readout & ~pause) ? column_select_6 : programming_token;
assign column_token[63:56] = (enable_readout & ~pause) ? column_select_7 : programming_token;

mux_8 input_mux_0 (	//Muxes from the columns to the tmp register
	.datain (datain[7:0]),
	.control (column_select_0),
	.dataout (buffer_reg[0])
	);
mux_8 input_mux_1 (
	.datain (datain[15:8]),
	.control (column_select_1),
	.dataout (buffer_reg[1])
	);
mux_8 input_mux_2 (
	.datain (datain[23:16]),
	.control (column_select_2),
	.dataout (buffer_reg[2])
	);
mux_8 input_mux_3 (
	.datain (datain[31:24]),
	.control (column_select_3),
	.dataout (buffer_reg[3])
	);
mux_8 input_mux_4 (
	.datain (datain[39:32]),
	.control (column_select_4),
	.dataout (buffer_reg[4])
	);
mux_8 input_mux_5 (
	.datain (datain[47:40]),
	.control (column_select_5),
	.dataout (buffer_reg[5])
	);
mux_8 input_mux_6 (
	.datain (datain[55:48]),
	.control (column_select_6),
	.dataout (buffer_reg[6])
	);
mux_8 input_mux_7 (
	.datain (datain[63:56]),
	.control (column_select_7),
	.dataout (buffer_reg[7])
	);	


mux_8 done_mux_0 (	//Muxes from the columns_done bits
	.datain (column_done[7:0]),
	.control (column_select_0),
	.dataout (blocks_done[0])
	);
mux_8 done_mux_1 (
	.datain (column_done[15:8]),
	.control (column_select_1),
	.dataout (blocks_done[1])
	);
mux_8 done_mux_2 (
	.datain (column_done[23:16]),
	.control (column_select_2),
	.dataout (blocks_done[2])
	);
mux_8 done_mux_3 (
	.datain (column_done[31:24]),
	.control (column_select_3),
	.dataout (blocks_done[3])
	);
mux_8 done_mux_4 (
	.datain (column_done[39:32]),
	.control (column_select_4),
	.dataout (blocks_done[4])
	);
mux_8 done_mux_5 (
	.datain (column_done[47:40]),
	.control (column_select_5),
	.dataout (blocks_done[5])
	);
mux_8 done_mux_6 (
	.datain (column_done[55:48]),
	.control (column_select_6),
	.dataout (blocks_done[6])
	);
mux_8 done_mux_7 (
	.datain (column_done[63:56]),
	.control (column_select_7),
	.dataout (blocks_done[7])
	);	


always @(posedge clk) begin	//This buffers the data from the columns BUT I HAVE TO EQUALIZE THIS DELAY
	buffer_reg_2 <=  buffer_reg;	
end

wire [7:0] dataout_scrambled;

mux_8 serializer_mux_0 (	//Muxes to the serializer 
	.datain (buffer_reg_2),
	.control (mux_control_0),
	.dataout (dataout_scrambled[0])
	);
mux_8 serializer_mux_1 (
	.datain (buffer_reg_2),
	.control (mux_control_1),
	.dataout (dataout_scrambled[1])
	);
mux_8 serializer_mux_2 (
	.datain (buffer_reg_2),
	.control (mux_control_2),
	.dataout (dataout_scrambled[2])
	);
mux_8 serializer_mux_3 (
	.datain (buffer_reg_2),
	.control (mux_control_3),
	.dataout (dataout_scrambled[3])
	);
mux_8 serializer_mux_4 (
	.datain (buffer_reg_2),
	.control (mux_control_4),
	.dataout (dataout_scrambled[4])
	);
mux_8 serializer_mux_5 (
	.datain (buffer_reg_2),
	.control (mux_control_5),
	.dataout (dataout_scrambled[5])
	);
mux_8 serializer_mux_6 (
	.datain (buffer_reg_2),
	.control (mux_control_6),
	.dataout (dataout_scrambled[6])
	);
mux_8 serializer_mux_7 (
	.datain (buffer_reg_2),
	.control (mux_control_7),
	.dataout (dataout_scrambled[7])
	);

always @(*) begin
	if (header) begin
		dataout = header_reg;
	end else begin
		case (parallel_columns)
		2'd0: begin
			dataout = dataout_scrambled;
		   end
		2'd1: begin
			dataout = {6'b000000, dataout_scrambled[4], dataout_scrambled[0]};
		   end
		2'd2: begin
			dataout = {4'b0000, dataout_scrambled[6], dataout_scrambled[4], dataout_scrambled[2], dataout_scrambled[0]};
		   end
		2'd3: begin
			dataout = dataout_scrambled;
		   end
		endcase
	end
end

//READOUT:
/*
- Start at start_readout
- Issue reset_counters
- Insert header (start_header signal? this is a timing mess)
- Issue enable_readout (check for pause signal)
- Wait for all_columns_done (EOC blocks should disable their column_done after a cycle)
- Disable enable_readout
- Repeat until end_readout
*/

/*
States (pause aware in states 3-12+): 4-bits?
0 - when off
1 - reset counters (including header column counter) 
2+ - wait for negedge of pause to enable encoder (or anyway sync to the pause signal, so the readout is synchronized with the encoder)
3-10 - insert header (8/4/2/1 cycles) + issue move token
11 - readout until columns_done; goto 2 if there are still columns to be read
12+ - wait to disable encoder because I have to fill an 8-bit word.
*/

reg [4:0] readout_state;
reg [4:0] readout_state_next;
reg pause_old, pause_negedge;
reg reset_header;

// Calculate the negative edge of the pause signal to sync the state machine
always @(posedge clk) begin
	pause_old <= pause;
end

always @(*) begin
	if (pause_old & ~pause) begin
		pause_negedge = 1;
	end else begin
		pause_negedge = 0;
	end
end

reg [39:0] pause_negedge_delayline;
reg pause_negedge_delayed;
always @(posedge clk) begin
	pause_negedge_delayline <= {pause_negedge_delayline[38:0], pause_negedge};
end

always @(*) begin
	case (parallel_columns)
	2'd0: begin
		pause_negedge_delayed = pause_negedge_delayline[7];
	   end
	2'd1: begin
		pause_negedge_delayed = pause_negedge_delayline[3];
	   end
	2'd2: begin
		pause_negedge_delayed = pause_negedge_delayline[1];
	   end
	2'd3: begin
		pause_negedge_delayed = pause_negedge_delayline[0];
	   end
	endcase
end

reg [7:0] sync_8bits_delayline;
reg sync_8bits_delayed;
always @(posedge clk) begin
	sync_8bits_delayline <= {sync_8bits_delayline[6:0], sync_8bits};
end
always @(*) begin
	sync_8bits_delayed = sync_8bits_delayline[5];
end

reg [39:0] sync_16bits_delayline;
reg sync_16bits_delayed;
always @(posedge clk) begin
	sync_16bits_delayline <= {sync_16bits_delayline[38:0], sync_16bits};
end
always @(*) begin
	case (parallel_columns)
	2'd0: begin
		sync_16bits_delayed = sync_16bits_delayline[5]; //It could be different...5
	   end
	2'd1: begin
		sync_16bits_delayed = sync_16bits_delayline[1]; //It could be different...1
	   end
	2'd2: begin
		sync_16bits_delayed = sync_16bits_delayline[39]; //It could be different...39
	   end
	2'd3: begin
		sync_16bits_delayed = sync_16bits_delayline[39]; //It could be different...39
	   end
	endcase
end

always @(posedge clk) begin
	if (~reset) begin
		readout_state <= 0;
	end else begin
		if (~pause || readout_state == 0) begin
			readout_state <= readout_state_next;
		end
	end
end

always @(*) begin
	case (readout_state)
	0: begin
		if (start_readout) begin
			readout_state_next = 1;
		end else begin
			readout_state_next = 0;
		end
	   end
	1: begin //reset_counters
		if (pause_negedge_delayed) begin
			readout_state_next = 2;
		end else begin
			readout_state_next = 1;
		end
	   end
	26: begin		       
		readout_state_next = 27;
	   end
	27: begin		       
		readout_state_next = 28;
	   end
	28: begin		       
		readout_state_next = 29;
	   end
	29: begin		       
		readout_state_next = 30;
	   end
	30: begin
		readout_state_next = 2;
	   end
	2: begin //move token + start_header + enable_encoder
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 3;
		   end
		2'd1: begin
			readout_state_next = 4;
		   end
		2'd2: begin
			readout_state_next = 6;
		   end
		2'd3: begin
			readout_state_next = 10;
		   end
		endcase
	   end
	3: begin
		readout_state_next = 4;
	   end
	4: begin
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 5;
		   end
		2'd1: begin
			readout_state_next = 6;
		   end
		2'd2: begin
			readout_state_next = 10;
		   end
		2'd3: begin
			readout_state_next = 10;
		   end
		endcase
	   end
	5: begin
		readout_state_next = 6;
	   end
	6: begin
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 7;
		   end
		2'd1: begin
			readout_state_next = 8;
		   end
		2'd2: begin
			readout_state_next = 10;
		   end
		2'd3: begin
			readout_state_next = 10;
		   end
		endcase
	   end
	7: begin
		readout_state_next = 8;
	   end
	8: begin
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 9;
		   end
		2'd1: begin
			readout_state_next = 10;
		   end
		2'd2: begin
			readout_state_next = 10;
		   end
		2'd3: begin
			readout_state_next = 10;
		   end
		endcase
	   end
	9: begin
		readout_state_next = 10;
	   end
	10: begin	//enable_readout	//Insert control structure to send carrier_extend here??
		if (all_columns_done && sync_8bits_delayed) begin
			readout_state_next = 11;
		end else begin
			readout_state_next = 10;
		end
	   end
	11: begin //carrier extend
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 12;
		   end
		2'd1: begin
			readout_state_next = 13;
		   end
		2'd2: begin
			readout_state_next = 15;
		   end
		2'd3: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		endcase
	   end
	12: begin
		readout_state_next = 13;
	   end
	13: begin
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 14;
		   end
		2'd1: begin
			readout_state_next = 15;
		   end
		2'd2: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		2'd3: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		endcase
	   end
	14: begin
		readout_state_next = 15;
	   end
	15: begin
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 16;
		   end
		2'd1: begin
			readout_state_next = 17;
		   end
		2'd2: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		2'd3: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		endcase
	   end
	16: begin
		readout_state_next = 17;
	   end
	17: begin
		case (parallel_columns)
		2'd0: begin
			readout_state_next = 18;
		   end
		2'd1: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		2'd2: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		2'd3: begin
			if (end_readout) begin
				readout_state_next = 31;
			end else begin
				readout_state_next = 2;
			end
		   end
		endcase
	   end
	18: begin
		if (end_readout) begin
			readout_state_next = 31;
		end else begin
			readout_state_next = 2;
		end
	   end
	31: begin
		if (sync_16bits_delayed)
			readout_state_next = 0;
		else
			readout_state_next = 31;
	   end
	default: begin
		readout_state_next = readout_state + 1;
	   end
	endcase
end

always @(*) begin
	case (readout_state)
	0: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	1: begin
		carrier_extend = 0;
		reset_counters = 1;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	26: begin
		carrier_extend = 0;
		reset_counters = 1;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	27: begin
		carrier_extend = 0;
		reset_counters = 1;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	28: begin
		carrier_extend = 0;
		reset_counters = 1;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	29: begin
		carrier_extend = 0;
		reset_counters = 1;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	30: begin
		carrier_extend = 0;
		reset_counters = 1;
		move_token = 0;
		start_header = 0;
		enable_encoder = 0;
		enable_readout = 0;
		reset_header = 0;
	   end
	2: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 1;
		start_header = 1;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	3: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	4: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	5: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	6: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	7: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	8: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	9: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	10: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 1;
		reset_header = 0;
	   end
	11: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	12: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	13: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	14: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	15: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	16: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	17: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
	18: begin
		carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_encoder = 1;
		enable_readout = 0;
		reset_header = 0;
	   end
//From now on it's idle time
	31: begin
	 	if (sync_16bits_delayed)
			carrier_extend = 0;
		else
			carrier_extend = 1;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_readout = 0;
		reset_header = 1;
		enable_encoder = 0;
	   end
	   default: begin
		carrier_extend = 0;
		reset_counters = 0;
		move_token = 0;
		start_header = 0;
		enable_readout = 0;
		reset_header = 1;
		enable_encoder = 0;
	   end
	endcase
end

//HEADER INSERTING:
/*
- Start at start_header
- Increment the column counter
- in 1-8 clock cycles, insert the header (which is column counter + parallel bits)
- BE AWARE OF THE PAUSE SIGNAL
- Send a signal back when it's done
*/

/*
States (pause aware): 4-bits?
0 - when off
1 - increment column counter + buffer column counter + start sending first bit(s)
2-8 - send remaining data (same mechanism as encoder)
*/

reg [2:0] header_state;
reg [2:0] header_state_next;
reg [5:0] column_counter_header;

always @(posedge clk) begin
	if (~reset) begin
		header_state <= 0;
	end else begin
		if (~pause) begin
			header_state <= header_state_next;
		end
	end
end

always @(*) begin
	case (header_state)
	0: begin
		if (start_header) begin
			case (parallel_columns)
			2'd0: begin
				header_state_next = 1;
			   end
			2'd1: begin
				header_state_next = 2;
			   end
			2'd2: begin
				header_state_next = 4;
			   end
			2'd3: begin
				header_state_next = 0;
			   end
			endcase
		end else begin
			header_state_next = 0;
		end
	   end
	1: begin
		header_state_next = 2;
	   end
	2: begin
		case (parallel_columns)
		2'd0: begin
			header_state_next = 3;
		   end
		2'd1: begin
			header_state_next = 4;
		   end
		endcase
	   end
	3: begin
		header_state_next = 4;
	   end
	4: begin
		case (parallel_columns)
		2'd0: begin
			header_state_next = 5;
		   end
		2'd1: begin
			header_state_next = 6;
		   end
		2'd2: begin
			header_state_next = 0;
		   end
		endcase
	   end
	5: begin
		header_state_next = 6;
	   end
	6: begin
		case (parallel_columns)
		2'd0: begin
			header_state_next = 7;
		   end
		2'd1: begin
			header_state_next = 0;
		   end
		endcase
	   end
	7: begin
		header_state_next = 0;
	   end
	endcase
end

wire [7:0] header_build;
reg [7:0] header_reg_temp;
//assign header_build = {parallel_columns, column_counter_header};
assign header_build = {column_counter_header[0], column_counter_header[1], column_counter_header[2], column_counter_header[3], column_counter_header[4], column_counter_header[5], parallel_columns[0], parallel_columns[1]};
always @(posedge clk) begin
	if (~reset || reset_header) begin
		column_counter_header <= 0;
	end else begin
		case (header_state)
		0: begin
			if (~pause) begin
				if (start_header) begin //this should work as if I have a start header and then a multi-cycle pause I'll not use the updated header
					column_counter_header <= column_counter_header + 1;
					header <= 1;
				end else begin
					header <= 0;
				end
				header_reg[7:0] <= {header_build[0], header_build[1], header_build[2], header_build[3], header_build[4], header_build[5], header_build[6], header_build[7]};
				header_reg_temp <= header_build;
			end
		   end
		1: begin
			header_reg[0] <= {header_reg_temp[6]};	//I use the already buffered data
		   end
		2: begin
			header_reg[1:0] <= {header_reg_temp[4], header_reg_temp[5]};
		   end
		3: begin
			header_reg[0] <= {header_reg_temp[4]};
		   end
		4: begin
			header_reg[3:0] <= {header_reg_temp[0], header_reg_temp[1], header_reg_temp[2], header_reg_temp[3]};
		   end
		5: begin
			header_reg[0] <= {header_reg_temp[2]};
		   end
		6: begin
			header_reg[1:0] <= {header_reg_temp[0], header_reg_temp[1]};
		   end
		7: begin
			header_reg[0] <= {header_reg_temp[0]};
		   end
		endcase
	end
end

//PROGRAMMING:
// I COULD RUN THIS DIRECTLY ON THE SLOW CLOCK. THIS WOULD LEAVE SPACE TO DELAY THE OUTPUT OF THE CLOCK DIVIDER TO EQUALIZE THE CLOCK DELAY,
// WHILE THE TOKEN PART CAN BE DELAYED WITHOYT ISSUES
/*
- Start at start_programming
- Issue reset_counters
- Shift data for 64 cycles
- In between, cycle through programming_token (8 cycles)
- Make sure programming_token is reset to 0 every time
- Buffer data
- Repeat 3602 times (+1?)
- Issue configure_matrix
*/

/*
States (NOT pause aware): 7-bits? less if I use a counter for the 64 states
0 - when off
1 - reset counters
2 - increase the 3602 counter, buffer data, shift data in
3-65 - keep shifting data in
	meanwhiile, 30-37 cycle programming_token
	at 65 also check if we're done
66+ - configure_matrix for "a few" clock cycles
*/

wire count_lfsr;
//wire lfsr_done;
reg reset_lfsr, load_buffer;
//reg configure_matrix;
reg load_tokens;

reg [3:0] programming_state;
reg [3:0] programming_state_next;
reg [2:0] word_position;
reg reset_word_position;

assign count_lfsr = load_buffer && (word_position == 7) && ~lfsr_done;

always @(posedge clk) begin
	if (~reset) begin
		programming_token <= 0;
	end else begin	//issue load_tokens at state 30 only
		programming_token[7:0] <= {programming_token[6:0], load_tokens};
	end
end

always @(posedge clk) begin
	if (reset_word_position || ~reset)
		word_position <= 0;
	else if (load_buffer)
		word_position <= word_position + 1;
end
		
always @(posedge clk) begin
	if (load_buffer) begin
		case (word_position)
		0: column_dataout[7:0] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		1: column_dataout[15:8] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		2: column_dataout[23:16] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		3: column_dataout[31:24] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		4: column_dataout[39:32] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		5: column_dataout[47:40] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		6: column_dataout[55:48] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		7: column_dataout[63:56] <= programming_word[7:0];//{programming_word[0], programming_word[1], programming_word[2], programming_word[3], programming_word[4], programming_word[5], programming_word[6], programming_word[7]};
		endcase
	end
end

wire reset_lfsr_out;
assign reset_lfsr_out = reset_lfsr || ~reset;

lfsr_counter_3602 lfsr(
    .clk(clk),
    .reset(reset_lfsr_out),
    .ce(count_lfsr),
    .lfsr_done(lfsr_done)
    );

always @(posedge clk) begin
	if (~reset) begin
		programming_state <= 0;
	end else begin
		programming_state <= programming_state_next;
	end
end

always @(*) begin
	case (programming_state)
	0: begin
		if (start_programming)
			programming_state_next = 1;
		else
			programming_state_next = 0;
	   end
	1: begin
		if (word_position == 'b111)
			programming_state_next = 2;
		else
			programming_state_next = 0;
	   end
	2: begin 
		if (lfsr_done)
			programming_state_next = 3;
		else
			programming_state_next = 0;
	   end
	default: begin
		programming_state_next = programming_state + 1;
	   end
	endcase
end

always @(*) begin
	case (programming_state)
	0: begin	//if start programming, reset lfsr
		load_buffer = 0;
		configure_matrix = 0;
		load_tokens = 0;
		reset_lfsr = 0;
		reset_word_position = 0;
	   end
	1: begin	//latch data buffer
		reset_lfsr = 0;
		load_buffer = 1;
		configure_matrix = 0;
		load_tokens = 0;
		reset_word_position = 0;
	   end
	2: begin	//start token if not lfsr_done
		reset_lfsr = 0;
		load_buffer = 0;
		configure_matrix = 0;
		reset_word_position = 0;
		if (~lfsr_done)
			load_tokens = 1;
		else
			load_tokens = 0;
	   end	
	11: begin 	//configure_matrix (for 8 cycles, safety first)
		reset_lfsr = 0;
		load_buffer = 0;
		configure_matrix = 1;
		load_tokens = 0;
		reset_word_position = 0;
	   end
	12: begin 	//configure_matrix (for 8 cycles, safety first)
		reset_lfsr = 0;
		load_buffer = 0;
		configure_matrix = 1;
		load_tokens = 0;
		reset_word_position = 0;
	   end
	13: begin 	//configure_matrix (for 8 cycles, safety first)
		reset_lfsr = 0;
		load_buffer = 0;
		configure_matrix = 1;
		load_tokens = 0;
		reset_word_position = 0;
	   end
	14: begin 	//configure_matrix (for 8 cycles, safety first)
		reset_lfsr = 0;
		load_buffer = 0;
		configure_matrix = 1;
		load_tokens = 0;
		reset_word_position = 0;
	   end
	15: begin 	//configure_matrix
		reset_lfsr = 1;
		load_buffer = 0;
		configure_matrix = 1;
		load_tokens = 0;
		reset_word_position = 1;
	   end
	default: begin
		reset_lfsr = 0;
		load_buffer = 0;
		configure_matrix = 0;
		load_tokens = 0;
		reset_word_position = 0;
	   end
	endcase
end
endmodule


module readout (
	input wire clk, //This is the output of the clock divider. It switches to the slow
		   //clock automatically in that block. When it switches, it COULD
		   //generate glitches. The logic MUST ignore pulses for a short while.
	input wire clk_divided,		   
	input wire reset_fast,
	input wire reset_slow,
	input wire [1:0] parallel_columns,
	input wire programming_data,	//serial programming data (starting from column 0 lowest bit)
	input wire [7:0] programming_word,
	input wire [63:0] datain,
	input wire [63:0] column_done,
	input wire pause,	//From the encoder!! Also check that I can start the state machine while pause==1
	input wire sync_8bits,
	input wire sync_16bits,
	output wire configure_matrix,	//At the end of the programming, to latch data
	output wire enable_encoder,		//Enable signal for the 8b10b encoder. This could be used as (a part of) the shift signal for eoc blocks
	output wire [63:0] column_token,	//Enable for the EOC block
	output wire [63:0] column_dataout,	//Programming data to columns
	output wire [7:0] dataout,
	output wire send_clk,
	
	input wire data_ready,
	input wire [4:0] ADDRESS,
//	input wire [7:0] DATA,
	output wire start_PRBS,
	output wire shift_data,
	output wire programming,
	output wire carrier_extend
	);

//wire clk, clk_divided, reset_fast, reset_slow;
//wire [1:0] parallel_columns;
//wire programming_data;
//wire [63:0] datain;
//wire [63:0] column_done;
//wire pause;
//wire configure_matrix;
//wire enable_encoder;     
//wire [63:0] column_token;
//wire [63:0] column_dataout;
//wire [7:0] dataout;
//wire sync_8bits, sync_16bits;
//wire [7:0] programming_word;

//wire start_PRBS;
//wire data_ready;
//wire [4:0] ADDRESS;
//wire [7:0] DATA;
//wire shift_data;
//wire programming;
//wire carrier_extend;
wire start_programming, start_readout, lfsr_done;

readout_control readout (
	.clk(clk_divided),
	.reset(reset_fast),
	.parallel_columns(parallel_columns),
	.programming_data(programming_data),
	.programming_word(programming_word),
	.datain(datain),	//64-bit
	.column_done(column_done),	//64-bit
	.start_programming_input(start_programming),	//from tb
	.start_readout_input(start_readout),	//from tb
	.pause(pause),
	.sync_8bits(sync_8bits),
	.sync_16bits(sync_16bits),
	.configure_matrix(configure_matrix),	//At the end of the programming, to latch data
	.enable_encoder(enable_encoder),		//Enable signal for the 8b10b encoder
	.column_token(column_token),	//Enable for the EOC block
	.column_dataout(column_dataout),	//Programming data to columns
	.dataout(dataout),
	.lfsr_done(lfsr_done),
	.carrier_extend(carrier_extend)
	);
	
reg enable_encoder_sync_fast;
always @(posedge clk_divided) begin
	enable_encoder_sync_fast <= enable_encoder;
end

readout_interpreter interpreter (
	.clk(clk),
	.data_ready(data_ready),
	.ADDRESS(ADDRESS),
	//.DATA(DATA),
	.reset(reset_slow),
	.send_clk(send_clk),
	.lfsr_done(lfsr_done),	   //for the shift_data signal (programming)
	.enable_encoder(enable_encoder_sync_fast),   //for the shift_data signal (readout)
	
	.start_PRBS(start_PRBS),
	.start_readout(start_readout),
	.start_programming(start_programming),
	.shift_data(shift_data),
	.programming(programming)	  //for the clock divider
	);
		

endmodule
