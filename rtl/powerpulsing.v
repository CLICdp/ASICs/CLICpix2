//Verilog HDL for "CLICpix_DEMO", "counter_8bits" "verilog"


module clock_divider_ppulse (
	input clk,
	input [1:0] S,
	output reg clkout,
	input reset
	);

//reg clkout;
reg clkout2, clkout4, clkout8;
wire clkin, clkoutn2, clkoutn4, clkoutn8;

assign clkin = ~clk;
assign clkoutn2 = ~clkout2;
assign clkoutn4 = ~clkout4;
assign clkoutn8 = ~clkout8;

always @(posedge clkin or negedge reset) begin
	if (~reset)
		clkout2 <= 0;
	else
		clkout2 <= clkoutn2;
end

always @(posedge clkoutn2 or negedge reset) begin
	if (~reset)
		clkout4 <= 0;
	else
		clkout4 <= clkoutn4;
end

always @(posedge clkoutn4 or negedge reset) begin
	if (~reset)
		clkout8 <= 0;
	else
		clkout8 <= clkoutn8;
end

always @(*) begin//
	case(S)
		0 : clkout = clk;
		1 : clkout = clkout2;
		2 : clkout = clkout4;
		3 : clkout = clkout8;
		default : clkout = clk;
	endcase
end

endmodule

module clock_counter (
	input clk,
	input [5:0] count_start,
	input [5:0] count_stop,
	output reg [63:0] enable,
	input reset,
	input power
	);

//reg [63:0] enable;
reg [5:0] current_count;

always @(posedge clk or negedge reset) begin
	if (~reset)
		current_count <= 0;	//resetting enable is not useful, it will reset itself after the reset signal is lifted
	else if (power) begin
		if (current_count < count_start)
			current_count <= current_count + 1;
		else begin
			current_count <= 0;
			enable[63:0] <= {enable[62:0], 1'b1};
		end
	end else begin
		if (current_count < count_stop)
			current_count <= current_count + 1;
		else begin
			current_count <= 0;
			enable[63:0] <= {enable[62:0], 1'b0};
		end
	end
end

endmodule

module ppulse_enabler (
	input clk,
	input [5:0] count_start,
	input [5:0] count_stop,
	output [63:0] enable,
	input reset,
	input power,
	input [1:0] select
	);
	
clock_divider_ppulse cd (
	.clk(clk),
	.S(select),
	.clkout(clkout),
	.reset(reset)
	);

clock_counter cc (
	.clk(clkout),
	.count_start(count_start),
	.count_stop(count_stop),
	.enable(enable),
	.reset(reset),
	.power(power)
	);

endmodule
