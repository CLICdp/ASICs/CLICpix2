`timescale 100ps/1ps

//State machine for DAC programming.
//It checks ADDRESS and DATA and writes DATA in the right register.
module dac_control (
	input clk,
	input data_ready,
	input wire [4:0] ADDRESS,
	input wire [7:0] DATA,
	input reset,
	output reg [7:0] bias_disc_N,
	output reg [7:0] bias_disc_P,
	output reg [7:0] bias_ikrum,
	output reg [7:0] bias_preamp,
	output reg [7:0] bias_DAC,
	output reg [7:0] bias_buffer,
	output reg [7:0] casc_preamp,
	output reg [7:0] casc_DAC,
	output reg [7:0] casc_Nmirror,
	output reg [7:0] vfbk,
	output reg [7:0] vthMSB,
	output reg [7:0] vthLSB,
	output reg [7:0] test_cap1_LSB,	//New signal
	output reg [7:0] test_cap1_MSB,
	output reg [7:0] test_cap2,	//New signal
	output reg [7:0] bias_disc_N_OFF,
	output reg [7:0] bias_disc_P_OFF,
	output reg [7:0] bias_preamp_OFF,
	output reg [21:0] output_mux_DAC,	//Updated size!
	
	output wire [7:0] bias_disc_N_n,
	output wire [7:0] bias_disc_P_n,
	output wire [7:0] bias_ikrum_n,
	output wire [7:0] bias_preamp_n,
	output wire [7:0] bias_DAC_n,
	output wire [7:0] bias_buffer_n,
	output wire [7:0] casc_preamp_n,
	output wire [7:0] casc_DAC_n,
	output wire [7:0] casc_Nmirror_n,
	output wire [7:0] vfbk_n,
	output wire [7:0] vthMSB_n,
	output wire [7:0] vthLSB_n,
	output wire [7:0] test_cap1_LSB_n,	//New signal
	output wire [7:0] test_cap1_MSB_n,	//New signal
	output wire [7:0] test_cap2_n,	//New signal
	output wire [7:0] bias_disc_N_OFF_n,
	output wire [7:0] bias_disc_P_OFF_n,
	output wire [7:0] bias_preamp_OFF_n,
	output wire [21:0] output_mux_DAC_n,	//Updated size!
	
	output reg [7:0] config_global,
	output reg [7:0] poweron_timer,
	output reg [7:0] poweroff_timer,
	output reg [7:0] dac_out_encoded,
	
	output reg [7:0] pulsegen_counts_LSB,	//New
	output reg [7:0] pulsegen_counts_MSB,	//New
	output reg [7:0] pulsegen_delay_LSB,	//New
	output reg [7:0] pulsegen_delay_MSB,	//New
	output reg [7:0] readout_config,	//New signal
	output reg [7:0] programming_word
	);

//reg [7:0] bias_disc_N, bias_disc_P, bias_ikrum, bias_preamp, bias_DAC, bias_buffer, casc_preamp, casc_DAC;
//reg [7:0] casc_Nmirror, vfbk, vthMSB, vthLSB, bias_disc_N_OFF, bias_disc_P_OFF, bias_preamp_OFF, test_cap1_MSB, test_cap1_LSB, test_cap2;
//reg [7:0] config_global, poweron_timer, poweroff_timer, readout_config;
//reg [21:0] output_mux_DAC;
//wire [21:0] output_mux_DAC_n;
//wire [7:0] bias_disc_N_n, bias_disc_P_n, bias_ikrum_n, bias_preamp_n, bias_DAC_n, bias_buffer_n, casc_preamp_n, casc_DAC_n;
//wire [7:0] casc_Nmirror_n, vfbk_n, vthMSB_n, vthLSB_n, bias_disc_N_OFF_n, bias_disc_P_OFF_n, bias_preamp_OFF_n, test_cap1_MSB_n, test_cap1_LSB_n, test_cap2_n;
//reg [7:0] pulsegen_counts_LSB, pulsegen_counts_MSB, pulsegen_delay_LSB, pulsegen_delay_MSB, dac_out_encoded;
//wire [7:0] DATA;
//wire [4:0] ADDRESS; //move to 8 bits. I can still only check 5...

reg [2:0] state;
reg [2:0] next_state;

//reg [7:0] programming_word;
//reg SPI_output;

assign bias_disc_N_n = ~bias_disc_N;
assign bias_disc_P_n = ~bias_disc_P;
assign bias_ikrum_n = ~bias_ikrum;
assign bias_preamp_n = ~bias_preamp;
assign bias_DAC_n = ~bias_DAC;
assign bias_buffer_n = ~bias_buffer;
assign casc_preamp_n = ~casc_preamp;
assign casc_DAC_n = ~casc_DAC;
assign casc_Nmirror_n = ~casc_Nmirror;
assign vfbk_n = ~vfbk;
assign vthMSB_n = ~vthMSB;
assign vthLSB_n = ~vthLSB;
assign bias_disc_N_OFF_n = ~bias_disc_N_OFF;
assign bias_disc_P_OFF_n = ~bias_disc_P_OFF;
assign bias_preamp_OFF_n = ~bias_preamp_OFF;
assign test_cap1_MSB_n = ~test_cap1_MSB;
assign test_cap1_LSB_n = ~test_cap1_LSB;
assign test_cap2_n = ~test_cap2;
assign output_mux_DAC_n = ~output_mux_DAC;

always @(posedge clk) begin	//maybe I can sync this?
	if (~reset) begin
		poweron_timer[7:0] <= 0;
		poweroff_timer[7:0] <= 0;
		config_global[7:0] <= 8'b01011000;
		programming_word <= 0;
		
		pulsegen_counts_LSB[7:0] <= 0;
		pulsegen_counts_MSB[7:0] <= 0;
		pulsegen_delay_LSB[7:0] <= 0;
		pulsegen_delay_MSB[7:0] <= 0;
		readout_config[7:0] <= 8'b00001011;
		
		bias_disc_N[7:0] <= 'd30;
		bias_disc_P[7:0] <= 'd50;
		bias_ikrum[7:0] <= 'd80;
		bias_preamp[7:0] <= 'd90;
		bias_DAC[7:0] <= 'd64;
		bias_buffer[7:0] <= 'd136;
		casc_preamp[7:0] <= 'd133;
		casc_DAC[7:0] <= 'd133;
		casc_Nmirror[7:0] <= 'd133;
		vfbk[7:0] <= 'd133;
		vthMSB[7:0] <= 'd138;
		vthLSB[7:0] <= 0;
		test_cap1_MSB[7:0] <= 'd138;
		test_cap1_LSB[7:0] <= 0;
		test_cap2[7:0] <= 'd133;
		bias_disc_N_OFF[7:0] <= 'd30;
		bias_disc_P_OFF[7:0] <= 'd50;
		bias_preamp_OFF[7:0] <= 'd90;
		dac_out_encoded <= 0;
	end else begin
		case (state)
		1: begin
        		case (ADDRESS)
		        'b00101: begin
				bias_disc_N[7:0] <= DATA[7:0];
			   end
		        'b00110: begin
       				bias_disc_P[7:0] <= DATA[7:0];
		           end
        		'b00111: begin
				bias_ikrum[7:0] <= DATA[7:0];
			   end
       			'b01000: begin
				bias_preamp[7:0] <= DATA[7:0];
			   end
		        'b01001: begin
        			bias_DAC[7:0] <= DATA[7:0];
			   end
		        'b01010: begin
				bias_buffer[7:0] <= DATA[7:0];
			   end
		        'b01011: begin
				casc_preamp[7:0] <= DATA[7:0];
			   end
		        'b01100: begin
				casc_DAC[7:0] <= DATA[7:0];
			   end
		        'b01101: begin
				casc_Nmirror[7:0] <= DATA[7:0];
			   end
		        'b01110: begin
				vfbk[7:0] <= DATA[7:0];
			   end
		        'b10011: begin
				vthMSB[7:0] <= DATA[7:0];
			   end
		        'b10010: begin
				vthLSB[7:0] <= DATA[7:0];
			   end
		        'b11110: begin
				config_global[7:0] <= DATA[7:0];
			   end
		        'b11000: begin
				poweron_timer[7:0] <= DATA[7:0];
			   end
		        'b11001: begin
                		poweroff_timer[7:0] <= DATA[7:0];
			   end
		        'b10111: begin
				dac_out_encoded <= DATA[7:0];
			   end
			'b01111: begin
				bias_disc_N_OFF[7:0] <= DATA[7:0];
			   end
		        'b10000: begin
				bias_disc_P_OFF[7:0] <= DATA[7:0];
			   end
		        'b10001: begin
				bias_preamp_OFF[7:0] <= DATA[7:0];
			   end
		        'b10100: begin
				test_cap1_LSB[7:0] <= DATA[7:0];
			   end
		        'b10101: begin
				test_cap1_MSB[7:0] <= DATA[7:0];
			   end
		        'b10110: begin
				test_cap2[7:0] <= DATA[7:0];
			   end
		        'b11111: begin
                		readout_config[7:0] <= DATA[7:0];
			   end
		        'b11010: begin
				pulsegen_counts_LSB[7:0] <= DATA[7:0];
			   end
		        'b11011: begin
				pulsegen_counts_MSB[7:0] <= DATA[7:0];
			   end
		        'b11100: begin
				pulsegen_delay_LSB[7:0] <= DATA[7:0];
			   end
		        'b11101: begin
				pulsegen_delay_MSB[7:0] <= DATA[7:0];
			   end
		        'b00010: begin	//PROGRAMMING
                		programming_word[7:0] <= DATA[7:0];
			   end
		        default: begin
		           end
		        endcase
		   end
		default: begin
		   end
		endcase
	end
end

always @(*) begin
case (dac_out_encoded[7:0])
'b0000_0000: begin
	output_mux_DAC = 'b00_0000_0000_0000_0000_0000;
end
'b0000_0001: begin
	output_mux_DAC = 'b00_0000_0000_0000_0000_0001;
end
'b0000_0010: begin
	output_mux_DAC = 'b00_0000_0000_0000_0000_0010;
end
'b0000_0011: begin
	output_mux_DAC = 'b00_0000_0000_0000_0000_0100;
end
'b0000_0100: begin
	output_mux_DAC = 'b00_0000_0000_0000_0000_1000;
end
'b0000_0101: begin
	output_mux_DAC = 'b00_0000_0000_0000_0001_0000;
end
'b0000_0110: begin
	output_mux_DAC = 'b00_0000_0000_0000_0010_0000;
end
'b0000_0111: begin
	output_mux_DAC = 'b00_0000_0000_0000_0100_0000;
end
'b0000_1000: begin
	output_mux_DAC = 'b00_0000_0000_0000_1000_0000;
end
'b0000_1001: begin
	output_mux_DAC = 'b00_0000_0000_0001_0000_0000;
end
'b0000_1010: begin
	output_mux_DAC = 'b00_0000_0000_0010_0000_0000;
end
'b0000_1011: begin
	output_mux_DAC = 'b00_0000_0000_0100_0000_0000;
end
'b0000_1100: begin
	output_mux_DAC = 'b00_0000_0000_1000_0000_0000;
end
'b0000_1101: begin
	output_mux_DAC = 'b00_0000_0001_0000_0000_0000;
end
'b0000_1110: begin
	output_mux_DAC = 'b00_0000_0010_0000_0000_0000;
end
'b0000_1111: begin
	output_mux_DAC = 'b00_0000_0100_0000_0000_0000;
end
'b0001_0000: begin
	output_mux_DAC = 'b00_0000_1000_0000_0000_0000;
end
'b0001_0001: begin
	output_mux_DAC = 'b00_0001_0000_0000_0000_0000;
end
'b0001_0010: begin
	output_mux_DAC = 'b00_0010_0000_0000_0000_0000;
end
'b0001_0011: begin
	output_mux_DAC = 'b00_0100_0000_0000_0000_0000;
end
'b0001_0100: begin
	output_mux_DAC = 'b00_1000_0000_0000_0000_0000;
end
'b0001_0101: begin
	output_mux_DAC = 'b01_0000_0000_0000_0000_0000;
end
'b0001_0110: begin
	output_mux_DAC = 'b10_0000_0000_0000_0000_0000;
end
default: begin
	output_mux_DAC = 'b0000_0000_00_0000_0000_0000;
end
endcase
end
//This needs to be longer in order to send out the stored data (+8 clock cycles)
always @(*) begin //CHECK ADDRESS BEFORE STARTING THE STATE MACHINE!
        if (state == 0 && data_ready == 1) begin// && ((ADDRESS[4] == 'b1) || (ADDRESS[3] == 'b1) || (ADDRESS[2] == 'b1))) begin
                next_state = 1;
        end else begin
                if (state > 0) begin
                        next_state = state + 1;
                end else begin
                        next_state = 0;
                end
        end
end

always @(posedge clk) begin //Don't need sync reset...
	if (~reset)
		state <=#0.1 0;
	else
	        state <=#0.1 next_state;
end

endmodule
