//Verilog HDL for "CLICpix_DEMO", "counter_8bits" "verilog"
`timescale 100ps/1ps

module pr_counters (clkTOA, clkTOT, shift, datain, TP, config_reg, dataout, load, TOA0, TOT0, event_count);

input clkTOA, clkTOT, shift, datain, load;
output TP, dataout, TOA0, TOT0, event_count;
output [3:0] config_reg;
reg TP, event_count;
wire dataout, TOA0, TOT0, clkTOA, clkTOT, clkTOT_long, clkTOA_long;
reg [3:0] config_reg;
reg [7:0] TOA;
reg [4:0] TOT;

reg long_counter; ///NEW

CKMUX2D2BWP7THVT mux1 (.I0 (clkTOT), .I1 (clkTOA), .S (long_counter), .Z (clkTOT_long)); ///NEW
CKMUX2D2BWP7THVT mux2 (.I0 (clkTOA), .I1 (clkTOA), .S (long_counter), .Z (clkTOA_long)); ///NEW

assign dataout = TOT[4];
assign TOT0 = TOT[0];
assign TOA0 = TOA[0];

// Configuration latches
//always @(posedge load) begin
always @(*) begin
	if (load) begin
		TP <= TOA[2];
		event_count <= TOA[3];
		config_reg [3:0] <= TOT [3:0];
		long_counter <= TOA[1]; ///NEW
	end 
end

//TOT 5 bit LFSR
always @(posedge clkTOT_long) begin
	if (shift) begin
		TOT[4:0] <= {TOT[3:0], TOA[7]};
	end else begin
		if (long_counter && ((TOT[4:0] != 5'b10000) || (TOA[7:0] != 8'b00000000))) begin ///NEW ORDER OF CHECKS??
			TOT[4:0] <= {TOT[3:0], TOA[7]};
		end else begin
			if (~long_counter && (TOT[4:0] != 5'b10000)) begin //NEW CHECK
				TOT[4:0] <= {TOT[3:0], (TOT[4]~^TOT[2])};
			end
		end
	end
end

//TOA 8 bit LFSR
always @(posedge clkTOA_long) begin
	if (shift) begin
		TOA[7:0] <= {TOA[6:0], datain};
	end else begin
		if (long_counter) begin ///NEW
			TOA[7:0] <= {TOA[6:0], ((TOT[4]^TOA[0])~^(TOA[3]^TOA[2]))};
		end else begin
			if (TOA[7:0] != 8'b10000000) begin //NEW CHECK
				TOA[7:0] <= {TOA[6:0], ((TOA[7]^TOA[5])~^(TOA[4]^TOA[3]))};
			end
		end
	end
end

endmodule

module compression_mux (datain_hit, datain_nothit, dataout, clk, shift, TOA0, TOT0, use_compression, load, mask, flag); /////FLAG
input datain_hit, datain_nothit, clk, shift, TOA0, TOT0, use_compression, load;
output dataout, mask, flag; /////FLAG
reg dataout, flag, mask;
wire clk_long;

CKMUX2D1BWP7THVT mux3 (.I0 ( clk ), .I1 (clk), .S (1'b1), .Z (clk_long)); //MAYBE THE SIZE 2??

//Latch for the mask bit
//always @(posedge load) begin 
always @(*) begin
	if (load) begin
		mask <= dataout;
	end
end

//Assign the signal to check if the pixel was hit or not
wire set_flag;
assign set_flag = ((TOA0 || TOT0) && ~shift);

//Pixel skipping mux
always @(posedge clk_long or posedge set_flag) begin
	if (set_flag) begin
		dataout <= 1;
	end else begin
		if (flag) begin
			dataout <= datain_hit;
		end else begin
			dataout <= datain_nothit;
		end
	end
end

//Flag setting logic
always @(posedge shift or negedge use_compression) begin
	if (~use_compression) begin //I need to add a reset signal if I want to be able to force the programming through bad pixels
		flag <= 1;
	end else begin
		if (mask) begin
			flag <= 0;
		end else begin
			flag <= dataout;
		end
	end
end

endmodule

module registers (clkTOA, clkTOT, clkCM, shift, datain_nothit, TP, config_reg, dataout, load, use_compression, event_count, mask);
input clkTOA, clkTOT, clkCM, shift, datain_nothit, load, use_compression;
output TP, dataout, event_count, mask;
output [3:0] config_reg;
wire clkTOA, clkTOT, clkCM, shift, datain_nothit, TP, datain_hit, load, TOA0, TOT0, event_count, datain_nothit_flag, flag; /////FLAG
wire [3:0] config_reg;
assign datain_nothit_flag = (flag) ? datain_nothit : 1'b0; ///NEW
pr_counters pr (clkTOA, clkTOT, shift, datain_nothit_flag, TP, config_reg, datain_hit, load, TOA0, TOT0, event_count); /////FLAG
compression_mux cm (datain_hit, datain_nothit, dataout, clkCM, shift, TOA0, TOT0, use_compression, load, mask, flag); /////FLAG
endmodule
