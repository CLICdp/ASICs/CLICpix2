//Verilog HDL for "CLICpix_DEMO", "counter_8bits" "verilog"
`timescale 100ps/1ps

module clock_divider_pixel (clk, S, R, clkout);

input clk, R;
input [1:0] S;
output clkout;
reg clkout;
reg clkout2, clkout4, clkout8;
wire clkin, clkoutn2, clkoutn4, clkoutn8;

assign clkin = ~clk;
assign clkoutn2 = ~clkout2;
assign clkoutn4 = ~clkout4;
assign clkoutn8 = ~clkout8;

always @(posedge clkin or posedge R) begin
if (R) begin
	clkout2 <= 0;
end
else begin
	clkout2 <= clkoutn2;
end
end
always @(posedge clkoutn2 or posedge R) begin //SWITCH TO CLKOUT instead of CLKCOUTN?
if (R) begin
	clkout4 <= 0;
end
else begin
	clkout4 <= clkoutn4;
end
end
always @(posedge clkoutn4 or posedge R) begin //SWITCH TO CLKOUT instead of CLKCOUTN?
if (R) begin
	clkout8 <= 0;
end
else begin
	clkout8 <= clkoutn8;
end
end

always @(*) begin
	if (R) begin
		clkout <= clk;
	end else begin
	case(S)
		0 : clkout <= clk;
		1 : clkout <= clkout2;
		2 : clkout <= clkout4;
		3 : clkout <= clkout8;
		default : clkout <= clk;
	endcase
	end//
end

endmodule
