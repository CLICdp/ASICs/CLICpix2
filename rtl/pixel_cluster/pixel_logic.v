//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/digital/pixel_logic/syn/verilog/clock_divider_sync.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/pixel_cluster/pr_counters.v"
`timescale 100ps/1ps

//// Asynchronous state machine to control the enable signal for TOT measurement
module clock_gating_TOT (clkin, clkout, S, D, R);
input S, D, clkin, R; //S = Shutter; D = Disc output
output clkout;
reg X, Y, Z, A, B, clkout;
always @(*) begin
X <=#1 (X && S) || (X && ~Y && D) || (~Y && D && S);
Y <=#1 (Y && D) || (~X && D && ~S) || (X && ~D && S);
Z <=#1 X && ~Y;



A <=#1 (~clkin && Z) || (clkin && A) || (A && Z);
B <=#1 (~A && Z) || (~clkin && Z && B);
clkout <=#1 A && clkin;
end
endmodule

// Asynchronous state machine to control the enable signal for TOA measurement
module clock_gating_TOA (clkin, clkout, S, D, R); //S = Shutter; D = Disc output
input S, D, clkin, R;
output clkout;
reg X, Y, A, B, clkout;
always @(*) begin
X <=#1 (~S && D) || (X && ~Y && D);
Y <=#1 (Y && S) || (~X && S && D);

A <=#1 (~clkin && Y) || (clkin && A) || (A && Y);
B <=#1 (~A && Y) || (~clkin && Y && B);
clkout <=#1 A && clkin;
end
endmodule

// Asynchronous state machine to control the enable signal for pulse counting
module counting_pulse (clkin, clkout, S, D);
input S, D, clkin; //S = Shutter; D = Disc output
output clkout;
reg Z, A, B, clkout;
always @(*) begin
Z <=#1 S && D;

A <=#1 (A && clkin) || (~clkin && ~B && Z) || (A && Z);
B <=#1 (A && clkin) || (A && B && Z) || (clkin && Z);
clkout <=#1 A && B;
end
endmodule

module pixel_logic (clkin, S, R, Dpremask, F, dataout, datain, TP, config_reg, load, use_compression, clk_count);
input clkin, S, R, Dpremask, datain, load, use_compression;
input [1:0] F;
output clk_count;
output dataout, TP;
output [3:0] config_reg;
wire clkin, S, R, D, Dpremask, TOA_clk, TOT_clk_undivided, TOT_clk, TOA_clk_reg, TOT_clk_reg, CM_clk_reg, clkTOA_buf, clkTOT_buf, clkCM_buf, datain, dataout, load, TP, use_compression, event_count, mask;
wire [3:0] config_reg, config_reg_n;
wire [1:0] F;

counting_pulse cp (clkin, clk_count, S, D);
assign D = Dpremask && ~mask;
wire TOA_clk_count;

clock_gating_TOT cg_TOT (clkin, TOT_clk_undivided, S, D, R);
clock_gating_TOA cg_TOA (clkin, TOA_clk, S, D, R);
clock_divider_pixel cd (TOT_clk_undivided, F, R, TOT_clk); //I HAVE TO RESET THIS...

assign TOA_clk_count = (event_count) ? clk_count : TOA_clk;

//Clock buffers and muxes (to select the right clock and to equalize the delays)
CKBD2BWP7THVT bufA (.I (clkin), .Z (TOA_clk_reg));
CKBD2BWP7THVT bufT (.I (clkin), .Z (TOT_clk_reg));
CKBD2BWP7THVT bufC (.I (clkin), .Z (CM_clk_reg));
CKMUX2D2BWP7THVT muxA (.I0 (TOA_clk_count), .I1 (TOA_clk_reg), .S (R), .Z (clkTOA_buf));
CKMUX2D2BWP7THVT muxT (.I0 (TOT_clk), .I1 (TOT_clk_reg), .S (R), .Z (clkTOT_buf));
CKMUX2D0BWP7THVT muxC (.I0 (1'b0), .I1 (CM_clk_reg), .S (R), .Z (clkCM_buf));

registers pr (clkTOA_buf, clkTOT_buf, clkCM_buf, R, datain, TP, config_reg, dataout, load, use_compression, event_count, mask);

endmodule
