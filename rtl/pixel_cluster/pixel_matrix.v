`timescale 100ps/1ps
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/periphery.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/pixel_cluster/pixel_cluster.v"
module double_column (
	input wire clk_input,
	input wire data_input,
	output wire data_output,
	output wire fastor_output,
	input wire Shutter,
	input wire Readout,
	input wire Load_configuration,
	input wire use_compression,
	input wire use_cluster_compression,
	input wire TPswitch,
	input wire Polarity,
	input wire [1:0] F,
	input wire [127:0] discriminator_input_left,
	input wire [127:0] discriminator_input_right,
	output wire [127:0] testpulse_output_left,
	output wire [127:0] testpulse_output_right
	);

wire [16:0] clk, data, fastor;
//clk[0] = input clk
assign clk[0] = clk_input;
//data[0] = output data
assign data_output = data[0];
//data[16] = input data
assign data[16] = data_input;
//fastor[0] = output fastor
assign fastor_output = fastor[0];
//fastor[16] = 1'b0
assign fastor[16] = 1'b0;

//wire Shutter, Readout, Load_configuration, use_compression, use_cluster_compression, TPswitch, Polarity;
//wire [1:0] F;
//wire [127:0] discriminator_output_left, discriminator_output_right;
//wire [127:0] testpulse_output_left, testpulse_output_right;
genvar j;
generate
	for (j=0; j < 16; j=j+1) begin
	pixel_cluster superpixel (
		.clkin(clk[j]),	// Column clock (connected to the periphery clk_column output for the first super-pixel, to clkout of the super-pixel below otherwise)
		.clkout(clk[j+1]),	// Column clock output (connected to the clkin input of the super-pixel above, left unconnected for the last super-pixel)
		.Shutter(Shutter),	// Shutter signal (column-wise: connected to the periphery shutter_multi output)
		.Readout(Readout),	// Readout/programming signal (column-wise: connected to the periphery shift_multi output)
		.F(F),		// Clock divider configuration (column-wise: connected to the periphery clock_div0_multi and clock_div1_multi output)
		.dataout(data[j]),	// Data output (connected to the periphery datain_column input for the first super-pixel, to datain of the super-pixel below otherwise)
		.datain(data[j+1]),	// Data input (connected to the periphery dataout_column output for the last super-pixel, to dataout of the super-pixel above otherwise)
		.load(Load_configuration),	// Load_configuration signal (column-wise: connected to the periphery configure_matrix_multi output)
		.use_compression(use_compression),	// Use_compression signal (column-wise: connected to the periphery use_compression_multi output)
		.use_cluster_compression(use_cluster_compression),	// Use_compression signal (column-wise: connected to the periphery use_cluster_compression_multi output)
		.fastor_in(fastor[j+1]),	// Fastor signal for the compression algorithm (connected to 1'b0 for the last super-pixel, to fastor_out of the super-pixel above otherwise)
		.fastor_out(fastor[j]),	// Fastor signal  (connected to the periphery fastor input for the first super-pixel, to fastor_in of the super-pixel below otherwise)
		.TPswitch(TPswitch),	// Switching signal for the test pulse (column-wise: connected to the periphery TPswitch_multi output)
		.polarityl(Polarity),	// Polarity signal (column-wise: connected to the periphery polarity_multi output)
		.polarityr(Polarity),	// Polarity signal(), same net as the above one. They are separated only for routing purposes.
		.TPl1(), .TPswitch_nl1(), .TPswitchl1(testpulse_output_left[7+(8*j)]), .polarityl1(),
		.TPl2(), .TPswitch_nl2(), .TPswitchl2(testpulse_output_left[6+(8*j)]), .polarityl2(),
		.TPl3(), .TPswitch_nl3(), .TPswitchl3(testpulse_output_left[5+(8*j)]), .polarityl3(),
		.TPl4(), .TPswitch_nl4(), .TPswitchl4(testpulse_output_left[4+(8*j)]), .polarityl4(),
		.TPl5(), .TPswitch_nl5(), .TPswitchl5(testpulse_output_left[3+(8*j)]), .polarityl5(),
		.TPl6(), .TPswitch_nl6(), .TPswitchl6(testpulse_output_left[2+(8*j)]), .polarityl6(),
		.TPl7(), .TPswitch_nl7(), .TPswitchl7(testpulse_output_left[1+(8*j)]), .polarityl7(),
		.TPl8(), .TPswitch_nl8(), .TPswitchl8(testpulse_output_left[0+(8*j)]), .polarityl8(),
		.TPr1(), .TPswitch_nr1(), .TPswitchr1(testpulse_output_right[7+(8*j)]), .polarityr1(),
		.TPr2(), .TPswitch_nr2(), .TPswitchr2(testpulse_output_right[6+(8*j)]), .polarityr2(),
		.TPr3(), .TPswitch_nr3(), .TPswitchr3(testpulse_output_right[5+(8*j)]), .polarityr3(),
		.TPr4(), .TPswitch_nr4(), .TPswitchr4(testpulse_output_right[4+(8*j)]), .polarityr4(),
		.TPr5(), .TPswitch_nr5(), .TPswitchr5(testpulse_output_right[3+(8*j)]), .polarityr5(),
		.TPr6(), .TPswitch_nr6(), .TPswitchr6(testpulse_output_right[2+(8*j)]), .polarityr6(),
		.TPr7(), .TPswitch_nr7(), .TPswitchr7(testpulse_output_right[1+(8*j)]), .polarityr7(),
		.TPr8(), .TPswitch_nr8(), .TPswitchr8(testpulse_output_right[0+(8*j)]), .polarityr8(),
		.config_regl1(), .config_reg_nl1(),
		.config_regl2(), .config_reg_nl2(),
		.config_regl3(), .config_reg_nl3(),
		.config_regl4(), .config_reg_nl4(),
		.config_regl5(), .config_reg_nl5(),
		.config_regl6(), .config_reg_nl6(),
		.config_regl7(), .config_reg_nl7(),
		.config_regl8(), .config_reg_nl8(),
		.config_regr1(), .config_reg_nr1(),
		.config_regr2(), .config_reg_nr2(),
		.config_regr3(), .config_reg_nr3(),
		.config_regr4(), .config_reg_nr4(),
		.config_regr5(), .config_reg_nr5(),
		.config_regr6(), .config_reg_nr6(),
		.config_regr7(), .config_reg_nr7(),
		.config_regr8(), .config_reg_nr8(),
		.Dl1(discriminator_input_left[7+(8*j)]),
		.Dl2(discriminator_input_left[6+(8*j)]),
		.Dl3(discriminator_input_left[5+(8*j)]),
		.Dl4(discriminator_input_left[4+(8*j)]),
		.Dl5(discriminator_input_left[3+(8*j)]),
		.Dl6(discriminator_input_left[2+(8*j)]),
		.Dl7(discriminator_input_left[1+(8*j)]),
		.Dl8(discriminator_input_left[0+(8*j)]),
		.Dr1(discriminator_input_right[7+(8*j)]),
		.Dr2(discriminator_input_right[6+(8*j)]),
		.Dr3(discriminator_input_right[5+(8*j)]),
		.Dr4(discriminator_input_right[4+(8*j)]),
		.Dr5(discriminator_input_right[3+(8*j)]),
		.Dr6(discriminator_input_right[2+(8*j)]),
		.Dr7(discriminator_input_right[1+(8*j)]),
		.Dr8(discriminator_input_right[0+(8*j)])
		);
	end
endgenerate
endmodule


module CLICpix2 (
//Basic I/O interfaces
	input wire clk,
	input wire reset_n,
	input wire power_pin,
	input wire shutter,
	input wire TPswitch,
	output wire clk_out,
	output wire dataout_serial,
	
//SPI-like interface
	input wire clk_slow, //also used for acquisition
	input wire chip_select,	
	input wire MOSI,
	output wire MISO,

//Analog signals to/from columns		
	output wire [63:0] ppulse_enable_gated,
	output wire [63:0] ppulse_enable_gated_n,
	
//Signals to the analog periphery	
	output wire bgap_enable,
	output wire bgap_enable_n,
	output wire [2:0] bgap_control_R3,
	output wire [2:0] bgap_control_R1,
	
	output wire [7:0] bias_disc_N,
	output wire [7:0] bias_disc_P,
	output wire [7:0] bias_ikrum,
	output wire [7:0] bias_preamp,
	output wire [7:0] bias_DAC,
	output wire [7:0] bias_buffer,
	output wire [7:0] casc_preamp,
	output wire [7:0] casc_DAC,
	output wire [7:0] casc_Nmirror,
	output wire [7:0] vfbk,
	output wire [7:0] vthMSB,
	output wire [7:0] vthLSB,
	output wire [7:0] test_cap1_LSB,	//New signal
	output wire [7:0] test_cap1_MSB,
	output wire [7:0] test_cap2, //New signal
	output wire [7:0] bias_disc_N_OFF,
	output wire [7:0] bias_disc_P_OFF,
	output wire [7:0] bias_preamp_OFF,
	output wire [21:0] output_mux_DAC,	 //Updated size!
	
	output wire [7:0] bias_disc_N_n,
	output wire [7:0] bias_disc_P_n,
	output wire [7:0] bias_ikrum_n,
	output wire [7:0] bias_preamp_n,
	output wire [7:0] bias_DAC_n,
	output wire [7:0] bias_buffer_n,
	output wire [7:0] casc_preamp_n,
	output wire [7:0] casc_DAC_n,
	output wire [7:0] casc_Nmirror_n,
	output wire [7:0] vfbk_n,
	output wire [7:0] vthMSB_n,
	output wire [7:0] vthLSB_n,
	output wire [7:0] test_cap1_LSB_n,	//New signal
	output wire [7:0] test_cap1_MSB_n,	//New signal
	output wire [7:0] test_cap2_n, //New signal
	output wire [7:0] bias_disc_N_OFF_n,
	output wire [7:0] bias_disc_P_OFF_n,
	output wire [7:0] bias_preamp_OFF_n,
	output wire [21:0] output_mux_DAC_n,	 //Updated size!
	
//Signals to/from the pixel matrix
	input wire [127:0] discriminator_input[127:0],
	output wire [127:0] testpulse_output[127:0]
	);

wire [63:0] datain_column;//
wire [63:0] fastor;//
wire [63:0] dataout_column;//
wire [63:0] clk_column;//
wire [63:0] clock_div0_multi;//
wire [63:0] clock_div1_multi;//
wire [63:0] use_compression_multi;//
wire [63:0] use_cluster_compression_multi;//
wire [63:0] polarity_multi;//
wire [63:0] configure_matrix_multi;//
wire [63:0] shift_multi;//
wire [63:0] TPswitch_multi;//
wire [63:0] shutter_multi;//

periphery periphery (

//Basic I/O interfaces
	.clk(clk),
	.reset_n(reset_n),
	.power_pin(power_pin),
	.shutter(shutter),
	.TPswitch(TPswitch),
	.clk_out(clk_out),
	.dataout_serial(dataout_serial),
	
//SPI-like interface
	.clk_slow(clk_slow), //also used for acquisition
	.chip_select(chip_select),	
	.MOSI(MOSI),
	.MISO(MISO),

//Signals to/from columns		
	.datain_column(datain_column),
	.fastor(fastor),
	.dataout_column(dataout_column),
	.clk_column(clk_column),
	.clock_div0_multi(clock_div0_multi),
	.clock_div1_multi(clock_div1_multi),
	.use_compression_multi(use_compression_multi),
	.use_cluster_compression_multi(use_cluster_compression_multi),
	.polarity_multi(polarity_multi),
	.configure_matrix_multi(configure_matrix_multi),
	.shift_multi(shift_multi),
	.TPswitch_multi(TPswitch_multi),
	.shutter_multi(shutter_multi),

	.ppulse_enable_gated(ppulse_enable_gated),
	.ppulse_enable_gated_n(ppulse_enable_gated_n),
	
//Signals to the analog periphery	
	.bgap_enable(bgap_enable),
	.bgap_enable_n(bgap_enable_n),
	.bgap_control_R3(bgap_control_R3),
	.bgap_control_R1(bgap_control_R1),
	
	.bias_disc_N(bias_disc_N),
	.bias_disc_P(bias_disc_P),
	.bias_ikrum(bias_ikrum),
	.bias_preamp(bias_preamp),
	.bias_DAC(bias_DAC),
	.bias_buffer(bias_buffer),
	.casc_preamp(casc_preamp),
	.casc_DAC(casc_DAC),
	.casc_Nmirror(casc_Nmirror),
	.vfbk(vfbk),
	.vthMSB(vthMSB),
	.vthLSB(vthLSB),
	.test_cap1_LSB(test_cap1_LSB),   //New signal
	.test_cap1_MSB(test_cap1_MSB),
	.test_cap2(test_cap2), //New signal
	.bias_disc_N_OFF(bias_disc_N_OFF),
	.bias_disc_P_OFF(bias_disc_P_OFF),
	.bias_preamp_OFF(bias_preamp_OFF),
	.output_mux_DAC(output_mux_DAC),  //Updated size!
	
	.bias_disc_N_n(bias_disc_N_n),
	.bias_disc_P_n(bias_disc_P_n),
	.bias_ikrum_n(bias_ikrum_n),
	.bias_preamp_n(bias_preamp_n),
	.bias_DAC_n(bias_DAC_n),
	.bias_buffer_n(bias_buffer_n),
	.casc_preamp_n(casc_preamp_n),
	.casc_DAC_n(casc_DAC_n),
	.casc_Nmirror_n(casc_Nmirror_n),
	.vfbk_n(vfbk_n),
	.vthMSB_n(vthMSB_n),
	.vthLSB_n(vthLSB_n),
	.test_cap1_LSB_n(test_cap1_LSB_n), //New signal
	.test_cap1_MSB_n(test_cap1_MSB_n), //New signal
	.test_cap2_n(test_cap2_n), //New signal
	.bias_disc_N_OFF_n(bias_disc_N_OFF_n),
	.bias_disc_P_OFF_n(bias_disc_P_OFF_n),
	.bias_preamp_OFF_n(bias_preamp_OFF_n),
	.output_mux_DAC_n(output_mux_DAC_n)	   //Updated size!
	);
	
      
genvar j;
generate
	for (j=0; j < 64; j=j+1) begin
	   wire [127:0] discriminator_input_slice_l, discriminator_input_slice_r;
	   wire [127:0] testpulse_output_slice_l, testpulse_output_slice_r;
	   for (genvar r=0; r<128; r=r+1) begin
	      assign discriminator_input_slice_l[r]=discriminator_input[r][2*j];
	      assign discriminator_input_slice_r[r]=discriminator_input[r][2*j+1];
	      assign testpulse_output_slice_l[r]=testpulse_output[r][2*j];
	      assign testpulse_output_slice_r[r]=testpulse_output[r][2*j+1];
           end
   
		double_column double_column (
			.clk_input(clk_column[j]),
			.data_input(dataout_column[j]),
			.data_output(datain_column[j]),
			.fastor_output(fastor[j]),
			.Shutter(shutter_multi[j]),
			.Readout(shift_multi[j]),
			.Load_configuration(configure_matrix_multi[j]),
			.use_compression(use_compression_multi[j]),
			.use_cluster_compression(use_cluster_compression_multi[j]),
			.TPswitch(TPswitch_multi[j]),
			.Polarity(polarity_multi[j]),
			.F({clock_div1_multi[j], clock_div0_multi[j]}),
			.discriminator_input_left(discriminator_input_slice_l),
			.discriminator_input_right(discriminator_input_slice_r),
			.testpulse_output_left(testpulse_output_slice_l),
			.testpulse_output_right(testpulse_output_slice_r)
		);
	end
endgenerate

endmodule
