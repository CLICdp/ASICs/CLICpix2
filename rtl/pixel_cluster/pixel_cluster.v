`timescale 100ps/1ps

//Verilog HDL for "CLICpix_DEMO", "clock_gating_TOT" "verilog"

//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/pixel_cluster/clock_divider_sync.v"
//`include "/projects/TSMC65/devel65/VCAD-6met-pre7-cad3/workAreas/pvalerio/clicpix2_verification/clicpix2/rtl/pixel_cluster/pixel_logic.v"

module pixel_cluster (

	// Please note: by "last" super-pixel I mean the one at the TOP of the matrix; by "first" super-pixel I mean the one at the bottom of the matrix.
	
	input wire clkin,	// Column clock (connected to the periphery clk_column output for the first super-pixel, to clkout of the super-pixel below otherwise)
	output wire clkout,	// Column clock output (connected to the clkin input of the super-pixel above, left unconnected for the last super-pixel)
	input wire Shutter,	// Shutter signal (column-wise: connected to the periphery shutter_multi output)
	input wire Readout,	// Readout/programming signal (column-wise: connected to the periphery shift_multi output)
	input wire [1:0] F,	// Clock divider configuration (column-wise: connected to the periphery clock_div0_multi and clock_div1_multi output)
	output wire dataout,	// Data output (connected to the periphery datain_column input for the first super-pixel, to datain of the super-pixel below otherwise)
	input wire datain,	// Data input (connected to the periphery dataout_column output for the last super-pixel, to dataout of the super-pixel above otherwise)
	input wire load,	// Load_configuration signal (column-wise: connected to the periphery configure_matrix_multi output)
	input wire use_compression,	// Use_compression signal (column-wise: connected to the periphery use_compression_multi output)
	input wire use_cluster_compression,	// Use_compression signal (column-wise: connected to the periphery use_cluster_compression_multi output)
	input wire fastor_in,	// Fastor signal for the compression algorithm (connected to 1'b0 for the last super-pixel, to fastor_out of the super-pixel above otherwise)
	output wire fastor_out,	// Fastor signal  (connected to the periphery fastor input for the first super-pixel, to fastor_in of the super-pixel below otherwise)
	input wire TPswitch,	// Switching signal for the test pulse (column-wise: connected to the periphery TPswitch_multi output)
	input wire polarityl,	// Polarity signal (column-wise: connected to the periphery polarity_multi output)
	input wire polarityr,	// Polarity signal, same net as the above one. They are separated only for routing purposes.
	
	// Pixel-specific signals:
	// Every signal has the name
	//	NAME{l;r}[1-8]
	// {l;r} means the pixel is at the left or right of the digital part
	// [1-8] is the number of the pixel in the column, so the superpixel would have this organization:
	//	|----|----|
	//	| l1 | r1 |
	//	| l2 | r2 |
	//	| l3 | r3 |
	//	| l4 | r4 |
	//	| l5 | r5 |
	//	| l6 | r6 |
	//	| l7 | r7 |
	//	| l8 | r8 |
	//	|----|----|
	//
	// TP: Enable signal for the test_pulse
	// TPswitch and TPswitch_n: switching signals (both phases) for the test_pulse
	// polarity: Polarity signal
	// config_reg and config_reg_n: Calibration DAC configuration register
	// D: Discriminator output
	
	output wire TPl1, TPswitch_nl1, TPswitchl1, polarityl1,
	output wire TPl2, TPswitch_nl2, TPswitchl2, polarityl2,
	output wire TPl3, TPswitch_nl3, TPswitchl3, polarityl3,
	output wire TPl4, TPswitch_nl4, TPswitchl4, polarityl4,
	output wire TPl5, TPswitch_nl5, TPswitchl5, polarityl5,
	output wire TPl6, TPswitch_nl6, TPswitchl6, polarityl6,
	output wire TPl7, TPswitch_nl7, TPswitchl7, polarityl7,
	output wire TPl8, TPswitch_nl8, TPswitchl8, polarityl8,
	output wire TPr1, TPswitch_nr1, TPswitchr1, polarityr1,
	output wire TPr2, TPswitch_nr2, TPswitchr2, polarityr2,
	output wire TPr3, TPswitch_nr3, TPswitchr3, polarityr3,
	output wire TPr4, TPswitch_nr4, TPswitchr4, polarityr4,
	output wire TPr5, TPswitch_nr5, TPswitchr5, polarityr5,
	output wire TPr6, TPswitch_nr6, TPswitchr6, polarityr6,
	output wire TPr7, TPswitch_nr7, TPswitchr7, polarityr7,
	output wire TPr8, TPswitch_nr8, TPswitchr8, polarityr8,
	output wire [3:0]config_regl1, config_reg_nl1,
	output wire [3:0]config_regl2, config_reg_nl2,
	output wire [3:0]config_regl3, config_reg_nl3,
	output wire [3:0]config_regl4, config_reg_nl4,
	output wire [3:0]config_regl5, config_reg_nl5,
	output wire [3:0]config_regl6, config_reg_nl6,
	output wire [3:0]config_regl7, config_reg_nl7,
	output wire [3:0]config_regl8, config_reg_nl8,
	output wire [3:0]config_regr1, config_reg_nr1,
	output wire [3:0]config_regr2, config_reg_nr2,
	output wire [3:0]config_regr3, config_reg_nr3,
	output wire [3:0]config_regr4, config_reg_nr4,
	output wire [3:0]config_regr5, config_reg_nr5,
	output wire [3:0]config_regr6, config_reg_nr6,
	output wire [3:0]config_regr7, config_reg_nr7,
	output wire [3:0]config_regr8, config_reg_nr8,
	input wire Dl1,
	input wire Dl2,
	input wire Dl3,
	input wire Dl4,
	input wire Dl5,
	input wire Dl6,
	input wire Dl7,
	input wire Dl8,
	input wire Dr1,
	input wire Dr2,
	input wire Dr3,
	input wire Dr4,
	input wire Dr5,
	input wire Dr6,
	input wire Dr7,
	input wire Dr8
);

pixel_logic pl1 (clk2, Shutter, Readout, D_nl1, F, dataoutl1, datain, TPl1, config_regl1, load, use_compression, clk_countl1);
pixel_logic pl2 (clk2, Shutter, Readout, D_nl2, F, dataoutl2, dataoutr2, TPl2, config_regl2, load, use_compression, clk_countl2);
pixel_logic pl3 (clk2, Shutter, Readout, D_nl3, F, dataoutl3, dataoutl2, TPl3, config_regl3, load, use_compression, clk_countl3);
pixel_logic pl4 (clk2, Shutter, Readout, D_nl4, F, dataoutl4, dataoutr4, TPl4, config_regl4, load, use_compression, clk_countl4);
pixel_logic pl5 (clk2, Shutter, Readout, D_nl5, F, dataoutl5, dataoutl4, TPl5, config_regl5, load, use_compression, clk_countl5);
pixel_logic pl6 (clk2, Shutter, Readout, D_nl6, F, dataoutl6, dataoutr6, TPl6, config_regl6, load, use_compression, clk_countl6);
pixel_logic pl7 (clk2, Shutter, Readout, D_nl7, F, dataoutl7, dataoutl6, TPl7, config_regl7, load, use_compression, clk_countl7);
pixel_logic pl8 (clk2, Shutter, Readout, D_nl8, F, dataout_preflag, dataoutr8, TPl8, config_regl8, load, use_compression, clk_countl8);
pixel_logic pr1 (clk2, Shutter, Readout, D_nr1, F, dataoutr1, dataoutl1, TPr1, config_regr1, load, use_compression, clk_countr1);
pixel_logic pr2 (clk2, Shutter, Readout, D_nr2, F, dataoutr2, dataoutr1, TPr2, config_regr2, load, use_compression, clk_countr2);
pixel_logic pr3 (clk2, Shutter, Readout, D_nr3, F, dataoutr3, dataoutl3, TPr3, config_regr3, load, use_compression, clk_countr3);
pixel_logic pr4 (clk2, Shutter, Readout, D_nr4, F, dataoutr4, dataoutr3, TPr4, config_regr4, load, use_compression, clk_countr4);
pixel_logic pr5 (clk2, Shutter, Readout, D_nr5, F, dataoutr5, dataoutl5, TPr5, config_regr5, load, use_compression, clk_countr5);
pixel_logic pr6 (clk2, Shutter, Readout, D_nr6, F, dataoutr6, dataoutr5, TPr6, config_regr6, load, use_compression, clk_countr6);
pixel_logic pr7 (clk2, Shutter, Readout, D_nr7, F, dataoutr7, dataoutl7, TPr7, config_regr7, load, use_compression, clk_countr7);
pixel_logic pr8 (clk2, Shutter, Readout, D_nr8, F, dataoutr8, dataoutr7, TPr8, config_regr8, load, use_compression, clk_countr8);

assign config_reg_nl1 =  ~config_regl1;
assign config_reg_nl2 =  ~config_regl2;
assign config_reg_nl3 =  ~config_regl3;
assign config_reg_nl4 =  ~config_regl4;
assign config_reg_nl5 =  ~config_regl5;
assign config_reg_nl6 =  ~config_regl6;
assign config_reg_nl7 =  ~config_regl7;
assign config_reg_nl8 =  ~config_regl8;
assign config_reg_nr1 =  ~config_regr1;
assign config_reg_nr2 =  ~config_regr2;
assign config_reg_nr3 =  ~config_regr3;
assign config_reg_nr4 =  ~config_regr4;
assign config_reg_nr5 =  ~config_regr5;
assign config_reg_nr6 =  ~config_regr6;
assign config_reg_nr7 =  ~config_regr7;
assign config_reg_nr8 =  ~config_regr8;

assign TPswitchl1 =  TPl1 && TPswitch;
assign TPswitchl2 =  TPl2 && TPswitch;
assign TPswitchl3 =  TPl3 && TPswitch;
assign TPswitchl4 =  TPl4 && TPswitch;
assign TPswitchl5 =  TPl5 && TPswitch;
assign TPswitchl6 =  TPl6 && TPswitch;
assign TPswitchl7 =  TPl7 && TPswitch;
assign TPswitchl8 =  TPl8 && TPswitch;
assign TPswitchr1 =  TPr1 && TPswitch;
assign TPswitchr2 =  TPr2 && TPswitch;
assign TPswitchr3 =  TPr3 && TPswitch;
assign TPswitchr4 =  TPr4 && TPswitch;
assign TPswitchr5 =  TPr5 && TPswitch;
assign TPswitchr6 =  TPr6 && TPswitch;
assign TPswitchr7 =  TPr7 && TPswitch;
assign TPswitchr8 =  TPr8 && TPswitch;

assign TPswitch_nl1 =  ~TPswitchl1;
assign TPswitch_nl2 =  ~TPswitchl2;
assign TPswitch_nl3 =  ~TPswitchl3;
assign TPswitch_nl4 =  ~TPswitchl4;
assign TPswitch_nl5 =  ~TPswitchl5;
assign TPswitch_nl6 =  ~TPswitchl6;
assign TPswitch_nl7 =  ~TPswitchl7;
assign TPswitch_nl8 =  ~TPswitchl8;
assign TPswitch_nr1 =  ~TPswitchr1;
assign TPswitch_nr2 =  ~TPswitchr2;
assign TPswitch_nr3 =  ~TPswitchr3;
assign TPswitch_nr4 =  ~TPswitchr4;
assign TPswitch_nr5 =  ~TPswitchr5;
assign TPswitch_nr6 =  ~TPswitchr6;
assign TPswitch_nr7 =  ~TPswitchr7;
assign TPswitch_nr8 =  ~TPswitchr8;

assign polarityl1 =  polarityl;
assign polarityl2 =  polarityl;
assign polarityl3 =  polarityl;
assign polarityl4 =  polarityl;
assign polarityl5 =  polarityl;
assign polarityl6 =  polarityl;
assign polarityl7 =  polarityl;
assign polarityl8 =  polarityl;
assign polarityr1 =  polarityr;
assign polarityr2 =  polarityr;
assign polarityr3 =  polarityr;
assign polarityr4 =  polarityr;
assign polarityr5 =  polarityr;
assign polarityr6 =  polarityr;
assign polarityr7 =  polarityr;
assign polarityr8 =  polarityr;

//assign fastor_rt_output = clk_countl1 || clk_countl2 || clk_countl3 || clk_countl4 || clk_countl5 || clk_countl6 || clk_countl7 || clk_countl8 ||
//clk_countr1 || clk_countr2 || clk_countr3 || clk_countr4 || clk_countr5 || clk_countr6 ||clk_countr7 || clk_countr8 || fastor_rt_input;

assign flags_or = dataoutl1 || dataoutl2 || dataoutl3 || dataoutl4 || dataoutl5 || dataoutl6 ||dataoutl7 || dataout_preflag ||
dataoutr1 || dataoutr2 || dataoutr3 || dataoutr4 || dataoutr5 || dataoutr6 ||dataoutr7 || dataoutr8;
compression_mux_cluster cmc (dataout_preflag, datain, dataout, clk3, Readout, flags_or, use_cluster_compression);

assign fastor_out = flags_or || fastor_in;

CKBD6BWP7THVT bufclk3 (.I (clk2), .Z (clk3_premux));
//assign clk3_premux = clk2;

CKMUX2D1BWP7THVT muxclk3 (.I0 (1'b0), .I1 (clk3_premux), .S (Readout), .Z (clk3));
CKBD6BWP7THVT bufclk (.I (clkin), .Z (clkout));
CKBD12BWP7THVT bufclk2 (.I (clkin), .Z (clk2));

CKND3BWP7THVT invDl1 (.I (Dl1), .ZN (D_nl1));
CKND3BWP7THVT invDl2 (.I (Dl2), .ZN (D_nl2));
CKND3BWP7THVT invDl3 (.I (Dl3), .ZN (D_nl3));
CKND3BWP7THVT invDl4 (.I (Dl4), .ZN (D_nl4));
CKND3BWP7THVT invDl5 (.I (Dl5), .ZN (D_nl5));
CKND3BWP7THVT invDl6 (.I (Dl6), .ZN (D_nl6));
CKND3BWP7THVT invDl7 (.I (Dl7), .ZN (D_nl7));
CKND3BWP7THVT invDl8 (.I (Dl8), .ZN (D_nl8));
CKND3BWP7THVT invDr1 (.I (Dr1), .ZN (D_nr1));
CKND3BWP7THVT invDr2 (.I (Dr2), .ZN (D_nr2));
CKND3BWP7THVT invDr3 (.I (Dr3), .ZN (D_nr3));
CKND3BWP7THVT invDr4 (.I (Dr4), .ZN (D_nr4));
CKND3BWP7THVT invDr5 (.I (Dr5), .ZN (D_nr5));
CKND3BWP7THVT invDr6 (.I (Dr6), .ZN (D_nr6));
CKND3BWP7THVT invDr7 (.I (Dr7), .ZN (D_nr7));
CKND3BWP7THVT invDr8 (.I (Dr8), .ZN (D_nr8));
endmodule

module compression_mux_cluster (datain_hit, datain_nothit, dataout, clk, shift, flags_or, use_compression);
input datain_hit, datain_nothit, clk, shift, flags_or, use_compression;
output dataout;
reg dataout, flag;
wire set_flag;
assign set_flag = (flags_or && ~shift);

always @(posedge clk or posedge set_flag) begin
if (set_flag) begin
	dataout <= 1;
end else begin
	if (flag) begin
		dataout <= datain_hit;
	end else begin
		dataout <= datain_nothit;
	end
end
end

always @(posedge shift or negedge use_compression) begin
if (~use_compression) begin
	flag <= 1;
end else begin
	flag <= dataout;
end
end

endmodule
